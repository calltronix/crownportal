


// "http://stackoverflow.com"
var base_url = window.location.origin + '/' + window.location.pathname.split ('/') [1] + '/';

$(document).ready(function () {
	$("#login_form").submit(function (e)
	{
		e.preventDefault();

		$.ajax({
			type: "POST",
			url: $(this).attr("action"),
			data: $(this).serializeArray(),
			beforeSend: function ()
			{
				var response = '<span class="text-info">Logging in..</span>';
				$("#response_div").html(response);
				$("#auth_button").hide();
			},
			success: function (res)
			{

				if(res === '1')
				{
					var response = '<span class="text-danger">Incorrect credentials</span>';

					$("#response_div").show();
					$("#loading").hide();
					$("#response_div").html(response);
					$("#auth_button").show();
				}

				else
				{
					var response = '<span class="text-success">Login successful... Redirecting ...</span>';

					$("#response_div").show();
					$("#login_form")[0].reset();
					$("#loading").hide();
					$("#response_div").html(response);

					setTimeout(function () {
						window.location.href = base_url + "index.php/Dealer/dashboard";
					},2000)
				}

			}
		});

	});
});
