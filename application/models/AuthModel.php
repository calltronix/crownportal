<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthModel extends CI_Model
{
	public function check_user_exists($email, $phone)
	{
		$this->db->where('email', $email);
		$this->db->or_where('phone', $phone);
		$query = $this->db->count_all_results('users');

		return $query;
	}
	public function login_user($dealer_code, $password)
	{

		$query = $this->db->where('dealercode', $dealer_code)
			->get('tbl_users');


		if ($query->row('password') === sha1($password)) {
			$data["userID"] = $query->row('userID');
			$data["levelID"] = $query->row('levelID');
			return $data;
		} else {
			return "wrong";
		}
	}
	public function get_user_details($session)
	{
		$this->db->where('dealerCode',$session);
		$query = $this->db->get('tbl_dealers_kenya');
		return $query->row();

	}
}
