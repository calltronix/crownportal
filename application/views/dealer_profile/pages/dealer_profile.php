<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body met-pro-bg">
							<div class="met-profile">
								<div class="row">
									<div class="col-lg-4 ml-auto">
										<div class="text-left list-unstyled personal-detail"><li>Director's Contacts</li></div>
										<hr>
										<ul class="list-unstyled personal-detail">
											<li class=""><i class="dripicons-user mr-2 text-info font-18"></i>
												<b>Name </b>: John Doe
											</li>
											<li class=""><i class="dripicons-phone mr-2 text-info font-18"></i>
												<b>phone </b>: +91 23456 78910
											</li>
											<li class="mt-2"><i class="dripicons-mail text-info font-18 mt-2 mr-2"></i>
												<b>Email </b>: mannat.theme@gmail.com
											</li>

										</ul>

									</div>
									<div class="col-lg-4 ml-auto">
										<div class="text-left list-unstyled personal-detail"><li>Key Person/Manager’s Contacts</li></div>
										<hr>
										<ul class="list-unstyled personal-detail">
											<li class=""><i class="dripicons-user mr-2 text-info font-18"></i>
												<b>Name </b>: John Doe
											</li>
											<li class=""><i class="dripicons-phone mr-2 text-info font-18"></i>
												<b>phone </b>: +91 23456 78910
											</li>
											<li class="mt-2"><i class="dripicons-mail text-info font-18 mt-2 mr-2"></i>
												<b>Email </b>: mannat.theme@gmail.com
											</li>

										</ul>
									</div>
									<div class="col-lg-4 ml-auto">
										<div class="text-left list-unstyled personal-detail"><li>Counter staff’s Contacts</li></div>
										<hr>
										<ul class="list-unstyled personal-detail">
										<li class=""><i class="dripicons-user mr-2 text-info font-18"></i>
											<b>Name </b>: John Doe
										</li>

											<li class=""><i class="dripicons-phone mr-2 text-info font-18"></i>
												<b>phone </b>: +91 23456 78910
											</li>
											<li class="mt-2"><i class="dripicons-mail text-info font-18 mt-2 mr-2"></i>
												<b>Email </b>: mannat.theme@gmail.com
											</li>

										</ul>
									</div>
									<!--end col-->
								</div><!--end row-->
							</div>
							<div class="met-profile">
								<div class="row">
									<div class="col-lg-4 ml-auto">
										<div class="text-left list-unstyled personal-detail"><li>DU Tinder's Contacts</li></div>
										<hr>
										<ul class="list-unstyled personal-detail">
											<li class=""><i class="dripicons-user mr-2 text-info font-18"></i>
												<b>Name </b>: John Doe
											</li>
											<li class=""><i class="dripicons-phone mr-2 text-info font-18"></i>
												<b>phone </b>: +91 23456 78910
											</li>
											<li class="mt-2"><i class="dripicons-mail text-info font-18 mt-2 mr-2"></i>
												<b>Email </b>: mannat.theme@gmail.com
											</li>

										</ul>

									</div>
									<div class="col-lg-4 ml-auto">
										<div class="text-left list-unstyled personal-detail"><li>Accountant’s Contacts</li></div>
										<hr>
										<ul class="list-unstyled personal-detail">
											<li class=""><i class="dripicons-user mr-2 text-info font-18"></i>
												<b>Name </b>: John Doe
											</li>
											<li class=""><i class="dripicons-phone mr-2 text-info font-18"></i>
												<b>phone </b>: +91 23456 78910
											</li>
											<li class="mt-2"><i class="dripicons-mail text-info font-18 mt-2 mr-2"></i>
												<b>Email </b>: mannat.theme@gmail.com
											</li>

										</ul>
									</div>
									<div class="col-lg-4 ml-auto">

									</div>
									<!--end col-->
								</div><!--end row-->

							</div>
						</div>
						<div class="card-body">
							<ul>
								<li>Address:</li>
								<br>
								<li>Location:</li>
								<br>
								<li>Sales Officer’s Name:</li>
								<br>
								<li>ASM’s Name:</li>
								<br>
								<li>Sales Order:</li>
							</ul>
						</div>
						<!--end card-body-->
					</div><!--end card-->
				</div><!--end col-->
			</div>

