<div class="page-wrapper"><!-- Page Content-->
	<div class="page-content-tab">
		<div class="container-fluid">
			<br><!-- Page-Title -->
			<div class="row">
				<div class="col-md-4">
					<div class="card report-card">
						<div class="card-body">
							<div class="float-right"><i class="fas fa-dollar-sign bg-soft-pink main-widgets-icon"></i>
							</div>
							<h4 class="title-text mt-0">Available Balance Credit Limit</h4>
							<h3 class="my-3">Ksh.500<small></small></h3>
						</div><!--end card-body-->
						<div class="card-body p-0">
							<div id="spark1" class="apex-charts"></div>
						</div>
					</div><!--end card--></div><!--end col-->
				<div class="col-md-4">
					<div class="card report-card">
						<div class="card-body">
							<div class="float-right"><i class="fas fa-apple-alt bg-soft-success main-widgets-icon"></i>
							</div>
							<h4 class="title-text mt-0">Total Volume Sales (MTD)</h4>
							<h3 class="my-3">Ksh.35000 <small></small></h3>
						</div><!--end card-body-->
						<div class="card-body p-0">
							<div id="spark3" class="apex-charts"></div>
						</div>
					</div><!--end card--></div>
				<div class="col-md-4">
					<div class="card report-card">
						<div class="card-body">
							<div class="float-right"><i class="fas fa-radiation bg-soft-warning main-widgets-icon"></i>
							</div>
							<h4 class="title-text mt-0">Total Value Sales (MTD)</h4>
							<h3 class="my-3">Ksh.8800 <small></small></h3>
						</div><!--end card-body-->
						<div class="card-body p-0">
							<div id="spark2" class="apex-charts"></div>
						</div>
					</div><!--end card--></div>
			</div>

			<!-- /.modal -->
			<!--end footer--></div>
		<!-- end page content --></div>
