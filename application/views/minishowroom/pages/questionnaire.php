<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-md-8">

				<hr>
				<iframe src="https://drive.google.com/file/d/1Boxeq3QKpGQVCA74n9J7a4vGlwRcqPD1/preview" width="940" height="480"></iframe>
			</div>
			<div class="col-md-4">
				<div class="text-center">
					<br><br><br>
					<button type="button" class="btn btn-primary"><i class="fa fa-print"></i>&nbsp;Print</button>
					<br><br><br>
					<button type="button" class="btn btn-warning"><i class="fa fa-download"></i>&nbsp;Download</button>
				</div>

			</div>

		</div>
	</div>

</div>
