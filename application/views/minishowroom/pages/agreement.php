<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-md-8">

				<hr>
				<iframe src="https://docs.google.com/document/d/e/2PACX-1vSgX20Wt4n4IvBqv3Dh8lJgQcJd5CLRulOc4YtNzT4HGp7MvYw6Re1TpWq-VcA9dY7NIyVd3NA9EggQ/pub?embedded=true" width="940" height="480"></iframe>
			</div>
			<div class="col-md-4">
				<div class="text-center">
					<br><br><br>
					<button type="button" class="btn btn-primary"><i class="fa fa-print"></i>&nbsp;Print</button>
					<br><br><br>
					<button type="button" class="btn btn-warning"><i class="fa fa-download"></i>&nbsp;Download</button>
				</div>

			</div>

		</div>
	</div>

</div>
