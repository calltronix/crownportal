<div class="container-fluid">
	<!-- Page-Title -->
	<!--end row-->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<button type="button"
							class="btn btn-primary waves-effect waves-light float-right mb-3" data-toggle="modal"
							data-animation="bounce" data-target=".bs-example-modal-lg">+ Add New Training Category
					</button>
					<h4 class="header-title mt-0 mb-3">All Training Category</h4>
					<div class="table-responsive dash-social">
						<div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

							<div class="row">
								<div class="col-sm-12">
									<table id="datatable" class="table dataTable no-footer" role="grid"
										   aria-describedby="datatable_info">
										<thead class="thead-light">
										<tr role="row">
											<th>Category</th>
											<th>Number of People</th>
											<th>Created At</th>
										</tr>
										<!--end tr-->
										</thead>
										<tbody>

										<tr role="row" class="odd">
											<td>Dealers and Managers
											</td>
											<td>3</td>
											<td>2020-01-26
											</td>
										</tr>
										<tr role="row" class="even">
											<td class="sorting_1">Painters</td>
											<td>6</td>
											<td>2020-01-23
											</td>
										</tr>

										</tbody>
									</table>
								</div>
							</div>

						</div>
					</div>
				</div>
				<!--end card-body-->
			</div>
			<!--end card-->
		</div>
		<!--end col-->
	</div>
	<!--end row-->
</div><!-- container -->
<!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
	 aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title mt-0" id="myLargeModalLabel">Add New Training Category</h5>
				<button type="button"
						class="close" data-dismiss="modal" aria-hidden="true">×
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="example-input1-group1">Training Request Category</label>
								<div class="input-group">
									<div class="input-group-prepend"></div>
									<select required class="form-control">
										<option>choose</option>
										<option>Dealers & Managers</option>
										<option>Painters</option>
										<option>Counter Staffs</option>
										<option>Mason</option>
										<option>Carpenters</option>
										<option>DU Tinter training </option>
									</select>
								</div>
							</div>


						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group"><label for="status-select" class="mr-2">Number of People</label>
								<input type="number" max="40" min="0" class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="status-select" class="mr-2">Venue</label>
								<div class="input-group">
									<div class="input-group-prepend"></div>
									<select required class="form-control">
										<option>choose</option>
										<option> Inshop </option>
										<option>Crown Training Venue</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<fieldset class="border p-2">
						<legend  class="w-auto"><small>Trainees Details</small></legend>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group"><label for="status-select" class="mr-2">Name</label>
									<input type="text" max="40" min="0" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group"><label for="status-select" class="mr-2">Phone Number</label>
									<input type="text"  class="form-control">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group"><label for="status-select" class="mr-2">Products to be Training</label>
									<input type="text" max="40" min="0" class="form-control">
								</div>
							</div>

						</div>
					</fieldset>


					<button type="button" class="btn btn-sm btn-primary">Save</button>
					<button type="button"
							class="btn btn-sm btn-danger">Delete
					</button>
				</form>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
