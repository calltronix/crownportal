<?php
$page = ucwords(strtolower(str_replace('_', ' ', $this->uri->segment(2))), "");
$page = ucwords($page);
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Crown Paints - Dealer Portal</title>
	<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
	<meta content="Crown Paints - Dealer Portal" name="description">
	<meta content="" name="author">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App favicon -->
	<link href="<?php echo base_url() ?>assets/plugins/ticker/jquery.jConveyorTicker.css" rel="stylesheet"
		  type="text/css">

	<link href="<?php echo base_url() ?>assets/plugins/jvectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet">
	<link rel="apple-touch-icon" sizes="57x57"
		  href="<?php echo base_url() ?>assets/images/favicons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60"
		  href="<?php echo base_url() ?>assets/images/favicons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72"
		  href="<?php echo base_url() ?>assets/images/favicons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76"
		  href="<?php echo base_url() ?>assets/images/favicons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114"
		  href="<?php echo base_url() ?>assets/images/favicons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120"
		  href="<?php echo base_url() ?>assets/images/favicons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144"
		  href="<?php echo base_url() ?>assets/images/favicons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152"
		  href="<?php echo base_url() ?>assets/images/favicons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180"
		  href="<?php echo base_url() ?>assets/images/favicons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"
		  href="<?php echo base_url() ?>assets/images/favicons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32"
		  href="<?php echo base_url() ?>assets/images/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96"
		  href="<?php echo base_url() ?>assets/images/favicons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16"
		  href="<?php echo base_url() ?>assets/images/favicons/favicon-16x16.png">
	<link rel="manifest" href="<?php echo base_url() ?>assets/images/favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<link href="<?php echo base_url('') ?>/assets/new_assets/plugins/jvectormap/jquery-jvectormap-2.0.2.css"
		  rel="stylesheet">
	<link href="<?php echo base_url('') ?>/assets/new_assets/plugins/lightpick/lightpick.css" rel="stylesheet">
	<!-- App css -->
	<link href="<?php echo base_url('') ?>/assets/new_assets/assets/css/bootstrap.min.css" rel="stylesheet"
		  type="text/css">
	<link href="<?php echo base_url('') ?>/assets/new_assets/assets/css/jquery-ui.min.css" rel="stylesheet">
	<link href="<?php echo base_url('') ?>/assets/new_assets/assets/css/icons.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('') ?>/assets/new_assets/assets/css/metisMenu.min.css" rel="stylesheet"
		  type="text/css">
	<link href="<?php echo base_url('') ?>/assets/new_assets/assets/css/app.min.css" rel="stylesheet" type="text/css">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<style type="text/css">
	.apexcharts-legend {
		display: flex;
		overflow: auto;
		padding: 0 10px;
	}

	.apexcharts-legend.position-bottom,
	.apexcharts-legend.position-top {
		flex-wrap: wrap
	}

	.apexcharts-legend.position-right,
	.apexcharts-legend.position-left {
		flex-direction: column;
		bottom: 0;
	}

	.apexcharts-legend.position-bottom.left,
	.apexcharts-legend.position-top.left,
	.apexcharts-legend.position-right,
	.apexcharts-legend.position-left {
		justify-content: flex-start;
	}

	.apexcharts-legend.position-bottom.center,
	.apexcharts-legend.position-top.center {
		justify-content: center;
	}

	.apexcharts-legend.position-bottom.right,
	.apexcharts-legend.position-top.right {
		justify-content: flex-end;
	}

	.apexcharts-legend-series {
		cursor: pointer;
		line-height: normal;
	}

	.apexcharts-legend.position-bottom .apexcharts-legend-series,
	.apexcharts-legend.position-top .apexcharts-legend-series {
		display: flex;
		align-items: center;
	}

	.apexcharts-legend-text {
		position: relative;
		font-size: 14px;
	}

	.apexcharts-legend-text *,
	.apexcharts-legend-marker * {
		pointer-events: none;
	}

	.apexcharts-legend-marker {
		position: relative;
		display: inline-block;
		cursor: pointer;
		margin-right: 3px;
	}

	.apexcharts-legend.right .apexcharts-legend-series,
	.apexcharts-legend.left .apexcharts-legend-series {
		display: inline-block;
	}

	.apexcharts-legend-series.no-click {
		cursor: auto;
	}

	.apexcharts-legend .apexcharts-hidden-zero-series,
	.apexcharts-legend .apexcharts-hidden-null-series {
		display: none !important;
	}

	.inactive-legend {
		opacity: 0.45;
	}

	.dash-info-carousel {
		height: 70vh;
	}

</style>

<body class="enlarge-menu" data-gr-c-s-loaded="true">
<!-- Top Bar Start -->
<div class="topbar">
	<!-- LOGO -->
	<div class="topbar-left"><a href="#" class="logo"><span>
                    <!-- <img src="<?php echo base_url() ?>assets/images/logo-sm.png" alt="logo-small" class="logo-sm"> </span><span> -->
                    <img style="height: 5vh;width: 7vh"
						 src="<?php echo base_url() ?>assets/images/logos/crownpaints.png" alt="logo-large"
						 class="logo-lg"></span></a></div>
	<!--end logo-->
	<!-- Navbar -->

	<nav class="navbar-custom">

		<ul class="list-unstyled topbar-nav float-right mb-0">

			<li class="hidden-sm">
				<div class="crypto-balance">
					<i class="dripicons-wallet text-muted align-self-center"></i>
					<div class="btc-balance">
						<h5 class="m-0">9.521.32 <span>ACC</span></h5><span class="text-muted">Account Balance</span>
					</div>
				</div>
			</li>
			<li class="hidden-sm"><a class="nav-link dropdown-toggle waves-effect waves-light"
									 data-toggle="dropdown" href="javascript: void(0);" role="button"
									 aria-haspopup="false"
									 aria-expanded="false"></a>

			</li>

			<li class="dropdown notification-list"><a
						class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown"
						href="crm-index.html#" role="button" aria-haspopup="false" aria-expanded="false"><i
							class="dripicons-cart noti-icon"></i> <span
							class="badge badge-danger badge-pill noti-icon-badge">2</span></a>
				<div class="dropdown-menu dropdown-menu-right dropdown-lg">
					<!-- item-->
					<!-- <h6 class="dropdown-item-text">Cart (18)</h6> -->

			</li>
			<li class="dropdown"><a class="nav-link dropdown-toggle waves-effect waves-light nav-user"
									data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
									aria-expanded="false"><img
							src="<?php echo base_url() ?>assets/images/users/avatar.jpg"
							alt="profile-user" class="rounded-circle"> <span
							class="ml-1 nav-user-name hidden-sm"><?php echo $details->dealerCode ?>
                            <i class="mdi mdi-chevron-down"></i></span></a>
				<div class="dropdown-menu dropdown-menu-right">
					<a class="dropdown-item" href="<?php echo site_url() ?>/Auth/signout"><i
								class="dripicons-exit text-muted mr-2"></i> Logout</a>
				</div>
			</li>
		</ul>
		<!--end topbar-nav-->
		<ul class="list-unstyled topbar-nav mb-0">
			<li>
				<button class="button-menu-mobile nav-link waves-effect waves-light"><i
							class="dripicons-menu nav-icon"></i></button>
			</li>
			<li class="hide-phone app-search">
				<h4>
					Dealer Name &nbsp;:&nbsp;<?php echo $details->dealerName ?>
				</h4>

			</li>
			<li class="hide-phone app-search">
				<h4>
					Dealer Code &nbsp;:&nbsp;<?php echo $details->dealerCode ?>
				</h4>

			</li>

		</ul>
	</nav><!-- end navbar-->
</div><!-- Top Bar End -->
<div class="page-wrapper">
	<!-- Left Sidenav -->
	<div class="left-sidenav"><!-- LOGO -->
		<!--end logo-->
		<div class="leftbar-profile p-3 w-100">
			<div class="media position-relative">

				<!--end media-body--></div>
		</div>

		<div id="leftCol">
			<ul class="metismenu left-sidenav-menu slimscroll">
				<li class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link">
						<i data-feather="monitor"
						   class="align-self-center vertical-menu-icon icon-dual-vertical"></i>
						<span>Dashboard</span> <span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
					<ul class="nav-second-level" aria-expanded="false">
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Dealer/dashboard"><i
										class="fa fa-dashboard"></i>Dashboard</a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Dealer/dealer_profile"><i
										class="dripicons-user-group"></i>Dealer Profile</a></li>



					</ul>
				</li>
				<li class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link"><i data-feather="grid"
																								  class="align-self-center vertical-menu-icon icon-dual-vertical"></i>
						<span>Sales Order</span> <span class="menu-arrow"><i
									class="mdi mdi-chevron-right"></i></span></a>
					<ul class="nav-second-level" aria-expanded="false">

						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Dealer/sales_dashboard"><i
										class="fa fa-dashboard"></i>Dashboard</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Dealer/place_order"><i
										class="dripicons-dot"></i>Place Order</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Dealer/order_status"><i
										class="dripicons-dot"></i> Order Status</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Dealer/foc_request"><i
										class="dripicons-dot"></i>FOC Request</a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Dealer/foc_request_status"><i
										class="dripicons-dot"></i>FOC Request Status</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Dealer/stock_balance"><i
										class="dripicons-dot"></i>Stock Balance</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Dealer/order_history"><i
										class="dripicons-dot"></i> Order History</a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Dealer/pending_orders"><i
										class="dripicons-dot"></i>Pending Orders</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Dealer/other_issues"><i
										class="dripicons-dot"></i> Other Issues</a></li>

						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Sales/faq"><i
										class="dripicons-information"></i>FAQs</a></li>
					</ul>
				</li>
				<li class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link"><i data-feather="copy"
																								  class="align-self-center vertical-menu-icon icon-dual-vertical"></i>
						<span>Promotion Scheme</span> <span class="menu-arrow"><i
									class="mdi mdi-chevron-right"></i></span></a>
					<ul class="nav-second-level" aria-expanded="false">
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Dealer/promotions"><i
										class="ti-arrow-circle-right"></i>Promotions</a></li>

						<li>
							<a href="javascript: void(0);"><i class="dripicons-view-apps"></i>Previlege Club <span
										class="menu-arrow left-has-menu"><i
											class="mdi mdi-chevron-right"></i></span></a>
							<ul class="nav-second-level" aria-expanded="false">
								<li class="nav-item"><a class="nav-link"
														href="<?php echo site_url() ?>/Dealer/club_PPT">
										<i class="dripicons-user-id"></i>Club PPT </a></li>
								<li class="nav-item"><a class="nav-link"
														href="<?php echo site_url() ?>/Dealer/club_benefit">
										<i class="dripicons-user-id"></i>Club Benefit </a></li>
								<li><a href="<?php echo site_url() ?>/Dealer/club_status"><i
												class="dripicons-user-id"></i>Club Status</a></li>
								<li><a href="<?php echo site_url() ?>/Dealer/club_faq"><i
												class="dripicons-information"></i>Club FAQs</a></li>
							</ul>
						</li>

						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Dealer/ongoing_scheme"><i
										class="dripicons-pulse"></i>Ongoing Scheme </a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Dealer/promotional_slab"><i
										class="dripicons-brightness-medium"></i>Promotional / Slab item status</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Dealer/scheme_circular"><i
										class="dripicons-basket"></i> Schemes Circulars</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Dealer/scheme_pictures"><i
										class="dripicons-clipboard"></i> Schemes Pictures/Banners</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Dealer/privilege_faq"><i
										class="dripicons-information"></i>FAQs</a></li>

					</ul>
				</li>
				<li class="leftbar-menu-item">
					<a href="javascript: void(0);" class="menu-link">
						<i class="ti ti-stats-up align-self-center vertical-menu-icon icon-dual-vertical"></i>
						<span>Accounts</span> <span class="menu-arrow">
						<i class="mdi mdi-chevron-right"></i>
					</span>
					</a>
					<ul class="nav-second-level" aria-expanded="false">

						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Dealer/accounts_dashboard">
								<i class="fa fa-dashboard"></i>Dashboard</a></li>

						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Accounts/account_Statement"><i
										class="dripicons-card"></i>Statement</a></li>

						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Accounts/faq">
								<i class="dripicons-information"></i>FAQs</a></li>
					</ul>

				</li>
				<li class="leftbar-menu-item">
					<a href="javascript: void(0);" class="menu-link">
						<i class="ti ti-menu-alt align-self-center vertical-menu-icon icon-dual-vertical"></i>
						<span>Products</span> <span class="menu-arrow">
						<i class="mdi mdi-chevron-right"></i>
					</span>
					</a>
					<ul class="nav-second-level" aria-expanded="false">
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Products/product"><i
										class="dripicons-menu"></i>Products</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Products/faq"><i
										class="dripicons-information"></i>FAQs</a></li>
					</ul>

				</li>
				<li class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link">
						<i class="ti ti-menu-alt align-self-center vertical-menu-icon icon-dual-vertical"></i>
						<span>Price List</span> <span class="menu-arrow"><i
									class="mdi mdi-chevron-right"></i></span></a>
					<ul class="nav-second-level" aria-expanded="false">
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Dealer/price_list"><i
										class="dripicons-list"></i>Pricelist</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Dealer/price_faq"><i
										class="dripicons-information"></i>FAQs</a></li>


					</ul>
				</li>
				<li class="leftbar-menu-item">
					<a href="javascript: void(0);" class="menu-link">
						<i class="ti ti-rss align-self-center vertical-menu-icon icon-dual-vertical"></i>
						<span>Marketing</span> <span class="menu-arrow">
						<i class="mdi mdi-chevron-right"></i>
					</span>
					</a>
					<ul class="nav-second-level" aria-expanded="false">

						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Marketing/promotional_items"><i
										class="dripicons-menu"></i>Promotional Items </a></li>
						<li>
							<a href="javascript: void(0);"><i class="dripicons-view-apps"></i>Item Request <span
										class="menu-arrow left-has-menu"><i
											class="mdi mdi-chevron-right"></i></span></a>
							<ul class="nav-second-level" aria-expanded="false">
								<li class="nav-item"><a class="nav-link"
														href="<?php echo site_url() ?>/Marketing/foc">
										<i class="dripicons-user-id"></i>FOC </a></li>
								<li><a href="<?php echo site_url() ?>/Marketing/cost_sharing"><i
												class="dripicons-user-id"></i>50% Cost Sharing</a></li>
								<li><a href="<?php echo site_url() ?>/Marketing/full_price"><i
												class="dripicons-user-id"></i>Full Price</a></li>
								<li><a href="<?php echo site_url() ?>/Marketing/faq"><i
												class="dripicons-information"></i> FAQs</a></li>
							</ul>
						</li>

						<li>
							<a href="javascript: void(0);"><i class="dripicons-view-apps"></i> Branding <span
										class="menu-arrow left-has-menu"><i
											class="mdi mdi-chevron-right"></i></span></a>
							<ul class="nav-second-level" aria-expanded="false">
								<li class="nav-item"><a class="nav-link"
														href="<?php echo site_url() ?>/Marketing/inside_shop">
										<i class="dripicons-user-id"></i>In Shop </a></li>
								<li><a href="<?php echo site_url() ?>/Marketing/outside_shop"><i
												class="dripicons-user-id"></i>Outside Shop</a></li>
								<li><a href="<?php echo site_url() ?>/Marketing/other_request"><i
												class="dripicons-user-id"></i>Other Request</a></li>
								<li><a href="<?php echo site_url() ?>/Marketing/du_machine"><i
												class="dripicons-user-id"></i>DU Machine Branding </a></li>
								<li><a href="<?php echo site_url() ?>/Marketing/branding_faq"><i
												class="dripicons-information"></i> FAQs</a></li>
							</ul>
						</li>

					</ul>

				</li>
				<li class="leftbar-menu-item">
					<a href="javascript: void(0);" class="menu-link">
						<i class="ti ti-check-box align-self-center vertical-menu-icon icon-dual-vertical"></i>
						<span>Training Request</span> <span class="menu-arrow">
						<i class="mdi mdi-chevron-right"></i>
					</span>
					</a>
					<ul class="nav-second-level" aria-expanded="false">
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Training/category"><i
										class="dripicons-menu"></i>Category</a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Training/factory_visit_request"><i
										class="dripicons-phone"></i>Factory Visit</a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Training/online_training"><i
										class="dripicons-camcorder"></i>Online Training</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Training/faq"><i
										class="dripicons-information"></i>FAQs</a></li>
					</ul>

				</li>
				<li class="leftbar-menu-item">
					<a href="javascript: void(0);" class="menu-link">
						<i class="ti ti-announcement align-self-center vertical-menu-icon icon-dual-vertical"></i>
						<span>Team Kubwa</span> <span class="menu-arrow">
						<i class="mdi mdi-chevron-right"></i>
					</span>
					</a>
					<ul class="nav-second-level" aria-expanded="false">
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Tkb/team_kubwa"><i
										class="dripicons-pamphlet"></i>Team Kubwa</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Tkb/painters_list"><i
										class="dripicons-menu"></i>Painters List</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Tkb/product_chart"><i
										class="dripicons-document"></i>Product Chart </a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Tkb/banking_process"><i
										class="dripicons-media-loop"></i>Banking process</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Tkb/redemption_table"><i
										class="fa fa-table"></i>Redemption Table</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Tkb/faq"><i
										class="dripicons-information"></i>FAQs</a></li>
					</ul>

				</li>
				<li class="leftbar-menu-item">
					<a href="javascript: void(0);" class="menu-link">
						<i class="ti ti-info-alt align-self-center vertical-menu-icon icon-dual-vertical"></i>
						<span>CYS</span> <span class="menu-arrow">
						<i class="mdi mdi-chevron-right"></i>
					</span>
					</a>
					<ul class="nav-second-level" aria-expanded="false">
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Cys/ppt"><i
										class="dripicons-pamphlet"></i>PPT</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Cys/videos"><i
										class="dripicons-camcorder"></i>Videos </a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Cys/agreement"><i
										class="dripicons-document-edit"></i>Agreement</a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Cys/customers_price_list"><i
										class=" dripicons-menu"></i>Customer Price List</a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Cys/purchase_price_list"><i
										class="dripicons-menu"></i>Product Purchase Price List</a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Cys/leaflets_and_bronchures"><i
										class="dripicons-document"></i>Leaflets and Bronchures</a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Cys/faq"><i
										class="dripicons-information"></i>FAQs</a></li>

					</ul>

				</li>
				<li class="leftbar-menu-item">
					<a href="javascript: void(0);" class="menu-link">
						<i class="ti ti-announcement align-self-center vertical-menu-icon icon-dual-vertical"></i>
						<span>Mini Showroom</span> <span class="menu-arrow">
						<i class="mdi mdi-chevron-right"></i>
					</span>
					</a>
					<ul class="nav-second-level" aria-expanded="false">
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Mini_showroom/video"><i
										class="fa fa-video"></i>Video</a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Mini_showroom/concept_ppt"><i
										class="dripicons-export"></i> Concept PPT </a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Mini_showroom/questionnaire"><i
										class="dripicons-pencil"></i>Questionnaire</a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Mini_showroom/agreement"><i
										class="dripicons-thumbs-up"></i>Agreement</a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Mini_showroom/value_sales"><i
										class="dripicons-media-loop"></i>Value Sales</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Mini_showroom/faq"><i
										class="dripicons-information"></i>FAQs</a></li>
					</ul>

				</li>
				<li class="leftbar-menu-item">
					<a href="javascript: void(0);" class="menu-link">
						<i class="ti ti-stamp align-self-center vertical-menu-icon icon-dual-vertical"></i>
						<span>DU Machine </span> <span class="menu-arrow">
						<i class="mdi mdi-chevron-right"></i>
					</span>
					</a>
					<ul class="nav-second-level" aria-expanded="false">
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Du_machine/package_details"><i
										class="dripicons-menu"></i>Package Details</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Du_machine/price"><i
										class="dripicons-export"></i> DU Price </a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Du_machine/agreement"><i
										class="dripicons-thumbs-up"></i>DU Agreement</a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Du_machine/questionnaire"><i
										class="dripicons-pencil"></i>Questionnaire</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Du_machine/sop"><i
										class="dripicons-broadcast"></i>SOP</a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Du_machine/application_process"><i
										class="dripicons-document-edit"></i>Application process</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Du_machine/value_sales"><i
										class="dripicons-contract"></i>Value Sales Target V/s Achievement status</a>
						</li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Du_machine/faq"><i
										class="dripicons-information"></i>FAQs</a></li>
					</ul>

				</li>
				<li class="leftbar-menu-item">
					<a href="javascript: void(0);" class="menu-link">
						<i class="ti-eraser align-self-center vertical-menu-icon icon-dual-vertical"></i>
						<span>Complaints</span> <span class="menu-arrow">
						<i class="mdi mdi-chevron-right"></i>
					</span>
					</a>
					<ul class="nav-second-level" aria-expanded="false">
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Dealer/complaint_dashboard"><i
										class="fa fa-question-circle"></i>Disposition from CRM</a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Dealer/upload_picture"><i
										class="fa fa-upload"></i>Upload Picture</a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Dealer/complaint_faqs"><i
										class="dripicons-information"></i>FAQs</a></li>


					</ul>

				</li>
				<li class="leftbar-menu-item">
					<a href="javascript: void(0);" class="menu-link">
						<i class="ti-arrow-circle-right align-self-center vertical-menu-icon icon-dual-vertical"></i>
						<span>Competitor Details</span> <span class="menu-arrow">
						<i class="mdi mdi-chevron-right"></i>
					</span>
					</a>
					<ul class="nav-second-level" aria-expanded="false">

						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Dealer/competitor_details"><i
										class="dripicons-swap"></i> Comparison</a></li>
					</ul>

				</li>
				<li class="leftbar-menu-item">
					<a href="javascript: void(0);" class="menu-link">
						<i class="ti-agenda align-self-center vertical-menu-icon icon-dual-vertical"></i>
						<span>CVP </span> <span class="menu-arrow">
						<i class="mdi mdi-chevron-right"></i>
					</span>
					</a>
					<ul class="nav-second-level" aria-expanded="false">

						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/CVP/questionnaire"><i
										class="dripicons-pencil"></i> Questionnaire</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/CVP/survey"><i
										class="dripicons-question"></i> Survey</a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/CVP/retail_audit_format"><i
										class="dripicons-document-remove"></i> Retail Audit Format</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Dealer/faq"><i
										class="dripicons-information"></i> FAQs</a></li>
					</ul>

				</li>

				<li class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link"><i data-feather="lock"
																								  class="align-self-center vertical-menu-icon icon-dual-vertical"></i>
						<span>Sales Reports</span> <span class="menu-arrow"><i
									class="mdi mdi-chevron-right"></i></span></a>
					<ul class="nav-second-level" aria-expanded="false">

						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Reports/value_sales"><i
										class="dripicons-user-id"></i>Value Sales</a></li>
						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Reports/volume_sales"><i
										class="dripicons-toggles"></i>Volume Sales</a></li>
						<li class="nav-item"><a class="nav-link"
												href="<?php echo site_url() ?>/Reports/product_category_level"><i
										class="dripicons-copy"></i>Product Category Level </a></li>
					</ul>
				</li>
				<li class="leftbar-menu-item"><a href="javascript: void(0);" class="menu-link">
						<i data-feather="phone"
						   class="align-self-center vertical-menu-icon icon-dual-vertical"></i>
						<span>Contact US</span> <span class="menu-arrow"><i
									class="mdi mdi-chevron-right"></i></span></a>
					<ul class="nav-second-level" aria-expanded="false">

						<li class="nav-item"><a class="nav-link" href="<?php echo site_url() ?>/Dealer/contacts"><i
										class="dripicons-phone"></i>Contact Us</a></li>

					</ul>
				</li>
			</ul>
		</div>
		<style>
			#leftCol {
				display: inline-block;
				vertical-align: top;
				height: 100%;
				overflow: auto;
			}
		</style>



	</div>
	<!-- end left-sidenav-->
	<div class="page-content">
		<div class="container-fluid">
			<!-- Page-Title -->
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title-box">
						<div class="float-right">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="javascript:void(0);">Dealer Portal</a></li>
								<li class="breadcrumb-item"><a href="javascript:void(0);"><?php echo $page; ?></a></li>

							</ol>
						</div>
						&nbsp;<h4 style="margin-left: 12px" class="page-title"><?php echo $page ?></h4>

					</div>
					<!--end page-title-box-->
				</div>
				<!--end col-->
			</div><!-- end page title end breadcrumb -->
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<div class="wrap">
								<!-- <div class="jctkr-label"><span><i class="fas fa-exchange-alt mr-2"></i>Change 24
										Hours</span></div> -->
								<div class="js-conveyor-example jctkr-wrapper jctkr-initialized" style="height: 50px;">
									<ul style="width: 2295px; left: -329.025px;">
										<li style=""><img
													src="<?php echo base_url() ?>/assets/images/permaplast-brilliant-white.jpg"
													alt=""
													class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Permaplast Brilliant White
                                               </b></span>
										</li>
										<li style=""><img src="<?php echo base_url() ?>/assets/images/vesta-plus.jpg"
														  alt=""
														  class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Economy Vesta Plastic</b></span>
										</li>
										<li style=""><img
													src="<?php echo base_url() ?>/assets/images/silk-vinyl-emulsion.jpg"
													alt=""
													class="thumb-xs rounded"> <span class="usd-rate font-14"><b>
													Silk Vinyl Emulsion</b></span>
										</li>
										<li style=""><img src="<?php echo base_url() ?>/assets/images/vesta-plus.jpg"
														  alt=""
														  class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Economy Vesta Plastic</b></span>
										</li>

										<li style=""><img
													src="<?php echo base_url() ?>/assets/images/crown-ultraguard-protect.jpg"
													alt=""
													class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Ultra Guard Protect
                                               </b></span>
										</li>


									</ul>
								</div>
							</div>
						</div>
						<!--end card-body-->
					</div>
					<!--end card-->
				</div>
				<footer class="footer text-center text-sm-left">&copy; <?php echo date('Y') ?> <a
							href="https://www.crownpaints.co.ke/" target="_blank">Crown Paints</a> <span
							class="text-muted d-none d-sm-inline-block float-right">Powered  by Calltronix</span>
				</footer>
				<!--end col-->
			</div>


