<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Crown Paints - Dealer Portal</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta content="A premium admin dashboard template by Mannatthemes" name="description">
    <meta content="Mannatthemes" name="author"><!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/logos/crownpaints.png">
    <link href="<?php echo base_url() ?>assets/plugins/jvectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet">
    <!-- App css -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/metisMenu.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <!-- Top Bar Start -->
    <div class="topbar">
        <!-- LOGO -->
        <div class="topbar-left"><a href="crm-index.html" class="logo"><span>
                    <!-- <img src="<?php echo base_url() ?>assets/images/logo-sm.png" alt="logo-small" class="logo-sm"> </span><span> -->
                    <img src="<?php echo base_url() ?>assets/images/logos/crownpaints.png" alt="logo-large"
                        class="logo-lg"></span></a></div>
        <!--end logo-->
        <!-- Navbar -->
        <nav class="navbar-custom">
            <ul class="list-unstyled topbar-nav float-right mb-0">
                <li class="hidden-sm"><a class="nav-link dropdown-toggle waves-effect waves-light"
                        data-toggle="dropdown" href="javascript: void(0);" role="button" aria-haspopup="false"
                        aria-expanded="false"></a>

                </li>
                <li class="dropdown notification-list"><a
                        class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown"
                        href="crm-index.html#" role="button" aria-haspopup="false" aria-expanded="false"><i
                            class="dripicons-bell noti-icon"></i> <span
                            class="badge badge-danger badge-pill noti-icon-badge">2</span></a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-lg">
                        <!-- item-->
                        <h6 class="dropdown-item-text">Notifications (18)</h6>
                        <div class="slimscroll notification-list">
                            <!-- item--> <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                <p class="notify-details">Your order is placed<small class="text-muted">Dummy text of
                                        the printing and typesetting industry.</small></p>
                            </a><!-- item--> <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <div class="notify-icon bg-warning"><i class="mdi mdi-message"></i></div>
                                <p class="notify-details">New Message received<small class="text-muted">You have 87
                                        unread messages</small></p>
                            </a><!-- item--> <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <div class="notify-icon bg-info"><i class="mdi mdi-glass-cocktail"></i></div>
                                <p class="notify-details">Your item is shipped<small class="text-muted">It is a long
                                        established fact that a reader will</small></p>
                            </a><!-- item--> <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <div class="notify-icon bg-primary"><i class="mdi mdi-cart-outline"></i></div>
                                <p class="notify-details">Your order is placed<small class="text-muted">Dummy text of
                                        the printing and typesetting industry.</small></p>
                            </a><!-- item--> <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <div class="notify-icon bg-danger"><i class="mdi mdi-message"></i></div>
                                <p class="notify-details">New Message received<small class="text-muted">You have 87
                                        unread messages</small></p>
                            </a></div><!-- All--> <a href="javascript:void(0);"
                            class="dropdown-item text-center text-primary">View all <i class="fi-arrow-right"></i></a>
                    </div>
                </li>
                <li class="dropdown"><a class="nav-link dropdown-toggle waves-effect waves-light nav-user"
                        data-toggle="dropdown" href="crm-index.html#" role="button" aria-haspopup="false"
                        aria-expanded="false"><img src="<?php echo base_url() ?>assets/images/users/user-4.jpg"
                            alt="profile-user" class="rounded-circle"> <span class="ml-1 nav-user-name hidden-sm">Amelia
                            <i class="mdi mdi-chevron-down"></i></span></a>
                    <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="crm-index.html#"><i
                                class="dripicons-user text-muted mr-2"></i> Profile</a> <a class="dropdown-item"
                            href="crm-index.html#"><i class="dripicons-wallet text-muted mr-2"></i> My Wallet</a> <a
                            class="dropdown-item" href="crm-index.html#"><i class="dripicons-gear text-muted mr-2"></i>
                            Settings</a> <a class="dropdown-item" href="crm-index.html#"><i
                                class="dripicons-lock text-muted mr-2"></i> Lock screen</a>
                        <div class="dropdown-divider"></div><a class="dropdown-item" href="crm-index.html#"><i
                                class="dripicons-exit text-muted mr-2"></i> Logout</a>
                    </div>
                </li>
            </ul>
            <!--end topbar-nav-->
            <ul class="list-unstyled topbar-nav mb-0">
                <li><button class="button-menu-mobile nav-link waves-effect waves-light"><i
                            class="dripicons-menu nav-icon"></i></button></li>
                <li class="hide-phone app-search">
                    <form role="search" class=""><input type="text" placeholder="Search..." class="form-control"> <a
                            href="crm-index.html"><i class="fas fa-search"></i></a></form>
                </li>
            </ul>
        </nav><!-- end navbar-->
    </div><!-- Top Bar End -->
    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <div class="left-sidenav">
            <div class="main-icon-menu">
                <nav class="nav"><a href="#MetricaAnalytic" class="nav-link active " data-toggle="tooltip-custom"
                        data-placement="top" title="" data-original-title="Dashboard"><svg class="nav-svg" version="1.1"
                            id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;"
                            xml:space="preserve">
                            <g>
                                <path
                                    d="M184,448h48c4.4,0,8-3.6,8-8V72c0-4.4-3.6-8-8-8h-48c-4.4,0-8,3.6-8,8v368C176,444.4,179.6,448,184,448z" />
                                <path class="svg-primary"
                                    d="M88,448H136c4.4,0,8-3.6,8-8V296c0-4.4-3.6-8-8-8H88c-4.4,0-8,3.6-8,8V440C80,444.4,83.6,448,88,448z" />
                                <path class="svg-primary" d="M280.1,448h47.8c4.5,0,8.1-3.6,8.1-8.1V232.1c0-4.5-3.6-8.1-8.1-8.1h-47.8c-4.5,0-8.1,3.6-8.1,8.1v207.8
                                        C272,444.4,275.6,448,280.1,448z" />
                                <path d="M368,136.1v303.8c0,4.5,3.6,8.1,8.1,8.1h47.8c4.5,0,8.1-3.6,8.1-8.1V136.1c0-4.5-3.6-8.1-8.1-8.1h-47.8
                                        C371.6,128,368,131.6,368,136.1z" />
                            </g>
                        </svg> </a>
                    <!--end MetricaAnalytic--><a href="#SalesDiv" class="nav-link" data-toggle="tooltip-custom"
                        data-placement="top" title="" data-original-title="Sales"><svg class="nav-svg" version="1.1"
                            id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;"
                            xml:space="preserve">
                            <g>
                                <ellipse class="svg-primary"
                                    transform="matrix(0.9998 -1.842767e-02 1.842767e-02 0.9998 -7.7858 3.0205)" cx="160"
                                    cy="424" rx="24" ry="24" />
                                <ellipse class="svg-primary"
                                    transform="matrix(2.381651e-02 -0.9997 0.9997 2.381651e-02 -48.5107 798.282)"
                                    cx="384.5" cy="424" rx="24" ry="24" />
                                <path
                                    d="M463.8,132.2c-0.7-2.4-2.8-4-5.2-4.2L132.9,96.5c-2.8-0.3-6.2-2.1-7.5-4.7c-3.8-7.1-6.2-11.1-12.2-18.6
                                        c-7.7-9.4-22.2-9.1-48.8-9.3c-9-0.1-16.3,5.2-16.3,14.1c0,8.7,6.9,14.1,15.6,14.1c8.7,0,21.3,0.5,26,1.9c4.7,1.4,8.5,9.1,9.9,15.8
                                        c0,0.1,0,0.2,0.1,0.3c0.2,1.2,2,10.2,2,10.3l40,211.6c2.4,14.5,7.3,26.5,14.5,35.7c8.4,10.8,19.5,16.2,32.9,16.2h236.6
                                        c7.6,0,14.1-5.8,14.4-13.4c0.4-8-6-14.6-14-14.6H189h-0.1c-2,0-4.9,0-8.3-2.8c-3.5-3-8.3-9.9-11.5-26l-4.3-23.7
                                        c0-0.3,0.1-0.5,0.4-0.6l277.7-47c2.6-0.4,4.6-2.5,4.9-5.2l16-115.8C464,134,464,133.1,463.8,132.2z" />
                            </g>
                        </svg> </a>
                    <!--end Sales-->
                    <a href="#ComplaintsDiv" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title=""
                        data-original-title="Complaints">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path class="svg-primary" d="M256 48C141.6 48 48 141.601 48 256s93.6 208 
                        208 208 208-93.601 208-208S370.4 48 256 48zm24 312h-48v-40h48v40zm0-88h-48V144h48v128z">

                            </path>
                        </svg>
                    </a>
                    <!--end Complaints-->
                    <a href="#MetricaCrypto" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title=""
                        data-original-title="Branding and Marketing">
                        <i class="fas fa-bullhorn"></i></a>
                    <!--end MetricaCrypto-->
                    <a href="#DocumentsDiv" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title=""
                        data-original-title="Documents">
                        <svg class="nav-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path d="M70.7 164.5l169.2 81.7c4.4 2.1 10.3 3.2 16.1 
                        3.2s11.7-1.1 16.1-3.2l169.2-81.7c8.9-4.3 8.9-11.3 0-15.6L272.1 
                        67.2c-4.4-2.1-10.3-3.2-16.1-3.2s-11.7 1.1-16.1 3.2L70.7 148.9c-8.9 
                        4.3-8.9 11.3 0 15.6z">
                            </path>
                            <path class="svg-primary" d="M441.3 248.2s-30.9-14.9-35-16.9-5.2-1.9-9.5.1S272 
                    291.6 272 291.6c-4.5 2.1-10.3 3.2-16.1 3.2s-11.7-1.1-16.1-3.2c0 
                    0-117.3-56.6-122.8-59.3-6-2.9-7.7-2.9-13.1-.3l-33.4 16.1c-8.9 
                    4.3-8.9 11.3 0 15.6l169.2 81.7c4.4 2.1 10.3 3.2 16.1 3.2s11.7-1.1 16.1-3.2l169.2-81.7c9.1-4.2 
                    9.1-11.2.2-15.5z"></path>
                            <path d="M441.3 347.5s-30.9-14.9-35-16.9-5.2-1.9-9.5.1S272.1 391 
                    272.1 391c-4.5 2.1-10.3 3.2-16.1 3.2s-11.7-1.1-16.1-3.2c0 
                    0-117.3-56.6-122.8-59.3-6-2.9-7.7-2.9-13.1-.3l-33.4 16.1c-8.9 4.3-8.9 11.3 0 15.6l169.2 81.7c4.4 2.2 
                    10.3 3.2 16.1 3.2s11.7-1.1 16.1-3.2l169.2-81.7c9-4.3 9-11.3.1-15.6z"></path>
                        </svg></a>
                    <!--end Documents-->
                </nav>
                <!--end nav-->
            </div>
            <!--end main-icon-menu-->
            <div class="main-menu-inner">
                <div class="menu-body slimscroll">
                    <div id="MetricaAnalytic" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">Dashboard</h6>
                        </div>
                        <ul class="nav">
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/welcome"><i
                                        class="dripicons-meter"></i>Dashboard</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/promotions"><i
                                        class="dripicons-user-group"></i>Promotions</a></li>
                            <!-- <li class="nav-item"><a class="nav-link" href="../analytics/analytics-customers.html"><i
                                        class="dripicons-user-group"></i>Schemes</a></li>
                            <li class="nav-item"><a class="nav-link" href="../analytics/analytics-reports.html"><i
                                        class="dripicons-document"></i>Reports</a></li> -->
                        </ul>
                    </div><!-- end Analytic -->
                    <div id="MetricaCrypto" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">Branding and Marketing</h6>
                        </div>
                        <ul class="nav">

                            <li class="nav-item"><a class="nav-link" href="../crypto/crypto-exchange.html"><i
                                        class="dripicons-swap"></i>Branding Request</a></li>
                            <li class="nav-item"><a class="nav-link" href="../crypto/crypto-wallet.html"><i
                                        class="dripicons-wallet"></i>Training Request</a></li>
                            <!-- <li class="nav-item"><a class="nav-link" href="../crypto/crypto-calendar.html"><i
                                        class="dripicons-calendar"></i>History Orders</a></li> -->
                            <!-- <li class="nav-item"><a class="nav-link" href="../crypto/crypto-news.html"><i
                                        class="dripicons-blog"></i>Crypto News</a></li>
                            <li class="nav-item"><a class="nav-link" href="../crypto/crypto-ico.html"><i
                                        class="dripicons-stack"></i>ICO List</a></li>
                            <li class="nav-item"><a class="nav-link" href="../crypto/crypto-settings.html"><i
                                        class="dripicons-gear"></i>Settings</a></li> -->
                        </ul>
                    </div><!-- end Crypto -->

                    <div id="SalesDiv" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">Sales</h6>
                        </div>
                        <ul class="nav">
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/salesdashboard"><i
                                        class="dripicons-device-desktop"></i>Dashboard</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/products"><i
                                        class="dripicons-view-apps"></i>Products</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/orderform"><i
                                        class="dripicons-list"></i>Sales Order</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/shopping"><i
                                        class="dripicons-cart">Cart</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/invoicing"><i class="fas fa-receipt"></i>
                                    Invoicing</a></li>
                                    <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/history"><i
                                        class="fas fa-history"></i>History Orders</a></li>
                        </ul>
                    </div><!-- end Ecommerce -->
                    <div id="ComplaintsDiv" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">Complaints</h6>
                        </div>
                        <ul class="nav">
                        <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/complaintdashboard"><i
                                        class="dripicons-monitor"></i>Dashboard</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/productcomplaint"><i
                                        class="dripicons-user-id"></i>Product Complaints</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/quotecomplaint"><i
                                        class="dripicons-lightbulb"></i>Quotation Complaints</a></li>
                            <!-- <li class="nav-item"><a class="nav-link" href="crm-leads.html"><i
                                        class="dripicons-toggles"></i>  Application Complaints</a></li> -->
                            <li class="nav-item"><a class="nav-link"  href="<?php echo site_url()?>/ordercomplaint"><i
                                        class="dripicons-user-group"></i>Order Complaints</a></li>
                        </ul>
                    </div><!-- end CRM -->
                    <div id="DocumentsDiv" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">Documents</h6>
                        </div>
                        <ul class="nav">
                            <!-- <li class="nav-item"><a class="nav-link" href="crm-index.html"><i
                                        class="dripicons-monitor"></i>Dashboard</a></li> -->
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/pricelist"><i
                                        class="dripicons-user-id"></i>Price List</a></li>
                            <li class="nav-item"><a class="nav-link" href="crm-opportunities.html"><i
                                        class="dripicons-lightbulb"></i>Material Safety Datasheet</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/paintspecifier"><i
                                        class="dripicons-toggles"></i>Crown Paints Specifier</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/faqs"><i
                                        class="dripicons-user-group"></i>FAQ's</a></li>
                        </ul>
                    </div><!-- end CRM -->


                </div>
                <!--end menu-body-->
            </div><!-- end main-menu-inner-->
        </div><!-- end left-sidenav-->