<?php
$page = ucwords(strtolower(str_replace('_', ' ', $this->uri->segment(2))), "");
$page = ucwords($page);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Crown Paints - Dealer Portal</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta content="A premium admin dashboard template by Mannatthemes" name="description">
    <meta content="Crown Paints" name="author"><!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/logos/crownpaints.png">
    <script type="text/javascript" src="https://a.opmnstr.com/app/js/api.min.js" data-account="76228" data-user="67769" async></script>
    <link href="<?php echo base_url() ?>assets/plugins/ticker/jquery.jConveyorTicker.css" rel="stylesheet"
        type="text/css">

    <link href="<?php echo base_url() ?>assets/plugins/jvectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet">
    <!-- App css -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet"
        type="text/css">
    <link href="<?php echo base_url() ?>assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/metisMenu.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/fonts/materialdesignicons-webfont.woff2?v=3.2.89" rel="stylesheet"
        type="text/css">
	<link href="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
	<!-- Responsive datatable examples -->
	<link href="<?php echo base_url() ?>assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css"><!-- App css -->
	<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>/assets/css/jquery-ui.min.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>/assets/css/icons.min.css" rel="stylesheet" type="text/css">

	<link href="<?php echo base_url() ?>/assets/css/app.min.css" rel="stylesheet" type="text/css">

</head>
<style type="text/css">
.apexcharts-legend {
    display: flex;
    overflow: auto;
    padding: 0 10px;
}

.apexcharts-legend.position-bottom,
.apexcharts-legend.position-top {
    flex-wrap: wrap
}

.apexcharts-legend.position-right,
.apexcharts-legend.position-left {
    flex-direction: column;
    bottom: 0;
}

.apexcharts-legend.position-bottom.left,
.apexcharts-legend.position-top.left,
.apexcharts-legend.position-right,
.apexcharts-legend.position-left {
    justify-content: flex-start;
}

.apexcharts-legend.position-bottom.center,
.apexcharts-legend.position-top.center {
    justify-content: center;
}

.apexcharts-legend.position-bottom.right,
.apexcharts-legend.position-top.right {
    justify-content: flex-end;
}

.apexcharts-legend-series {
    cursor: pointer;
    line-height: normal;
}

.apexcharts-legend.position-bottom .apexcharts-legend-series,
.apexcharts-legend.position-top .apexcharts-legend-series {
    display: flex;
    align-items: center;
}

.apexcharts-legend-text {
    position: relative;
    font-size: 14px;
}

.apexcharts-legend-text *,
.apexcharts-legend-marker * {
    pointer-events: none;
}

.apexcharts-legend-marker {
    position: relative;
    display: inline-block;
    cursor: pointer;
    margin-right: 3px;
}

.apexcharts-legend.right .apexcharts-legend-series,
.apexcharts-legend.left .apexcharts-legend-series {
    display: inline-block;
}

.apexcharts-legend-series.no-click {
    cursor: auto;
}

.apexcharts-legend .apexcharts-hidden-zero-series,
.apexcharts-legend .apexcharts-hidden-null-series {
    display: none !important;
}

.inactive-legend {
    opacity: 0.45;
}
</style>

<body class="enlarge-menu" data-gr-c-s-loaded="true">
    <!-- Top Bar Start -->
    <div class="topbar">
        <!-- LOGO -->
        <div class="topbar-left"><a href="crm-index.html" class="logo"><span>
                    <!-- <img src="<?php echo base_url() ?>assets/images/logo-sm.png" alt="logo-small" class="logo-sm"> </span><span> -->
                    <img style="height: 5vh;width: 7vh" src="<?php echo base_url() ?>assets/images/logos/crownpaints.png" alt="logo-large"
                        class="logo-lg"></span></a></div>
        <!--end logo-->
        <!-- Navbar -->
        <nav class="navbar-custom">
            <ul class="list-unstyled topbar-nav float-right mb-0">
                <li class="hidden-sm">
                    <div class="crypto-balance">
                    <i class="dripicons-wallet text-muted align-self-center"></i>
                        <div class="btc-balance">
                            <h5 class="m-0">9.521.32 <span>ACC</span></h5><span class="text-muted">Account Balance</span>
                        </div>
                    </div>
                </li>
                <li class="hidden-sm"><a class="nav-link dropdown-toggle waves-effect waves-light"
                        data-toggle="dropdown" href="javascript: void(0);" role="button" aria-haspopup="false"
                        aria-expanded="false"></a>

                </li>
                <li class="dropdown notification-list"><a
                        class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown"
                        href="crm-index.html#" role="button" aria-haspopup="false" aria-expanded="false"><i
                            class="dripicons-cart noti-icon"></i> <span
                            class="badge badge-danger badge-pill noti-icon-badge">2</span></a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-lg">
                        <!-- item-->
                        <!-- <h6 class="dropdown-item-text">Cart (18)</h6> -->

                </li>
                <li class="dropdown"><a class="nav-link dropdown-toggle waves-effect waves-light nav-user"
                        data-toggle="dropdown" href="crm-index.html#" role="button" aria-haspopup="false"
                        aria-expanded="false"><img src="<?php echo base_url() ?>assets/images/users/avatar.jpg"
                            alt="profile-user" class="rounded-circle"> <span class="ml-1 nav-user-name hidden-sm">Dealer
                            <i class="mdi mdi-chevron-down"></i></span></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <!-- <a class="dropdown-item" href="crm-index.html#"><i
                                class="dripicons-user text-muted mr-2"></i> Profile</a> 
                                <a class="dropdown-item"
                            href="crm-index.html#"><i class="dripicons-wallet text-muted mr-2"></i> My Wallet</a> 
                            <a
                            class="dropdown-item" href="crm-index.html#"><i class="dripicons-gear text-muted mr-2"></i>
                            Settings</a> 
                            <a class="dropdown-item" href="crm-index.html#"><i
                                class="dripicons-lock text-muted mr-2"></i> Lock screen</a> -->
                        <a class="dropdown-item" href="<?php echo site_url()?>/Auth/signout"><i
                                class="dripicons-exit text-muted mr-2"></i> Logout</a>
                    </div>
                </li>
            </ul>
            <!--end topbar-nav-->
            <ul class="list-unstyled topbar-nav mb-0">
                <li><button class="button-menu-mobile nav-link waves-effect waves-light"><i
                            class="dripicons-menu nav-icon"></i></button></li>
                <li class="hide-phone app-search">
                    <form role="search" class=""><input type="text" placeholder="Search..." class="form-control"> <a
                            href="crm-index.html"><i class="fas fa-search"></i></a></form>
                </li>
            </ul>
        </nav><!-- end navbar-->
    </div><!-- Top Bar End -->
    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <div class="left-sidenav">
            <div class="main-icon-menu">
                <nav class="nav">
					<a href="#MetricaAnalytic" class="nav-link" data-toggle="tooltip-custom"
                        data-placement="top" title="" data-original-title="Dashboard">
						<i style="font-size: 25px" class="ti ti-dashboard"></i>
					</a>
                    <a href="#SalesDiv" class="nav-link" data-toggle="tooltip-custom"
                        data-placement="top" title="" data-original-title="Sales Order">
						<i style="font-size: 25px" class="ti ti-shopping-cart"></i>
					 </a>
					<a href="#promotionDiv" class="nav-link" data-toggle="tooltip-custom"
					   data-placement="top" title="" data-original-title="Promotion Scheme">
						<i style="font-size: 25px" class="ti ti-info-alt"></i>
					</a>
					<a href="#products_Div" class="nav-link" data-toggle="tooltip-custom"
					   data-placement="top" title="" data-original-title="Products">
						<i style="font-size: 25px" class="ti ti-list"></i>
					</a>
					<a href="#marketingDiv" class="nav-link" data-toggle="tooltip-custom"
					   data-placement="top" title="" data-original-title="Marketing">
						<i style="font-size: 25px" class="ti ti-rss"></i>
					</a>
					<a href="#trainingDiv" class="nav-link" data-toggle="tooltip-custom"
					   data-placement="top" title="" data-original-title="Training Request">
						<i style="font-size: 25px" class="ti ti-check-box"></i>
					</a>
                    <!--end Sales-->
					<a href="#teamkubwaDiv" class="nav-link" data-toggle="tooltip-custom"
					   data-placement="top" title="" data-original-title="Team Kubwa">
						<i style="font-size: 25px" class="ti ti-announcement"></i>
					</a>
					<!--end Team kubwa-->
					<a href="#Cys_div" class="nav-link" data-toggle="tooltip-custom"
					   data-placement="top" title="" data-original-title="CYS">
						<i style="font-size: 25px" class="ti ti-info-alt"></i>
					</a>

                    <a href="#ComplaintsDiv" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title=""
                        data-original-title="Complaints">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path class="svg-primary" d="M256 48C141.6 48 48 141.601 48 256s93.6 208 
                        208 208 208-93.601 208-208S370.4 48 256 48zm24 312h-48v-40h48v40zm0-88h-48V144h48v128z">

                            </path>
                        </svg>
                    </a>

                    <!--end Complaints-->
                    <!--end MetricaCrypto-->
                    <a href="#DocumentsDiv" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title=""
                        data-original-title="Documents">
                        <svg class="nav-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path d="M70.7 164.5l169.2 81.7c4.4 2.1 10.3 3.2 16.1 
                        3.2s11.7-1.1 16.1-3.2l169.2-81.7c8.9-4.3 8.9-11.3 0-15.6L272.1 
                        67.2c-4.4-2.1-10.3-3.2-16.1-3.2s-11.7 1.1-16.1 3.2L70.7 148.9c-8.9 
                        4.3-8.9 11.3 0 15.6z">
                            </path>
                            <path class="svg-primary" d="M441.3 248.2s-30.9-14.9-35-16.9-5.2-1.9-9.5.1S272 
                    291.6 272 291.6c-4.5 2.1-10.3 3.2-16.1 3.2s-11.7-1.1-16.1-3.2c0 
                    0-117.3-56.6-122.8-59.3-6-2.9-7.7-2.9-13.1-.3l-33.4 16.1c-8.9 
                    4.3-8.9 11.3 0 15.6l169.2 81.7c4.4 2.1 10.3 3.2 16.1 3.2s11.7-1.1 16.1-3.2l169.2-81.7c9.1-4.2 
                    9.1-11.2.2-15.5z"></path>
                            <path d="M441.3 347.5s-30.9-14.9-35-16.9-5.2-1.9-9.5.1S272.1 391 
                    272.1 391c-4.5 2.1-10.3 3.2-16.1 3.2s-11.7-1.1-16.1-3.2c0 
                    0-117.3-56.6-122.8-59.3-6-2.9-7.7-2.9-13.1-.3l-33.4 16.1c-8.9 4.3-8.9 11.3 0 15.6l169.2 81.7c4.4 2.2 
                    10.3 3.2 16.1 3.2s11.7-1.1 16.1-3.2l169.2-81.7c9-4.3 9-11.3.1-15.6z"></path>
                        </svg></a>
                    <!--end Documents-->
                </nav>
                <!--end nav-->
            </div>
            <!--end main-icon-menu-->
            <div class="main-menu-inner">
                <div class="menu-body slimscroll">
                    <div id="MetricaAnalytic" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">Dashboard</h6>
                        </div>
                        <ul class="nav">
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/dashboard"><i
                                        class="dripicons-meter"></i>Dashboard</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/promotions"><i
                                        class="ti-arrow-circle-right"></i>Promotions</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/dealer_profile"><i
										class="dripicons-user-group"></i>Dealer Profile</a></li>

                        </ul>
                    </div>

                    <div id="MetricaCrypto" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">Branding and Marketing</h6>
                        </div>
                        <ul class="nav">

                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/branding"><i class="dripicons-swap"></i>Branding
                                    Request</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/training"><i class="dripicons-wallet"></i>Training
                                    Request</a></li>

                        </ul>
                    </div><!-- end Crypto -->
					<div id="promotionDiv" class="main-icon-menu-pane">
						<div class="title-box">
							<h6 class="menu-title">Promotion Scheme</h6>
						</div>
						<ul class="nav">

							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/privilege_club_ppt"><i
										class="dripicons-user-id"></i>Privilege Club PPT</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/privilege_club"><i
										class="dripicons-toggles"></i>Privilege Clubs</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/ongoing_scheme"><i
										class="dripicons-pulse"></i>Ongoing Scheme Status</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/promotional_slab"><i
										class="dripicons-brightness-medium"></i>Promotional / Slab item status</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/scheme_circular"><i
										class="dripicons-basket"></i>All Scheme Circulars</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/privilege_faq"><i
										class="dripicons-user-group"></i>FAQ's</a></li>
						</ul>
					</div>
                    <div id="SalesDiv" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">Sales Order</h6>
                        </div>
                        <ul class="nav">
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/sales_dashboard"><i
                                        class="dripicons-device-desktop"></i>Dashboard</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/account_Statement"><i
                                        class="dripicons-card"></i>Account Statement</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/Products"><i
                                        class="dripicons-view-apps"></i>Products</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/order_Form"><i
                                        class="dripicons-list"></i>Sales Order</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/cart"><i
                                        class="dripicons-cart"></i>Cart</a></li>

                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/invoice"><i
                                        class="dripicons-document"></i>
                                    Invoicing</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/history"><i
                                        class="dripicons-swap"></i>History Orders</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Accounts/credit_note"><i
										class="dripicons-contract"></i>Credit Note</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Accounts/debit_note"><i
										class="dripicons-enter"></i>Debit Note</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Accounts/invoice_summary"><i
										class="dripicons-meter"></i>Invoice Summary</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Accounts/faq"><i
										class="dripicons-user-group"></i>FAQ's</a></li>

                        </ul>
                    </div><!-- end Ecommerce -->
					<div id="products_Div" class="main-icon-menu-pane">
						<div class="title-box">
							<h6 class="menu-title">Products</h6>
						</div>
						<ul class="nav">
							<!-- <li class="nav-item"><a class="nav-link" href="crm-index.html"><i
										class="dripicons-monitor"></i>Dashboard</a></li> -->
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Price/products"><i
										class="dripicons-menu"></i>Products</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Price/price_list"><i
										class="dripicons-checklist"></i>Price List</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Price/faq"><i
										class="dripicons-contract"></i>Faq</a></li>

						</ul>
					</div>
					<div id="marketingDiv" class="main-icon-menu-pane">
						<div class="title-box">
							<h6 class="menu-title">Marketing</h6>
						</div>
						<ul class="nav">
							<!-- <li class="nav-item"><a class="nav-link" href="crm-index.html"><i
										class="dripicons-monitor"></i>Dashboard</a></li> -->
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Marketing/promotional_items"><i
										class="dripicons-menu"></i>Promotional Items</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Marketing/promotional_items_request"><i
										class="dripicons-export"></i>Promotional Items Request</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Marketing/branding"><i
										class="dripicons-broadcast"></i>Branding</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Marketing/faq"><i
										class="dripicons-contract"></i>Faq</a></li>

						</ul>
					</div>
					<div id="trainingDiv" class="main-icon-menu-pane">
						<div class="title-box">
							<h6 class="menu-title">Training Request</h6>
						</div>
						<ul class="nav">
							<!-- <li class="nav-item"><a class="nav-link" href="crm-index.html"><i
										class="dripicons-monitor"></i>Dashboard</a></li> -->
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Training/category"><i
										class="dripicons-menu"></i>Category</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Training/training_venue"><i
										class="dripicons-export"></i>Training Venue </a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Training/factory_visit_request"><i
										class="dripicons-phone"></i>Factory Visit Request</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Training/Products_TNA"><i
										class="dripicons-broadcast"></i>Products to be Trained (TNA)</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Training/online_training"><i
										class="dripicons-camcorder"></i>On-line training videos/tutorials</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Training/faq"><i
										class="dripicons-contract"></i>FAQs</a></li>
						</ul>
					</div>
					<div id="teamkubwaDiv" class="main-icon-menu-pane">
						<div class="title-box">
							<h6 class="menu-title">Team Kubwa</h6>
						</div>
						<ul class="nav">
							<!-- <li class="nav-item"><a class="nav-link" href="crm-index.html"><i
										class="dripicons-monitor"></i>Dashboard</a></li> -->
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Tkb/painters_list"><i
										class="dripicons-menu"></i>List of painters</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Tkb/product_chart"><i
										class="dripicons-export"></i>Product and Point chart </a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Tkb/banking_process"><i
										class="dripicons-phone"></i>Banking process</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Tkb/redemption_table"><i
										class="dripicons-broadcast"></i>Redemption Table (with process)</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Tkb/faq"><i
										class="dripicons-contract"></i>FAQs</a></li>
						</ul>
					</div>
					<div id="Cys_div" class="main-icon-menu-pane">
						<div class="title-box">
							<h6 class="menu-title">CYS</h6>
						</div>
						<ul class="nav">
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Cys/ppt"><i
										class="dripicons-menu"></i>PPT</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Cys/videos"><i
										class="dripicons-export"></i>Videos </a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Cys/agreement"><i
										class="dripicons-broadcast"></i>Agreement</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Cys/customers_price_list"><i
										class="dripicons-contract"></i>Price List for Customers</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Cys/purchase_price_list"><i
										class="dripicons-cart"></i>Product Purchase Price List</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Cys/leaflets_and_bronchures"><i
										class="dripicons-document"></i>Leaflets and Bronchures</a></li>


							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Cys/questionare"><i
										class="dripicons-pencil"></i>Questionare</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Cys/value_sales"><i
										class="dripicons-meter"></i>Value Sales</a></li>

							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Cys/faq"><i
										class="dripicons-phone"></i>FAQ</a></li>
						</ul>
					</div>
                    <div id="ComplaintsDiv" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">Complaints</h6>
                        </div>
                        <ul class="nav">
                            <li class="nav-item"><a class="nav-link"
                                    href="<?php echo site_url()?>/Dealer/complaint_dashboard"><i
                                        class="dripicons-monitor"></i>Dashboard</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/product_complaint"><i
                                        class="dripicons-user-id"></i>Product Complaints</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/quote_complaint"><i
                                        class="dripicons-lightbulb"></i>Quotation Complaints</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/order_complaint"><i
                                        class="dripicons-user-group"></i>Order Complaints</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/cvp_questionare"><i
										class="dripicons-document-edit"></i>CVP Questionnaire</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/customer_satisfaction_survey"><i
										class="dripicons-pencil"></i>Customer Satisfaction Survey</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/retail_audit"><i
										class="dripicons-document-new"></i>Retail Audit Format</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Dealer/faq"><i
										class="dripicons-user-group"></i>FAQ</a></li>
                        </ul>
                    </div><!-- end CRM -->
                    <div id="DocumentsDiv" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">Reports</h6>
                        </div>
                        <ul class="nav">
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Reports/value_sales"><i
                                        class="dripicons-user-id"></i>Value Sales</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Reports/volume_sales"><i
                                        class="dripicons-toggles"></i>Volume Sales</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url()?>/Reports/product_category_level"><i
                                        class="dripicons-copy"></i>Product Category Level </a></li>
                        </ul>

                    </div><!-- end CRM -->


                </div>
                <!--end menu-body-->
            </div><!-- end main-menu-inner-->
        </div><!-- end left-sidenav-->
		<div class="page-content">
			<div class="container-fluid">
				<!-- Page-Title -->
				<div class="row">
					<div class="col-sm-12">
						<div class="page-title-box">
							<div class="float-right">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="javascript:void(0);">Dealer Portal</a></li>
									<li class="breadcrumb-item"><a href="javascript:void(0);"><?php echo $page; ?></a></li>

								</ol>
							</div>

							&nbsp;<h4 style="margin-left: 12px" class="page-title"><?php echo $page ?></h4>
						</div>
						<!--end page-title-box-->
					</div>
					<!--end col-->
				</div><!-- end page title end breadcrumb -->
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
								<div class="wrap">
									<!-- <div class="jctkr-label"><span><i class="fas fa-exchange-alt mr-2"></i>Change 24
											Hours</span></div> -->
									<div class="js-conveyor-example jctkr-wrapper jctkr-initialized">
										<ul style="width: 2295px; left: -329.025px;">
											<li style=""><img src="<?php echo base_url()?>/assets/images/coins/btc.png" alt=""
															  class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Account Limit:
                                                8435.21</b></span>
											</li>
											<li style=""><img src="<?php echo base_url()?>/assets/images/coins/dash.png" alt=""
															  class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Account Balance:
                                                1233.54</b></span>
											</li>

											<li style=""><img src="<?php echo base_url()?>/assets/images/coins/btc.png" alt=""
															  class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Credit Limit:
                                                224446.64</b></span>
											</li>
											<li style=""><img src="<?php echo base_url()?>/assets/images/coins/lite.png" alt=""
															  class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Commitment Limit:
                                                129999.91</b></span>
											</li>

										</ul>
									</div>
								</div>
							</div>
							<!--end card-body-->
						</div>
						<!--end card-->
					</div>

					<!--end col-->
				</div>
