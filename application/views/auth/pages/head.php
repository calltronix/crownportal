<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Crown Paints || Dealer Portal</title>
	<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
	<meta content="Premium Multipurpose Admin & Dashboard Template" name="description">
	<meta content="" name="author">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App favicon -->
	<link rel="shortcut icon" href="<?php echo base_url() ?>assets/front/images/favicon.ico"><!-- App css -->
	<link href="<?php echo base_url() ?>assets/front/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/front/css/jquery-ui.min.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/css/front/icons.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/front/metisMenu.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>assets/css/front/app.min.css" rel="stylesheet" type="text/css">
</head>
