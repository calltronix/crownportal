<div class="container-fluid">
	<!-- Page-Title -->
	<!--end row-->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<button type="button"
							class="btn btn-primary waves-effect waves-light float-right mb-3" data-toggle="modal"
							data-animation="bounce" data-target=".bs-example-modal-lg">+ Add New Request
					</button>
					<h4 class="header-title mt-0 mb-3">All Other Requests</h4>
					<div class="table-responsive dash-social">
						<div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

							<div class="row">
								<div class="col-sm-12">
									<table id="datatable" class="table dataTable no-footer" role="grid"
										   aria-describedby="datatable_info">
										<thead class="thead-light">
										<tr role="row">
											<th>Category</th>
											<th>Width</th>
											<th>Height</th>
											<th>Proposed Date</th>
										</tr>
										<!--end tr-->
										</thead>
										<tbody>

										<tr>
											<td>Wall Branding</td>
											<td>35</td>
											<td>32</td>
											<td>2020-01-26
											</td>
										</tr>
										<tr>
											<td>Signage</td>
											<td>36</td>
											<td>35</td>
											<td>2020-02-23
											</td>
										</tr>

										</tbody>
									</table>
								</div>
							</div>

						</div>
					</div>
				</div>
				<!--end card-body-->
			</div>
			<!--end card-->
		</div>
		<!--end col-->
	</div>
	<!--end row-->
</div><!-- container -->
<!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
	 aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title mt-0" id="myLargeModalLabel">Add New Branding Request</h5>
				<button type="button"
						class="close" data-dismiss="modal" aria-hidden="true">×
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="example-input1-group1">Category</label>
								<div class="input-group">
									<div class="input-group-prepend"></div>
									<select required class="form-control">
										<option>choose</option>
										<option>Poster</option>
										<option>Dangler</option>
										<option>Roll up Standee</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group"><label for="status-select" class="mr-2">Width</label>
								<input type="number"  class="form-control">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group"><label for="status-select" class="mr-2">Height</label>
								<input type="number"  class="form-control">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group"><label for="status-select" class="mr-2">Proposed Date</label>
								<input type="date"  class="form-control">
							</div>
						</div>

					</div>



					<button type="button" class="btn btn-sm btn-primary">Save</button>
					<button type="button"
							class="btn btn-sm btn-danger">Delete
					</button>
				</form>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
