<div class="container-fluid">
	<!-- Page-Title -->
	<!--end row-->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<button type="button"
							class="btn btn-primary waves-effect waves-light float-right mb-3" data-toggle="modal"
							data-animation="bounce" data-target=".bs-example-modal-lg">+ Add New Request
					</button>
					<h4 class="header-title mt-0 mb-3">All Full Price Requests</h4>
					<div class="table-responsive dash-social">
						<div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

							<div class="row">
								<div class="col-sm-12">
									<table id="datatable" class="table dataTable no-footer" role="grid"
										   aria-describedby="datatable_info">
										<thead class="thead-light">
										<tr role="row">
											<th>Promo Item</th>
											<th>Item Code</th>
											<th>Quantity</th>
											<th>Color</th>
											<td>Date</td>
										</tr>
										<!--end tr-->
										</thead>
										<tbody>

										<tr role="row" class="odd">
											<td>Duco NC</td>
											<td>5183CGH</td>
											<td>5 Litres</td>
											<td>Blue</td>
											<td>2020-01-26</td>
										</tr>
										<tr role="row" class="even">
											<td>Duxone</td>
											<td>4DER45</td>
											<td>3 Litres</td>
											<td>Light Blue</td>
											<td>2020-02-23
											</td>
										</tr>

										</tbody>
									</table>
								</div>
							</div>

						</div>
					</div>
				</div>
				<!--end card-body-->
			</div>
			<!--end card-->
		</div>
		<!--end col-->
	</div>
	<!--end row-->
</div><!-- container -->
<!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
	 aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title mt-0" id="myLargeModalLabel">Add New Full Price Request</h5>
				<button type="button"
						class="close" data-dismiss="modal" aria-hidden="true">×
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="example-input1-group1"> Promotional Item</label>
								<div class="input-group">
									<div class="input-group-prepend"></div>
									<select required class="form-control">
										<option>choose</option>
										<option>Item 1</option>
										<option>Item 2 </option>
									</select>
								</div>
							</div>

						</div>
						<div class="col-md-12">
							<div class="form-group"><label for="status-select" class="mr-2">Item Code </label>
								<input type="text" class="form-control">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group"><label for="status-select" class="mr-2">Quantity</label>
								<input type="text" class="form-control">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group"><label for="status-select" class="mr-2">Color</label>
								<input type="text" class="form-control">
							</div>
						</div>

					</div>


					<button type="button" class="btn btn-sm btn-primary">Save</button>
					<button type="button"
							class="btn btn-sm btn-danger">Delete
					</button>
				</form>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
