<div class="page-content">
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="float-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Dealer Portal</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Branding and Marketing</a></li>
                            <li class="breadcrumb-item active">Training Request</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Training Request</h4>
                </div>
                <!--end page-title-box-->
            </div>
            <!--end col-->
        </div><!-- end page title end breadcrumb -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="wrap">
                            <!-- <div class="jctkr-label"><span><i class="fas fa-exchange-alt mr-2"></i>Change 24
                                    Hours</span></div> -->
                            <div class="js-conveyor-example jctkr-wrapper jctkr-initialized">
                                <ul style="width: 2295px; left: -329.025px;">
                                    <li style=""><img src="../assets/images/coins/btc.png" alt=""
                                            class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Account Limit:
                                                8435.21</b></span>
                                    </li>
                                    <li style=""><img src="../assets/images/coins/dash.png" alt=""
                                            class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Account Balance:
                                                1233.54</b></span>
                                    </li>

                                    <li style=""><img src="../assets/images/coins/btc.png" alt=""
                                            class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Credit Limit:
                                                224446.64</b></span>
                                    </li>
                                    <li style=""><img src="../assets/images/coins/lite.png" alt=""
                                            class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Commitment
                                                Limit:
                                                129999.91</b></span>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->
            </div>
            <!--end col-->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Training Request Form</h4>

                        <form>
                            <!-- <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-input1-group1">Branding Request</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="fa fa-hashtag"></i></span></div><input type="number"
                                                id="example-input1-group1" name="example-input1-group1"
                                                class="form-control" placeholder="OrderID">
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="row">
                            <div class ="col-md-6">
                                <div class="form-group row">
                                <label for="example-input1-group1">Training Personnel</label>
                                    <div class="col-md-9">
                                        <div class="checkbox my-2">
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input" id="customCheck02"
                                                    data-parsley-multiple="groups" data-parsley-mincheck="2"> <label
                                                    class="custom-control-label" for="customCheck02">Carpenters</label>
                                            </div>
                                        </div>
                                        <div class="checkbox my-2">
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input" checked="" id="customCheck3"
                                                    data-parsley-multiple="groups" data-parsley-mincheck="2"> <label
                                                    class="custom-control-label" for="customCheck3">Mason</label>
                                            </div>
                                        </div>
                                        <div class="checkbox my-2">
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input" id="customCheck4"
                                                    data-parsley-multiple="groups" data-parsley-mincheck="2"> <label
                                                    class="custom-control-label" for="customCheck4">Counter
                                                    Staff</label>
                                            </div>
                                        </div>
                                        <div class="checkbox my-2">
                                            <div class="custom-control custom-checkbox"><input type="checkbox"
                                                    class="custom-control-input" id="customCheck5"
                                                    data-parsley-multiple="groups" data-parsley-mincheck="2"> <label
                                                    class="custom-control-label" for="customCheck5">Others</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-input1-group1">No of Training Personnel</label>
                                        <input class="form-control" type="number" id="example-number-input">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="message">Description</label>
                                        <textarea class="form-control" rows="5" id="description"></textarea>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-danger">Cancel</button>
                        </form>
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->
            </div>
        </div>
        <!--end row-->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mt-0 mb-3">Branding Requests</h4>
                        <div class="table-responsive dash-social">
                            <div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="datatable" class="table dataTable no-footer" role="grid"
                                            aria-describedby="datatable_info">
                                            <thead class="thead-light">
                                                <tr role="row">

                                                    <th class="sorting" tabindex="0" aria-controls="datatable"
                                                        rowspan="1" colspan="1"
                                                        aria-label="Email: activate to sort column ascending"
                                                        style="width: 144px;">Request ID</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable"
                                                        rowspan="1" colspan="1"
                                                        aria-label="Email: activate to sort column ascending"
                                                        style="width: 144px;">Training Personnel</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable"
                                                        rowspan="1" colspan="1"
                                                        aria-label="Status: activate to sort column ascending"
                                                        style="width: 87px;">Status</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable"
                                                        rowspan="1" colspan="1"
                                                        aria-label="Action: activate to sort column ascending"
                                                        style="width: 82px;">Created At</th>
                                                </tr>
                                                <!--end tr-->
                                            </thead>
                                            <tbody>

                                                <tr role="row" class="odd">

                                                    <td>0001</td>
                                                    <td>Carpenter</td>
                                                    <td><span class="badge badge-soft-purple">Processing</span></td>
                                                    <td>2020-01-26
                                                    </td>
                                                </tr>
                                                <tr role="row" class="even">
                                                    <td>0002</td>
                                                    <td>Mason</td>
                                                    <td><span class="badge badge-soft-purple">Processing</span></td>
                                                    <td>2020-01-26
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div><!-- container -->