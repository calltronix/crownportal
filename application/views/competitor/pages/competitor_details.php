<div class="row">
	<div class="col-lg-8">
		<div class="card">
			<div class="card-body">
				<button type="button"
						class="btn btn-gradient-primary waves-effect waves-light float-right mb-3"
						data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-lg">+ Create
					New Record
				</button>
				<h4 class="header-title mt-0 mb-3">Comparison Details</h4>
				<div class="table-responsive dash-social">
					<div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

						<div class="row">
							<div class="col-sm-12">
								<table id="datatable" class="table dataTable no-footer" role="grid"
									   aria-describedby="datatable_info">
									<thead class="thead-light">
									<tr role="row">
										<th>Competitor Name
										</th>
										<th>Price
										</th>
										<th>Pack Size
										</th>
										<th>Packaging
										</th>
										<th>Shade
										</th>
										<th>Creation Date
										</th>

									</tr>
									<!--end tr-->
									</thead>
									<tbody>

									<tr role="row" class="odd">
										<td>BASCO</td>
										<td>3240</td>
										<td>4 Litres</td>
										<td>Container</td>
										<td>SUPER GLOSS ALKYD BASE PASTEL</td>
										<td>2020-01-26</td>

									</tr>
									<tr role="row" class="odd">
										<td>Sadolin</td>
										<td>3140</td>
										<td>3.5 Litres</td>
										<td>Container</td>
										<td>SUPER GLOSS ALKYD BASE PASTEL</td>
										<td>2020-01-26</td>

									</tr>

									</tbody>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>
			<!--end card-body-->
		</div>
		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
			 aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header"><h5 class="modal-title mt-0"
												  id="myLargeModalLabel">Create New
							Record</h5>
						<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">×
						</button>
					</div>
					<div class="modal-body">
						<form>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group"><label for="Location">Competitor Name </label>
										<input type="text" class="form-control"
											   id="Location" required=""></div>
								</div>
								<div class="col-md-6">
									<div class="form-group"><label for="PhoneNo">Price
										</label> <input type="text"
														class="form-control"
														id="PhoneNo" required="">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group"><label for="Location">Pack Size</label>
										<input type="text" class="form-control"
											   id="Location" required=""></div>
								</div>
								<div class="col-md-6">
									<div class="form-group"><label for="PhoneNo">Packaging
										</label> <input type="text"
														class="form-control"
														id="PhoneNo" required="">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group"><label for="Location">Quality</label>
										<input type="text" class="form-control"
											   id="Location" required=""></div>
								</div>
								<div class="col-md-6">
									<div class="form-group"><label for="PhoneNo">Shade
										</label> <input type="text"
														class="form-control"
														id="PhoneNo" required="">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group"><label for="Location">Any ongoing Scheme</label>
										<input type="text" class="form-control"
											   id="Location" required=""></div>
								</div>
								<div class="col-md-6">
									<div class="form-group"><label for="PhoneNo">Any Activities
										</label> <input type="text"
														class="form-control"
														id="PhoneNo" required="">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group"><label for="Location">Upload Pictures</label>
										<input type="file" class="form-control"
											   id="Location" required=""></div>
								</div>
								<div class="col-md-6">
									<div class="form-group"><label for="PhoneNo">Remarks
										</label>
										<textarea class="form-control">

										</textarea>

									</div>
								</div>
							</div>

							<button type="button" class="btn btn-sm btn-primary">
								Save
							</button>
							<button type="button" class="btn btn-sm btn-danger">
								Delete
							</button>
						</form>
					</div>
				</div><!-- /.modal-content --></div><!-- /.modal-dialog --></div>
		<!--end card-->
	</div>

	<div class="col-lg-4">
		<div class="card carousel-bg-img">
			<div class="card-body dash-info-carousel">
				<h4 class="mt-0 header-title">New Products</h4>
				<div id="carousel_2" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="media"><img src="<?php echo base_url() ?>/assets/images/min_1.png" height="400"
													class="mr-4" alt="...">

							</div>
						</div>
						<div class="carousel-item">
							<div class="media"><img src="<?php echo base_url() ?>/assets/images/min_2.jpg" style="height: 400px;width: 400px!important;" class="mr-4" alt="...">

							</div>
						</div>

					</div><a class="carousel-control-prev" href="#carousel_2" role="button"
							 data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span> </a><a class="carousel-control-next"
																	 href="#carousel_2" role="button" data-slide="next"><span
								class="carousel-control-next-icon" aria-hidden="true"></span> <span
								class="sr-only">Next</span></a>
				</div>
			</div>
			<!--end card-body-->
		</div>
	</div>
	<!--end col-->
</div>
