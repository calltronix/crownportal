

<div class="row">
	<div class="col-lg-8">
		<div class="card">
			<div class="card-body">
				<button type="button"
						class="btn btn-gradient-primary waves-effect waves-light float-right mb-3"
						data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-lg">+ Add
Painter				</button>
				<h4 class="header-title mt-0 mb-3">All Painters</h4>
				<div class="table-responsive dash-social">
					<div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

						<div class="row">
							<div class="col-sm-12">
								<table id="datatable" class="table dataTable no-footer" role="grid"
									   aria-describedby="datatable_info">
									<thead class="thead-light">
									<tr role="row">
										<th >Member ID</th>
										<th>Full Names</th>
										<th> Status</th>
										<th>Action </th>
									</tr>
									<!--end tr-->
									</thead>
									<tbody>

									<tr role="row" class="odd">
										<td class="sorting_1">1345678
										</td>
										<td>Peter Marangi</td>

										<td><span class="badge badge-soft-purple">Active</span></td>
										<td><a href="" class="mr-2"><i
													class="fas fa-edit text-info font-16"></i></a> <a
												href=""><i
													class="fas fa-trash-alt text-danger font-16"></i></a></td>

									</tr>
									<tr role="row" class="even">
										<td class="sorting_1">2345678</td>
										<td>John Doe</td>
										<td><span class="badge badge-soft-primary">Active</span></td>
										<td><a href="#" class="mr-2"><i
													class="fas fa-edit text-info font-16"></i></a> <a
												href="#"><i
													class="fas fa-trash-alt text-danger font-16"></i></a></td>

									</tr>

									</tbody>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>
			<!--end card-body-->
		</div>
		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
			 aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header"><h5 class="modal-title mt-0"
												  id="myLargeModalLabel">Add New
							Painter</h5>
						<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">×
						</button>
					</div>
					<div class="modal-body">
						<form>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group"><label for="Location">Member ID </label>
										<input type="number" min="0" class="form-control"
											   id="Location" required=""></div>
								</div>
								<div class="col-md-12">
									<div class="form-group"><label for="PhoneNo">First Name
										</label> <input type="text"
														class="form-control"
														id="PhoneNo" required="">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group"><label for="PhoneNo">Last Name
										</label> <input type="text"
														class="form-control"
														id="PhoneNo" required="">
									</div>
								</div>
							</div>

							<button type="button" class="btn btn-sm btn-primary">
								Save
							</button>
							<button type="button" class="btn btn-sm btn-danger">
								Delete
							</button>
						</form>
					</div>
				</div><!-- /.modal-content --></div><!-- /.modal-dialog --></div>
		<!--end card-->
	</div>

	<div class="col-lg-4">
		<div class="card carousel-bg-img">
			<div class="card-body dash-info-carousel">
				<h4 class="mt-0 header-title">New Products</h4>
				<div id="carousel_2" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="media"><img src="<?php echo base_url() ?>/assets/images/min_1.png" height="400"
													class="mr-4" alt="...">

							</div>
						</div>
						<div class="carousel-item">
							<div class="media"><img src="<?php echo base_url() ?>/assets/images/min_2.jpg" style="height: 400px;width: 400px!important;" class="mr-4" alt="...">

							</div>
						</div>

					</div><a class="carousel-control-prev" href="#carousel_2" role="button"
							 data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span> </a><a class="carousel-control-next"
																	 href="#carousel_2" role="button" data-slide="next"><span
								class="carousel-control-next-icon" aria-hidden="true"></span> <span
								class="sr-only">Next</span></a>
				</div>
			</div>
			<!--end card-body-->
		</div>
	</div>
	<!--end col-->
</div>

<!--end footer--></div>
<!-- end page content --></div>
<!-- Styles -->

