<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-md-8">

				<hr>
				<iframe width="940" height="480" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vT3F7k_R3z7c2gQxQb8abAkmbTIr5SJF9CY5bCoSxQJh0ERuq0ReWs6XRyVpZV4jmG-5EDAK3ApFApv/pubhtml?widget=true&amp;headers=false"></iframe>
			</div>
			<div class="col-md-4">
				<div class="text-center">
					<br><br><br>
					<button type="button" class="btn btn-primary"><i class="fa fa-print"></i>&nbsp;Print</button>
					<br><br><br>
					<button type="button" class="btn btn-warning"><i class="fa fa-download"></i>&nbsp;Download</button>
				</div>

			</div>

		</div>
	</div>

</div>
