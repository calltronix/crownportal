<div class="page-content">
	<div class="container-fluid">

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<h4 class="header-title mt-0 mb-3">All Products</h4>
						<div class="table-responsive dash-social">
							<div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

								<div class="row">
									<div class="col-sm-12">
										<table id="datatable" class="table dataTable no-footer" role="grid"
											   aria-describedby="datatable_info">
											<thead class="thead-light">
											<tr role="row">

												<th class="sorting" tabindex="0" aria-controls="datatable"
													rowspan="1" colspan="1"
													aria-label="Phone No: activate to sort column ascending"
													style="width: 108px;">Product Code</th>
												<th class="sorting" tabindex="0" aria-controls="datatable"
													rowspan="1" colspan="1"
													aria-label="Company: activate to sort column ascending"
													style="width: 151px;">Product Name</th>
												<th class="sorting" tabindex="0" aria-controls="datatable"
													rowspan="1" colspan="1"
													aria-label="Company: activate to sort column ascending"
													style="width: 151px;">Pack Size</th>
												<th class="sorting" tabindex="0" aria-controls="datatable"
													rowspan="1" colspan="1"
													aria-label="Status: activate to sort column ascending"
													style="width: 87px;">Product Category</th>
												<th class="sorting" tabindex="0" aria-controls="datatable"
													rowspan="1" colspan="1"
													aria-label="Action: activate to sort column ascending"
													style="width: 82px;">Product Sub-Category</th>
												<th class="sorting" tabindex="0" aria-controls="datatable"
													rowspan="1" colspan="1"
													aria-label="Action: activate to sort column ascending"
													style="width: 82px;">Unit Price</th>
											</tr>
											<!--end tr-->
											</thead>
											<tbody>
											<!--end tr-->
											<!--end tr-->
											<!--end tr-->
											<!--end tr-->
											<!--end tr-->
											<!--end tr-->
											<tr role="row" class="odd">

												<td>C02AA0910H</td>
												<td>COVERMATT EMULSION ASH</td>
												<td>4</td>
												<td>COVERMATT EMULSION</td>
												<td>COVERMATT EMULSION</td>
												<td>1080</td>
											</tr>
											<tr role="row" class="even">

												<td>C14AA0860D</td>
												<td>ECONOMY VESTA GLOSS SUMMER BLUE</td>
												<td>1</td>
												<td>ECONOMY VESTA GLOSS</td>
												<td>ECON VESTA RANGE - O/B</td>
												<td>327</td>
											</tr>
											<tr role="row" class="odd">

												<td>C02AA0910H</td>
												<td>COVERMATT EMULSION ASH</td>
												<td>4</td>
												<td>COVERMATT EMULSION</td>
												<td>COVERMATT EMULSION</td>
												<td>1080</td>
											</tr>
											<tr role="row" class="even">

												<td>C02AA0910H</td>
												<td>COVERMATT EMULSION ASH</td>
												<td>4</td>
												<td>COVERMATT EMULSION</td>
												<td>COVERMATT EMULSION</td>
												<td>1080</td>
											</tr>
											<tr role="row" class="odd">

												<td>C02AA0910H</td>
												<td>COVERMATT EMULSION ASH</td>
												<td>4</td>
												<td>COVERMATT EMULSION</td>
												<td>COVERMATT EMULSION</td>
												<td>1080</td>
											</tr>
											<tr role="row" class="even">
												<td>C02AA0910H</td>
												<td>COVERMATT EMULSION ASH</td>
												<td>4</td>
												<td>COVERMATT EMULSION</td>
												<td>COVERMATT EMULSION</td>
												<td>1080</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>

							</div>
						</div>
					</div>
					<!--end card-body-->
				</div>
				<!--end card-->
			</div>
			<!--end col-->
		</div>
		<!--end row-->
	</div><!-- container -->
