<div class="page-content">
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="float-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Dealer Portal</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Documents</a></li>
                            <li class="breadcrumb-item active">FAQ's</li>
                        </ol>
                    </div>
                    <h4 class="page-title">FAQ's</h4>
                </div>
                <!--end page-title-box-->
            </div>
            <!--end col-->
        </div><!-- end page title end breadcrumb -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="wrap">
                            <!-- <div class="jctkr-label"><span><i class="fas fa-exchange-alt mr-2"></i>Change 24
                                    Hours</span></div> -->
                            <div class="js-conveyor-example jctkr-wrapper jctkr-initialized">
                            <ul style="width: 2295px; left: -329.025px;">
                                    <li style=""><img src="../assets/images/coins/btc.png" alt=""
                                            class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Account Limit:
                                                8435.21</b></span>
                                    </li>
                                    <li style=""><img src="../assets/images/coins/dash.png" alt=""
                                            class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Account Balance:
                                                1233.54</b></span>
                                    </li>
                                    
                                    <li style=""><img src="../assets/images/coins/btc.png" alt=""
                                            class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Credit Limit:
                                                224446.64</b></span>
                                    </li>
                                    <li style=""><img src="../assets/images/coins/lite.png" alt=""
                                            class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Commitment Limit:
                                                129999.91</b></span>
                                    </li>
                 
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->
            </div>
            <!--end col-->
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Most Commonly Asked Questions</h4>
                        <p class="text-muted">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                            richardson ad squid.</p>
                        <div class="accordion" id="accordionExample-faq">
                            <div class="card shadow-none border mb-1">
                                <div class="card-header" id="headingOne">
                                    <h5 class="my-0"><button class="btn btn-link ml-4 collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseOne" aria-expanded="false"
                                            aria-controls="collapseOne">How can I contact Crown Paints head office for
                                            enquiries?</button></h5>
                                </div>
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                    data-parent="#accordionExample-faq" style="">
                                    <div class="card-body">
                                        We have mobile helpline numbers <strong>+254703333777 /
                                            +254703333888</strong> and office numbers
                                        <strong>+254202165704/03/06</strong>.
                                    </div>
                                </div>

                            </div>
                            <div class="card shadow-none border mb-1">
                                <div class="card-header" id="headingTwo">
                                    <h5 class="my-0"><button class="btn btn-link ml-4 align-self-center collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseTwo"
                                            aria-expanded="false" aria-controls="collapseTwo">Where are the Crown depots
                                            in Nairobi?</button></h5>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                    data-parent="#accordionExample-faq" style="">
                                    <div class="card-body">
                                        We have the main depot off <strong>Likoni Road</strong> housing the
                                        headquarters and another depot along <strong>Muthithi Road</strong> in
                                        <b>Westlands Muthithi House</b>, where we have our main showroom. The third
                                        depot along <strong>Dar es Salaam road Industrial Area</strong> is mainly
                                        for automotive paints. We also have our main factory on <strong>Mogadishu
                                            road</strong>.
                                    </div>
                                </div>
                            </div>
                            <div class="card shadow-none border mb-1">
                                <div class="card-header" id="headingThree">
                                    <h5 class="my-0"><button class="btn btn-link ml-4 collapsed" type="button"
                                            data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                            aria-controls="collapseThree">Can I make payments via MPESA, what is the
                                            procedure?</button></h5>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionExample-faq" style="">
                                    <div class="card-body">
                                        Yes, you can pay using MPESA. Our pay-bill number is <b>544400</b> While the
                                            account details are CL (Likoni Depot), CM (Mombasa Depot), CW (Westlands
                                            Depot), CD (Dar Road Depot) followed by your name.</Yes,>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end accordion-->
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->
            </div>
            <!--end col-->
        </div>
    </div><!-- container -->