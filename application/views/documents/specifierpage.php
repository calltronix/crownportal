<div class="page-content">
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="float-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Dealer Portal</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Documents</a></li>
                            <li class="breadcrumb-item active">Paint Specifier</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Paint Specifier</h4>
                </div>
                <!--end page-title-box-->
            </div>
            <!--end col-->
        </div><!-- end page title end breadcrumb -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="wrap">
                            <!-- <div class="jctkr-label"><span><i class="fas fa-exchange-alt mr-2"></i>Change 24
                                    Hours</span></div> -->
                            <div class="js-conveyor-example jctkr-wrapper jctkr-initialized">
                            <ul style="width: 2295px; left: -329.025px;">
                                    <li style=""><img src="../assets/images/coins/btc.png" alt=""
                                            class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Account Limit:
                                                8435.21</b></span>
                                    </li>
                                    <li style=""><img src="../assets/images/coins/dash.png" alt=""
                                            class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Account Balance:
                                                1233.54</b></span>
                                    </li>
                                    
                                    <li style=""><img src="../assets/images/coins/btc.png" alt=""
                                            class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Credit Limit:
                                                224446.64</b></span>
                                    </li>
                                    <li style=""><img src="../assets/images/coins/lite.png" alt=""
                                            class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Commitment Limit:
                                                129999.91</b></span>
                                    </li>
                 
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->
            </div>
            <!--end col-->
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mt-0 mb-3">Categories</h4>
                        <div class="files-nav">
                            <div class="nav flex-column nav-pills" id="files-tab" aria-orientation="vertical">
                                <a class="nav-link active" id="files-projects-tab" data-toggle="pill"
                                    href="#files-projects" aria-selected="true"><i
                                        class="em em-file_folder mr-3 text-warning d-inline-block"></i>
                                    <div class="d-inline-block align-self-center">
                                        <h5 class="m-0">Undercoats</h5>
                                        
                                    </div>
                                </a>
                                <a class="nav-link" id="files-pdf-tab" data-toggle="pill" href="#files-pdf"
                                    aria-selected="false"><i
                                        class="em em-file_folder mr-3 text-warning d-inline-block"></i>
                                    <div class="d-inline-block align-self-center">
                                        <h5 class="m-0">Primers</h5>
                                    </div>
                                </a><a class="nav-link align-items-center" id="files-documents-tab" data-toggle="pill"
                                    href="#files-documents" aria-selected="false"><i
                                        class="em em-file_folder mr-3 text-warning d-inline-block"></i>
                                    <div class="d-inline-block align-self-center">
                                        <h5 class="m-0">Solo Range</h5>
                                    </div>
                                    <!-- <span class="badge badge-warning ml-auto">8</span> -->
                                </a>
                                <a class="nav-link" id="files-pdf-tab" data-toggle="pill" href="#files-pdf"
                                    aria-selected="false"><i
                                        class="em em-file_folder mr-3 text-warning d-inline-block"></i>
                                    <div class="d-inline-block align-self-center">
                                        <h5 class="m-0">Premium Range</h5>
                                    </div>
                                </a>
                                <a class="nav-link" id="files-pdf-tab" data-toggle="pill" href="#woodfinish"
                                    aria-selected="false"><i
                                        class="em em-file_folder mr-3 text-warning d-inline-block"></i>
                                    <div class="d-inline-block align-self-center">
                                        <h5 class="m-0">Wood Finishes</h5>
                                    </div>
                                </a>
                                <!-- <a class="nav-link" id="files-pdf-tab" data-toggle="pill" href="#files-pdf"
                                    aria-selected="false"><i
                                        class="em em-file_folder mr-3 text-warning d-inline-block"></i>
                                    <div class="d-inline-block align-self-center">
                                        <h5 class="m-0">Specialized Finishes</h5>
                                    </div>
                                </a>
                                <a class="nav-link" id="files-pdf-tab" data-toggle="pill" href="#files-pdf"
                                    aria-selected="false"><i
                                        class="em em-file_folder mr-3 text-warning d-inline-block"></i>
                                    <div class="d-inline-block align-self-center">
                                        <h5 class="m-0">Miscellaneous Paints&Sundries</h5>
                                    </div>
                                </a> -->
                                <!-- <a class="nav-link mb-0" href="#" data-toggle="modal" data-animation="bounce"
                                    data-target=".hide-modal"><i
                                        class="em em-lock mr-3 text-warning d-inline-block"></i>
                                    <div class="d-inline-block align-self-center">
                                        <h5 class="m-0">Files Lock</h5><small class="text-muted">80GB/200GB Used</small>
                                    </div>
                                </a> -->
                            </div>
                        </div>
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->
                <!-- <div class="card">
                    <div class="card-body"><small class="float-right">62%</small>
                        <h6 class="mt-0">620GB / 1TB Used</h6>
                        <div class="progress" style="height: 5px;">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 62%;"
                                aria-valuenow="62" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div> -->
                <!--end card-->
            </div>
            <!--end col-->
            <div class="col-lg-9">
                <div class="">
                    <div class="tab-content" id="files-tabContent">
                        <div class="tab-pane fade show active" id="files-projects">
                            <h4 class="header-title mt-0 mb-3">Undercoats</h4>
                            <div class="file-box-content">
                                <div class="file-box"><a href="#" class="download-icon-link"><i
                                            class="dripicons-download file-download-icon"></i></a>
                                    <div class="text-center"><i class="far fa-file-alt text-primary"></i>
                                        <h6 class="text-truncate">Crown Undercoat</h6><small class="text-muted">06 March
                                            2019 / 5MB</small>
                                    </div>
                                </div>
                                <div class="file-box"><a href="#" class="download-icon-link"><i
                                            class="dripicons-download file-download-icon"></i></a>
                                    <div class="text-center"><i class="far fa-file-code text-danger"></i>
                                        <h6 class="text-truncate">CROWN ACRYLIC PRIMER &SEALER UNDERCOAT</h6><small class="text-muted">15 March
                                            2019 / 8MB</small>
                                    </div>
                                </div>
                               
                          
                            </div>
                        
                         
                        </div>
                        <!--end tab-pane-->
                        <div class="tab-pane fade" id="files-pdf">
                            <h4 class="mt-0 header-title mb-3">Primers</h4>
                            <div class="file-box-content">
                                <div class="file-box"><a href="#" class="download-icon-link"><i
                                            class="dripicons-download file-download-icon"></i></a>
                                    <div class="text-center"><i class="far fa-file-pdf text-info"></i>
                                        <h6 class="text-truncate">CROWN ALUMINIUM WOOD PRIMER</h6><small class="text-muted">06 March
                                            2019 / 5MB</small>
                                    </div>
                                </div>
                                <div class="file-box"><a href="#" class="download-icon-link"><i
                                            class="dripicons-download file-download-icon"></i></a>
                                    <div class="text-center"><i class="far fa-file-pdf text-danger"></i>
                                        <h6 class="text-truncate">CROWN WHITE WOOD PRIMER</h6><small class="text-muted">15 March
                                            2019 / 8MB</small>
                                    </div>
                                </div>
                                <div class="file-box"><a href="#" class="download-icon-link"><i
                                            class="dripicons-download file-download-icon"></i></a>
                                    <div class="text-center"><i class="far fa-file-pdf text-warning"></i>
                                        <h6 class="text-truncate">CROWN UNIVERSAL ALKALI RESISTING WALL PRIMER</h6><small class="text-muted">11 April
                                            2019 / 10MB</small>
                                    </div>
                                </div>
                                <div class="file-box"><a href="#" class="download-icon-link"><i
                                            class="dripicons-download file-download-icon"></i></a>
                                    <div class="text-center"><i class="far fa-file-pdf text-secondary"></i>
                                        <h6 class="text-truncate">CROWN ALKALI RESISTING CHLORINATED-RUBBER PRIMER</h6><small class="text-muted">06
                                            March 2019 / 5MB</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end tab-pane-->
                        <div class="tab-pane fade" id="woodfinish">
                            <h4 class="mt-0 header-title mb-3">Wood Finishes</h4>
                            <div class="file-box-content">
                                <div class="file-box"><a href="#" class="download-icon-link"><i
                                            class="dripicons-download file-download-icon"></i></a>
                                    <div class="text-center"><i class="far fa-file-pdf text-info"></i>
                                        <h6 class="text-truncate">CROWN TWO-PACK EPOXY VARNISH</h6><small class="text-muted">06 March
                                            2019 / 5MB</small>
                                    </div>
                                </div>
                                <div class="file-box"><a href="#" class="download-icon-link"><i
                                            class="dripicons-download file-download-icon"></i></a>
                                    <div class="text-center"><i class="far fa-file-pdf text-danger"></i>
                                        <h6 class="text-truncate">CROWN WOODCARE 365</h6><small class="text-muted">15 March
                                            2019 / 8MB</small>
                                    </div>
                                </div>
                                <div class="file-box"><a href="#" class="download-icon-link"><i
                                            class="dripicons-download file-download-icon"></i></a>
                                    <div class="text-center"><i class="far fa-file-pdf text-warning"></i>
                                        <h6 class="text-truncate">CROWN WOODGUARD</h6><small class="text-muted">11 April
                                            2019 / 10MB</small>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                        <!--end tab-pane-->
                        <div class="tab-pane fade" id="files-documents">
                            <h4 class="mt-0 header-title mb-3">Solo Range</h4>
                            <div class="file-box-content">
                                <div class="file-box"><a href="#" class="download-icon-link"><i
                                            class="dripicons-download file-download-icon"></i></a>
                                    <div class="text-center"><i class="far fa-file-pdf text-info"></i>
                                        <h6 class="text-truncate">CROWN SOLO HI-MATT - VINYL EMULSION</h6><small class="text-muted">06
                                            March 2019 / 5MB</small>
                                    </div>
                                </div>
                                <div class="file-box"><a href="#" class="download-icon-link"><i
                                            class="dripicons-download file-download-icon"></i></a>
                                    <div class="text-center"><i class="far fa-file-pdf text-danger"></i>
                                        <h6 class="text-truncate">CROWN SOLO ULTRA SUPER GLOSS</h6><small class="text-muted">15 March 2019 /
                                            8MB</small>
                                    </div>
                                </div>
                         
                                <div class="file-box"><a href="#" class="download-icon-link"><i
                                            class="dripicons-download file-download-icon"></i></a>
                                    <div class="text-center"><i class="far fa-file-pdf text-secondary"></i>
                                        <h6 class="text-truncate">CROWN SOLO PERMACOTE EMULSION</h6><small class="text-muted">06 March
                                            2019 / 5MB</small>
                                    </div>
                                </div>
                            </div>
                        
                        
                        </div>
                        <!--end tab-pen-->
                        <div class="tab-pane fade" id="files-hide">
                            <h4 class="mt-0 header-title mb-3">Hide</h4>
                        </div>
                        <!--end tab-pane-->
                    </div>
                    <!--end tab-content-->
                </div>
                <!--end card-body-->
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div><!-- container -->
 