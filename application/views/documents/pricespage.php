<div class="page-content">
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="float-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Dealer Portal</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Documents</a></li>
                            <li class="breadcrumb-item active">Price Lists</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Price Lists</h4>
                </div>
                <!--end page-title-box-->
            </div>
            <!--end col-->
        </div><!-- end page title end breadcrumb -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="wrap">
                            <!-- <div class="jctkr-label"><span><i class="fas fa-exchange-alt mr-2"></i>Change 24
                                    Hours</span></div> -->
                            <div class="js-conveyor-example jctkr-wrapper jctkr-initialized">
                            <ul style="width: 2295px; left: -329.025px;">
                                    <li style=""><img src="../assets/images/coins/btc.png" alt=""
                                            class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Account Limit:
                                                8435.21</b></span>
                                    </li>
                                    <li style=""><img src="../assets/images/coins/dash.png" alt=""
                                            class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Account Balance:
                                                1233.54</b></span>
                                    </li>
                                    
                                    <li style=""><img src="../assets/images/coins/btc.png" alt=""
                                            class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Credit Limit:
                                                224446.64</b></span>
                                    </li>
                                    <li style=""><img src="../assets/images/coins/lite.png" alt=""
                                            class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Commitment Limit:
                                                129999.91</b></span>
                                    </li>
                 
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->
            </div>
            <!--end col-->
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mt-0 mb-3">Categories</h4>
                        <ul>
                            <li><a href="#"> <h5 class="m-0">Price List – Decorative</h5></a></li><small class="text-muted">06 March
                                            2019 / 5MB</small>
                            <li><a href="#"><h5 class="m-0">Price List – Automotive</h5></a></li><small class="text-muted">06 March
                                            2019 / 5MB</small>
                            <li><a href="#"><h5 class="m-0">Price List – Dr Fixit</h5></li><small class="text-muted">06 March
                                            2019 / 5MB</small>
                            <li><a href="#"><h5 class="m-0">Price List – Italia Series</h5></a></li><small class="text-muted">06 March
                                            2019 / 5MB</small>
                            <li><a href="#"><h5 class="m-0">Price List – Bases & Colourants</h5></a></li><small class="text-muted">06 March
                                            2019 / 5MB</small>
                      
                        </ul>
                    </div>
                    <!--end card-body-->
                </div>

            </div>
            <!--end col-->

        </div>
        <!--end row-->
    </div><!-- container -->