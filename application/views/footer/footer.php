<!--Start of Tawk.to Script-->
<script type="text/javascript">
	var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
	(function () {
		var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
		s1.async = true;
		s1.src = 'https://embed.tawk.to/5e95aaea35bcbb0c9ab0e73b/default';
		s1.charset = 'UTF-8';
		s1.setAttribute('crossorigin', '*');
		s0.parentNode.insertBefore(s1, s0);
	})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
<script src="<?php echo base_url('') ?>/assets/new_assets/assets/js/jquery.min.js"></script>
<script src="<?php echo base_url('') ?>/assets/new_assets/assets/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url('') ?>/assets/new_assets/assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url('') ?>/assets/new_assets/assets/js/metismenu.min.js"></script>
<script src="<?php echo base_url('') ?>/assets/new_assets/assets/js/waves.js"></script>
<script src="<?php echo base_url('') ?>/assets/new_assets/assets/js/feather.min.js"></script>
<script src="<?php echo base_url('') ?>/assets/new_assets/assets/js/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url('') ?>/assets/new_assets/plugins/apexcharts/apexcharts.min.js"></script>
<script src="<?php echo base_url('') ?>/assets/new_assets/plugins/chartjs/chart.min.js"></script>
<script src="<?php echo base_url('') ?>/assets/new_assets/plugins/chartjs/roundedBar.min.js"></script>
<script src="<?php echo base_url('') ?>/assets/new_assets/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="<?php echo base_url('') ?>/assets/new_assets/plugins/jvectormap/jquery-jvectormap-us-aea-en.js"></script>
<script src="<?php echo base_url('') ?>/assets/pages/jquery.ecommerce_dashboard.init.js"></script><!-- App js -->
<script src="<?php echo base_url('') ?>/assets/new_assets/assets/js/app.js"></script>
<script src="<?php echo base_url() ?>assets/pages/jquery.crypto-news.init.js"></script>


<script src="<?php echo base_url() ?>assets/plugins/ticker/jquery.jConveyorTicker.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/metisMenu.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/waves.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/pages/jquery.crm_dashboard.init.js"></script>
<script src="<?php echo base_url() ?>assets/pages/jquery.crypto-news.init.js"></script>


<!-- Resources -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<!-- Chart code -->
<script>
	am4core.ready(function () {

// Themes begin
		am4core.useTheme(am4themes_animated);
// Themes end


		var chart = am4core.create('chartdiv', am4charts.XYChart)
		chart.colors.step = 2;

		chart.legend = new am4charts.Legend()
		chart.legend.position = 'top'
		chart.legend.paddingBottom = 20
		chart.legend.labels.template.maxWidth = 95

		var xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
		xAxis.dataFields.category = 'category'
		xAxis.renderer.cellStartLocation = 0.1
		xAxis.renderer.cellEndLocation = 0.9
		xAxis.renderer.grid.template.location = 0;

		var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
		yAxis.min = 0;

		function createSeries(value, name) {
			var series = chart.series.push(new am4charts.ColumnSeries())
			series.dataFields.valueY = value
			series.dataFields.categoryX = 'category'
			series.name = name

			series.events.on("hidden", arrangeColumns);
			series.events.on("shown", arrangeColumns);

			var bullet = series.bullets.push(new am4charts.LabelBullet())
			bullet.interactionsEnabled = false
			bullet.dy = 30;
			bullet.label.text = '{valueY}'
			bullet.label.fill = am4core.color('#ffffff')

			return series;
		}

		chart.data = [
			{
				category: 'Total MTD Volume Sales',
				first: 40,
				second: 55,
				third: 60
			},
			{
				category: 'Last Year same month v/s Current month',
				first: 30,
				second: 78,
				third: 69
			},
			{
				category: ' Last Year v/s Current YTD ',
				first: 27,
				second: 40,
				third: 45
			}
		]


		createSeries('first', '2019');
		createSeries('second', '2020');

		function arrangeColumns() {

			var series = chart.series.getIndex(0);

			var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
			if (series.dataItems.length > 1) {
				var x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
				var x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
				var delta = ((x1 - x0) / chart.series.length) * w;
				if (am4core.isNumber(delta)) {
					var middle = chart.series.length / 2;

					var newIndex = 0;
					chart.series.each(function (series) {
						if (!series.isHidden && !series.isHiding) {
							series.dummyData = newIndex;
							newIndex++;
						} else {
							series.dummyData = chart.series.indexOf(series);
						}
					})
					var visibleCount = newIndex;
					var newMiddle = visibleCount / 2;

					chart.series.each(function (series) {
						var trueIndex = chart.series.indexOf(series);
						var newIndex = series.dummyData;

						var dx = (newIndex - trueIndex + middle - newMiddle) * delta

						series.animate({
							property: "dx",
							to: dx
						}, series.interpolationDuration, series.interpolationEasing);
						series.bulletsContainer.animate({
							property: "dx",
							to: dx
						}, series.interpolationDuration, series.interpolationEasing);
					})
				}
			}
		}

	}); // end am4core.ready()
</script>

<!-- Chart code -->
<script>
	am4core.ready(function () {

// Themes begin
		am4core.useTheme(am4themes_animated);
// Themes end


		var chart = am4core.create('chartdiv2', am4charts.XYChart)
		chart.colors.step = 2;

		chart.legend = new am4charts.Legend()
		chart.legend.position = 'top'
		chart.legend.paddingBottom = 20
		chart.legend.labels.template.maxWidth = 95

		var xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
		xAxis.dataFields.category = 'category'
		xAxis.renderer.cellStartLocation = 0.1
		xAxis.renderer.cellEndLocation = 0.9
		xAxis.renderer.grid.template.location = 0;

		var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
		yAxis.min = 0;

		function createSeries(value, name) {
			var series = chart.series.push(new am4charts.ColumnSeries())
			series.dataFields.valueY = value
			series.dataFields.categoryX = 'category'
			series.name = name

			series.events.on("hidden", arrangeColumns);
			series.events.on("shown", arrangeColumns);

			var bullet = series.bullets.push(new am4charts.LabelBullet())
			bullet.interactionsEnabled = false
			bullet.dy = 30;
			bullet.label.text = '{valueY}'
			bullet.label.fill = am4core.color('#ffffff')

			return series;
		}

		chart.data = [
			{
				category: 'Total MTD Value Sales',
				first: 40,
				second: 55,
				third: 60
			},
			{
				category: 'Month Comparison',
				first: 30,
				second: 78,
				third: 69
			},
			{
				category: ' Last Year v/s Current YTD ',
				first: 27,
				second: 40,
				third: 45
			}
		]


		createSeries('first', '2019');
		createSeries('third', '2020');

		function arrangeColumns() {

			var series = chart.series.getIndex(0);

			var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
			if (series.dataItems.length > 1) {
				var x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
				var x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
				var delta = ((x1 - x0) / chart.series.length) * w;
				if (am4core.isNumber(delta)) {
					var middle = chart.series.length / 2;

					var newIndex = 0;
					chart.series.each(function (series) {
						if (!series.isHidden && !series.isHiding) {
							series.dummyData = newIndex;
							newIndex++;
						} else {
							series.dummyData = chart.series.indexOf(series);
						}
					})
					var visibleCount = newIndex;
					var newMiddle = visibleCount / 2;

					chart.series.each(function (series) {
						var trueIndex = chart.series.indexOf(series);
						var newIndex = series.dummyData;

						var dx = (newIndex - trueIndex + middle - newMiddle) * delta

						series.animate({
							property: "dx",
							to: dx
						}, series.interpolationDuration, series.interpolationEasing);
						series.bulletsContainer.animate({
							property: "dx",
							to: dx
						}, series.interpolationDuration, series.interpolationEasing);
					})
				}
			}
		}

	}); // end am4core.ready()
</script>

<script>
	am4core.ready(function () {

// Themes begin
		am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
		var chart = am4core.create("chartdiv3", am4charts.PieChart);

// Set data
		var selected;
		var types = [{
			type: "Premium Decorative",
			percent: 20,
			color: chart.colors.getIndex(0),
			subs: [{
				type: "Product 1",
				percent: 10
			}, {
				type: "Product 2",
				percent: 7
			}, {
				type: "Product 3",
				percent: 3
			}]
		}, {
			type: "Premium Automotive and Industrial ",
			percent: 25,
			color: chart.colors.getIndex(1),
			subs: [{
				type: "Product 1",
				percent: 5
			}, {
				type: "Product 2",
				percent: 10
			}, {
				type: "Product 3",
				percent: 10
			}]
		}, {
			type: "Economy ",
			percent: 8,
			color: chart.colors.getIndex(2),
			subs: [{
				type: "Product 1",
				percent: 4
			}, {
				type: "Product 2",
				percent: 2
			}, {
				type: "Product 3",
				percent: 2
			}]
		},
			{
				type: "Wood Finish ",
				percent: 15,
				color: chart.colors.getIndex(3),
				subs: [{
					type: "Product 1",
					percent: 5
				}, {
					type: "Product 2",
					percent: 5
				}, {
					type: "Product 3",
					percent: 5
				}]
			},
			{
				type: "New Products",
				percent: 10,
				color: chart.colors.getIndex(4),
				subs: [{
					type: "Product 1",
					percent: 5
				}, {
					type: "Products 2",
					percent: 3
				}, {
					type: "Product 3",
					percent: 2
				}]
			},
			{
				type: "Pidilite Products ",
				percent: 2,
				color: chart.colors.getIndex(5),
				subs: [{
					type: "Product 1",
					percent: 1
				}, {
					type: "Product 2",
					percent: 2
				}]
			}
		];

// Add data
		chart.data = generateChartData();

// Add and configure Series
		var pieSeries = chart.series.push(new am4charts.PieSeries());
		pieSeries.dataFields.value = "percent";
		pieSeries.dataFields.category = "type";
		pieSeries.slices.template.propertyFields.fill = "color";
		pieSeries.slices.template.propertyFields.isActive = "pulled";
		pieSeries.slices.template.strokeWidth = 0;

		function generateChartData() {
			var chartData = [];
			for (var i = 0; i < types.length; i++) {
				if (i == selected) {
					for (var x = 0; x < types[i].subs.length; x++) {
						chartData.push({
							type: types[i].subs[x].type,
							percent: types[i].subs[x].percent,
							color: types[i].color,
							pulled: true
						});
					}
				} else {
					chartData.push({
						type: types[i].type,
						percent: types[i].percent,
						color: types[i].color,
						id: i
					});
				}
			}
			return chartData;
		}

		pieSeries.slices.template.events.on("hit", function (event) {
			if (event.target.dataItem.dataContext.id != undefined) {
				selected = event.target.dataItem.dataContext.id;
			} else {
				selected = undefined;
			}
			chart.data = generateChartData();
		});

	}); // end am4core.ready()
</script>
<script>
	am4core.ready(function () {

// Themes begin
		am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
		var chart = am4core.create("chartdiv11", am4charts.PieChart);

// Set data
		var selected;
		var types = [{
			type: "Product 1",
			percent: 20,
			color: chart.colors.getIndex(0),
		}, {
			type: "Product 2 ",
			percent: 8,
			color: chart.colors.getIndex(2),
		}, {
			type: "Product 3",
			percent: 10,
			color: chart.colors.getIndex(4),

		}
		];

// Add data
		chart.data = generateChartData();

// Add and configure Series
		var pieSeries = chart.series.push(new am4charts.PieSeries());
		pieSeries.dataFields.value = "percent";
		pieSeries.dataFields.category = "type";
		pieSeries.slices.template.propertyFields.fill = "color";
		pieSeries.slices.template.propertyFields.isActive = "pulled";
		pieSeries.slices.template.strokeWidth = 0;

		function generateChartData() {
			var chartData = [];
			for (var i = 0; i < types.length; i++) {
				if (i == selected) {
					for (var x = 0; x < types[i].subs.length; x++) {
						chartData.push({
							type: types[i].subs[x].type,
							percent: types[i].subs[x].percent,
							color: types[i].color,
							pulled: true
						});
					}
				} else {
					chartData.push({
						type: types[i].type,
						percent: types[i].percent,
						color: types[i].color,
						id: i
					});
				}
			}
			return chartData;
		}

		pieSeries.slices.template.events.on("hit", function (event) {
			if (event.target.dataItem.dataContext.id != undefined) {
				selected = event.target.dataItem.dataContext.id;
			} else {
				selected = undefined;
			}
			chart.data = generateChartData();
		});

	}); // end am4core.ready()
</script>
<script>
	am4core.ready(function () {

// Themes begin
		am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
		var chart = am4core.create("chartdiv6", am4charts.PieChart);

// Set data
		var selected;
		var types = [{
			type: "Product 1",
			percent: 20,
			color: chart.colors.getIndex(0),
		}, {
			type: "Product 2 ",
			percent: 8,
			color: chart.colors.getIndex(2),
		}, {
			type: "Product 3",
			percent: 10,
			color: chart.colors.getIndex(4),

		}
		];

// Add data
		chart.data = generateChartData();

// Add and configure Series
		var pieSeries = chart.series.push(new am4charts.PieSeries());
		pieSeries.dataFields.value = "percent";
		pieSeries.dataFields.category = "type";
		pieSeries.slices.template.propertyFields.fill = "color";
		pieSeries.slices.template.propertyFields.isActive = "pulled";
		pieSeries.slices.template.strokeWidth = 0;

		function generateChartData() {
			var chartData = [];
			for (var i = 0; i < types.length; i++) {
				if (i == selected) {
					for (var x = 0; x < types[i].subs.length; x++) {
						chartData.push({
							type: types[i].subs[x].type,
							percent: types[i].subs[x].percent,
							color: types[i].color,
							pulled: true
						});
					}
				} else {
					chartData.push({
						type: types[i].type,
						percent: types[i].percent,
						color: types[i].color,
						id: i
					});
				}
			}
			return chartData;
		}

		pieSeries.slices.template.events.on("hit", function (event) {
			if (event.target.dataItem.dataContext.id != undefined) {
				selected = event.target.dataItem.dataContext.id;
			} else {
				selected = undefined;
			}
			chart.data = generateChartData();
		});

	}); // end am4core.ready()
</script>
<script>
	am4core.ready(function () {

// Themes begin
		am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
		var chart = am4core.create("chartdiv7", am4charts.PieChart);

// Set data
		var selected;
		var types = [{
			type: "Product 1",
			percent: 20,
			color: chart.colors.getIndex(0),
		}, {
			type: "Product 2 ",
			percent: 8,
			color: chart.colors.getIndex(2),
		}, {
			type: "Product 3",
			percent: 10,
			color: chart.colors.getIndex(4),

		}
		];

// Add data
		chart.data = generateChartData();

// Add and configure Series
		var pieSeries = chart.series.push(new am4charts.PieSeries());
		pieSeries.dataFields.value = "percent";
		pieSeries.dataFields.category = "type";
		pieSeries.slices.template.propertyFields.fill = "color";
		pieSeries.slices.template.propertyFields.isActive = "pulled";
		pieSeries.slices.template.strokeWidth = 0;

		function generateChartData() {
			var chartData = [];
			for (var i = 0; i < types.length; i++) {
				if (i == selected) {
					for (var x = 0; x < types[i].subs.length; x++) {
						chartData.push({
							type: types[i].subs[x].type,
							percent: types[i].subs[x].percent,
							color: types[i].color,
							pulled: true
						});
					}
				} else {
					chartData.push({
						type: types[i].type,
						percent: types[i].percent,
						color: types[i].color,
						id: i
					});
				}
			}
			return chartData;
		}

		pieSeries.slices.template.events.on("hit", function (event) {
			if (event.target.dataItem.dataContext.id != undefined) {
				selected = event.target.dataItem.dataContext.id;
			} else {
				selected = undefined;
			}
			chart.data = generateChartData();
		});

	}); // end am4core.ready()
</script>
<script>
	am4core.ready(function () {

// Themes begin
		am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
		var chart = am4core.create("chartdiv8", am4charts.PieChart);

// Set data
		var selected;
		var types = [{
			type: "Product 1",
			percent: 20,
			color: chart.colors.getIndex(0),
		}, {
			type: "Product 2 ",
			percent: 8,
			color: chart.colors.getIndex(2),
		}, {
			type: "Product 3",
			percent: 10,
			color: chart.colors.getIndex(4),

		}
		];

// Add data
		chart.data = generateChartData();

// Add and configure Series
		var pieSeries = chart.series.push(new am4charts.PieSeries());
		pieSeries.dataFields.value = "percent";
		pieSeries.dataFields.category = "type";
		pieSeries.slices.template.propertyFields.fill = "color";
		pieSeries.slices.template.propertyFields.isActive = "pulled";
		pieSeries.slices.template.strokeWidth = 0;

		function generateChartData() {
			var chartData = [];
			for (var i = 0; i < types.length; i++) {
				if (i == selected) {
					for (var x = 0; x < types[i].subs.length; x++) {
						chartData.push({
							type: types[i].subs[x].type,
							percent: types[i].subs[x].percent,
							color: types[i].color,
							pulled: true
						});
					}
				} else {
					chartData.push({
						type: types[i].type,
						percent: types[i].percent,
						color: types[i].color,
						id: i
					});
				}
			}
			return chartData;
		}

		pieSeries.slices.template.events.on("hit", function (event) {
			if (event.target.dataItem.dataContext.id != undefined) {
				selected = event.target.dataItem.dataContext.id;
			} else {
				selected = undefined;
			}
			chart.data = generateChartData();
		});

	}); // end am4core.ready()
</script>
<script>
	am4core.ready(function () {

// Themes begin
		am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
		var chart = am4core.create("chartdiv9", am4charts.PieChart);

// Set data
		var selected;
		var types = [{
			type: "Product 1",
			percent: 20,
			color: chart.colors.getIndex(0),
		}, {
			type: "Product 2 ",
			percent: 8,
			color: chart.colors.getIndex(2),
		}, {
			type: "Product 3",
			percent: 10,
			color: chart.colors.getIndex(4),

		}
		];

// Add data
		chart.data = generateChartData();

// Add and configure Series
		var pieSeries = chart.series.push(new am4charts.PieSeries());
		pieSeries.dataFields.value = "percent";
		pieSeries.dataFields.category = "type";
		pieSeries.slices.template.propertyFields.fill = "color";
		pieSeries.slices.template.propertyFields.isActive = "pulled";
		pieSeries.slices.template.strokeWidth = 0;

		function generateChartData() {
			var chartData = [];
			for (var i = 0; i < types.length; i++) {
				if (i == selected) {
					for (var x = 0; x < types[i].subs.length; x++) {
						chartData.push({
							type: types[i].subs[x].type,
							percent: types[i].subs[x].percent,
							color: types[i].color,
							pulled: true
						});
					}
				} else {
					chartData.push({
						type: types[i].type,
						percent: types[i].percent,
						color: types[i].color,
						id: i
					});
				}
			}
			return chartData;
		}

		pieSeries.slices.template.events.on("hit", function (event) {
			if (event.target.dataItem.dataContext.id != undefined) {
				selected = event.target.dataItem.dataContext.id;
			} else {
				selected = undefined;
			}
			chart.data = generateChartData();
		});

	}); // end am4core.ready()
</script>
<script>
	am4core.ready(function () {

// Themes begin
		am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
		var chart = am4core.create("chartdiv10", am4charts.PieChart);

// Set data
		var selected;
		var types = [{
			type: "Product 1",
			percent: 20,
			color: chart.colors.getIndex(0),
		}, {
			type: "Product 2 ",
			percent: 8,
			color: chart.colors.getIndex(2),
		}, {
			type: "Product 3",
			percent: 10,
			color: chart.colors.getIndex(4),

		}
		];

// Add data
		chart.data = generateChartData();

// Add and configure Series
		var pieSeries = chart.series.push(new am4charts.PieSeries());
		pieSeries.dataFields.value = "percent";
		pieSeries.dataFields.category = "type";
		pieSeries.slices.template.propertyFields.fill = "color";
		pieSeries.slices.template.propertyFields.isActive = "pulled";
		pieSeries.slices.template.strokeWidth = 0;

		function generateChartData() {
			var chartData = [];
			for (var i = 0; i < types.length; i++) {
				if (i == selected) {
					for (var x = 0; x < types[i].subs.length; x++) {
						chartData.push({
							type: types[i].subs[x].type,
							percent: types[i].subs[x].percent,
							color: types[i].color,
							pulled: true
						});
					}
				} else {
					chartData.push({
						type: types[i].type,
						percent: types[i].percent,
						color: types[i].color,
						id: i
					});
				}
			}
			return chartData;
		}

		pieSeries.slices.template.events.on("hit", function (event) {
			if (event.target.dataItem.dataContext.id != undefined) {
				selected = event.target.dataItem.dataContext.id;
			} else {
				selected = undefined;
			}
			chart.data = generateChartData();
		});

	}); // end am4core.ready()
</script>
<script>
	am4core.ready(function () {

// Themes begin
		am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
		var chart = am4core.create("chartdiv3", am4charts.PieChart);

// Set data
		var selected;
		var types = [{
			type: "Premium Decorative",
			percent: 20,
			color: chart.colors.getIndex(0),
			subs: [{
				type: "Product 1",
				percent: 10
			}, {
				type: "Product 2",
				percent: 7
			}, {
				type: "Product 3",
				percent: 3
			}]
		}, {
			type: "Premium Automotive and Industrial ",
			percent: 25,
			color: chart.colors.getIndex(1),
			subs: [{
				type: "Product 1",
				percent: 5
			}, {
				type: "Product 2",
				percent: 10
			}, {
				type: "Product 3",
				percent: 10
			}]
		}, {
			type: "Economy ",
			percent: 8,
			color: chart.colors.getIndex(2),
			subs: [{
				type: "Product 1",
				percent: 4
			}, {
				type: "Product 2",
				percent: 2
			}, {
				type: "Product 3",
				percent: 2
			}]
		},
			{
				type: "Wood Finish ",
				percent: 15,
				color: chart.colors.getIndex(3),
				subs: [{
					type: "Product 1",
					percent: 5
				}, {
					type: "Product 2",
					percent: 5
				}, {
					type: "Product 3",
					percent: 5
				}]
			},
			{
				type: "New Products",
				percent: 10,
				color: chart.colors.getIndex(4),
				subs: [{
					type: "Product 1",
					percent: 5
				}, {
					type: "Products 2",
					percent: 3
				}, {
					type: "Product 3",
					percent: 2
				}]
			},
			{
				type: "Pidilite Products ",
				percent: 2,
				color: chart.colors.getIndex(5),
				subs: [{
					type: "Product 1",
					percent: 1
				}, {
					type: "Product 2",
					percent: 2
				}]
			}
		];

// Add data
		chart.data = generateChartData();

// Add and configure Series
		var pieSeries = chart.series.push(new am4charts.PieSeries());
		pieSeries.dataFields.value = "percent";
		pieSeries.dataFields.category = "type";
		pieSeries.slices.template.propertyFields.fill = "color";
		pieSeries.slices.template.propertyFields.isActive = "pulled";
		pieSeries.slices.template.strokeWidth = 0;

		function generateChartData() {
			var chartData = [];
			for (var i = 0; i < types.length; i++) {
				if (i == selected) {
					for (var x = 0; x < types[i].subs.length; x++) {
						chartData.push({
							type: types[i].subs[x].type,
							percent: types[i].subs[x].percent,
							color: types[i].color,
							pulled: true
						});
					}
				} else {
					chartData.push({
						type: types[i].type,
						percent: types[i].percent,
						color: types[i].color,
						id: i
					});
				}
			}
			return chartData;
		}

		pieSeries.slices.template.events.on("hit", function (event) {
			if (event.target.dataItem.dataContext.id != undefined) {
				selected = event.target.dataItem.dataContext.id;
			} else {
				selected = undefined;
			}
			chart.data = generateChartData();
		});

	}); // end am4core.ready()
</script>
<script>


	am4core.ready(function () {

// Themes begin
		am4core.useTheme(am4themes_animated);
// Themes end

		// Create chart instance
		var chart = am4core.create("chartdiv4", am4charts.XYChart);

// Add data
		chart.data = [{
			"year": 2019,
			"target": 23.5,
			"achievement": 24
		}, {
			"year": 2020,
			"target": 24.6,
			"achievement": 25
		}];

// Create axes
		var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "year";
		categoryAxis.numberFormatter.numberFormat = "#";
		categoryAxis.renderer.inversed = true;
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.cellStartLocation = 0.1;
		categoryAxis.renderer.cellEndLocation = 0.9;

		var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
		valueAxis.renderer.opposite = true;

// Create series
		function createSeries(field, name) {
			var series = chart.series.push(new am4charts.ColumnSeries());
			series.dataFields.valueX = field;
			series.dataFields.categoryY = "year";
			series.name = name;
			series.columns.template.tooltipText = "{name}: [bold]{valueX}[/]";
			series.columns.template.height = am4core.percent(100);
			series.sequencedInterpolation = true;

			var valueLabel = series.bullets.push(new am4charts.LabelBullet());
			valueLabel.label.text = "{valueX}";
			valueLabel.label.horizontalCenter = "left";
			valueLabel.label.dx = 10;
			valueLabel.label.hideOversized = false;
			valueLabel.label.truncate = false;

			var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
			categoryLabel.label.text = "{name}";
			categoryLabel.label.horizontalCenter = "right";
			categoryLabel.label.dx = -10;
			categoryLabel.label.fill = am4core.color("#fff");
			categoryLabel.label.hideOversized = false;
			categoryLabel.label.truncate = false;
		}

		createSeries("target", "Target");
		createSeries("achievement", "Achievement");

	}); // end am4core.ready()
</script>
<script>


	am4core.ready(function () {

// Themes begin
		am4core.useTheme(am4themes_animated);
// Themes end

		// Create chart instance
		var chart = am4core.create("chartdiv5", am4charts.XYChart);

// Add data
		chart.data = [{
			"year": 2019,
			"target": 23.5,
			"achievement": 24
		}, {
			"year": 2020,
			"target": 24.6,
			"achievement": 25
		}];

// Create axes
		var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "year";
		categoryAxis.numberFormatter.numberFormat = "#";
		categoryAxis.renderer.inversed = true;
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.cellStartLocation = 0.1;
		categoryAxis.renderer.cellEndLocation = 0.9;

		var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
		valueAxis.renderer.opposite = true;

// Create series
		function createSeries(field, name) {
			var series = chart.series.push(new am4charts.ColumnSeries());
			series.dataFields.valueX = field;
			series.dataFields.categoryY = "year";
			series.name = name;
			series.columns.template.tooltipText = "{name}: [bold]{valueX}[/]";
			series.columns.template.height = am4core.percent(100);
			series.sequencedInterpolation = true;

			var valueLabel = series.bullets.push(new am4charts.LabelBullet());
			valueLabel.label.text = "{valueX}";
			valueLabel.label.horizontalCenter = "left";
			valueLabel.label.dx = 10;
			valueLabel.label.hideOversized = false;
			valueLabel.label.truncate = false;

			var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
			categoryLabel.label.text = "{name}";
			categoryLabel.label.horizontalCenter = "right";
			categoryLabel.label.dx = -10;
			categoryLabel.label.fill = am4core.color("#fff");
			categoryLabel.label.hideOversized = false;
			categoryLabel.label.truncate = false;
		}

		createSeries("target", "Target");
		createSeries("achievement", "Achievement");

	}); // end am4core.ready()
</script>
