<div class="row">
	<div class="col-md-4">
		<div class="card report-card">
			<div class="card-body">
				<div class="float-right"><i class="fas fa-dollar-sign bg-soft-pink main-widgets-icon"></i>
				</div>
				<h4 class="title-text mt-0">Available Balance Credit Limit</h4>
				<h3 class="my-3">Ksh.500<small></small></h3>
			</div><!--end card-body-->
			<div class="card-body p-0">
				<div id="spark1" class="apex-charts"></div>
			</div>
		</div><!--end card--></div><!--end col-->
	<div class="col-md-4">
		<div class="card report-card">
			<div class="card-body">
				<div class="float-right"><i class="fas fa-apple-alt bg-soft-success main-widgets-icon"></i>
				</div>
				<h4 class="title-text mt-0">Total Volume Sales (MTD)</h4>
				<h3 class="my-3">Ksh.35000 <small></small></h3>
			</div><!--end card-body-->
			<div class="card-body p-0">
				<div id="spark3" class="apex-charts"></div>
			</div>
		</div><!--end card--></div>
	<div class="col-md-4">
		<div class="card report-card">
			<div class="card-body">
				<div class="float-right"><i class="fas fa-radiation bg-soft-warning main-widgets-icon"></i>
				</div>
				<h4 class="title-text mt-0">Total Value Sales (MTD)</h4>
				<h3 class="my-3">Ksh.8800 <small></small></h3>
			</div><!--end card-body-->
			<div class="card-body p-0">
				<div id="spark2" class="apex-charts"></div>
			</div>
		</div><!--end card--></div>
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<button type="button" class="btn btn-outline-success crypto-modal-btn" data-toggle="modal"
						data-target="#exampleModalRequest"><i
						class="dripicons-search align-self-center icon-md mr-2"></i>FIlter
				</button>


			</div>
			<!--end card-body-->
		</div>
		<!--end card-->

	</div>

</div>
<div class="row">
	<div class="col-md-4">
		<div class="card report-card">
			<div class="card-body">
				<div class="float-right"><i class="fas fa-download"></i>
				</div>
				<h4 class="title-text mt-0"><i class="fas fa-book-open"></i>&nbsp; Credit Note</h4>
			</div><!--end card-body-->

		</div><!--end card--></div>
	<div class="col-md-4">
		<div class="card report-card">
			<div class="card-body">
				<div class="float-right"><i class="fas fa-download"></i>
				</div>
				<h4 class="title-text mt-0"><i class="fas fa-book"></i>&nbsp; Debit Note</h4>
			</div><!--end card-body-->

		</div><!--end card--></div>
	<div class="col-md-4">
		<div class="card report-card">
			<div class="card-body">
				<div class="float-right"><i class="fas fa-download"></i>
				</div>
				<h4 class="title-text mt-0"><i class="fas fa-file-excel"></i>&nbsp; Invoice Summary</h4>
			</div><!--end card-body-->

		</div><!--end card--></div>
</div>
<!--end row-->
<div class="modal fade" id="exampleModalRequest" tabindex="-1" role="dialog"
	 aria-labelledby="exampleModalDefaultRequest" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header"><h6 class="modal-title m-0"
										  id="exampleModalDefaultRequest">Filter </h6>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="la la-times"></i></span></button>
			</div><!--end modal-header-->
			<div class="modal-body">
				<form>
					<div class="form-group">
						<label>From:</label>
						<input type="date" name="from" class="form-control">
					</div>
					<div class="form-group">
						<label>To:</label>
						<input type="date" name="from" class="form-control">
					</div>
					<div class="form-group">
						<button class="btn btn-primary btn-rounded btn-block waves-effect waves-light py-2"
								type="text">Done <i
								class="fas fa-check ml-1"></i></button>
					</div>
				</form>
			</div>
			<!--end modal-body--></div>
		<!--end modal-content--></div>
	<!--end modal-dialog--></div>

