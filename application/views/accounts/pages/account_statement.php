
<div class="row">
	<div class="col-md-4">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-12">
						<div class="wallet-bal-usd">
							<h4 class="wallet-title m-0">Your Total Balance as of: <?php echo date('Y-m-d') ?></h4>
							<br><br>
							<h4 class="text-left">Debit:&nbsp;&nbsp;Ksh.85692.00</h4>
							<br>
							<h4 class="text-left">Credit:&nbsp;&nbsp;Ksh.85692.00</h4>
						</div>
						<br>
						<br>

						<button class="btn btn-success" type="submit">Add Funds</button>

					</div>
				</div>
			</div>

			<!--end card-body-->
		</div>
	</div>
	<div class="col-md-8">
		<div class="card">
			<div class="card-body">
				<h5 class="mt-0">Generate Your Account Statement</h5>
				<p class="text-muted mb-3">Select From The Statement Periods Below
				</p>
				<br>
				<div class="general-label">
					<form class="form">
						<!-- <div class="form-group row"><label class="col-md-12 my-2 control-label">Select From The Statements Periods Below</label></div> -->
						<div class="form-group row">
							<div class="col-md-6">
								<div class="my-2">
									<div class="custom-control custom-radio"><input type="radio"
																					id="customRadio3" name="customRadio"
																					class="custom-control-input">
										<label class="custom-control-label" for="customRadio3">Last 3
											Months</label>
									</div>
								</div>
								<div class="my-2">
									<div class="custom-control custom-radio"><input type="radio"
																					id="customRadio4" checked=""
																					name="customRadio"
																					class="custom-control-input"> <label
											class="custom-control-label"
											for="customRadio4">Last 6 Months</label></div>
								</div>
								<div class="my-2">
									<div class="custom-control custom-radio"><input type="radio"
																					id="customRadio4" checked=""
																					name="customRadio"
																					class="custom-control-input"> <label
											class="custom-control-label"
											for="customRadio4">Last 1 Year</label></div>
								</div>

							</div>
							<div class="col-md-3">
								<div class="my-2">
									<div class="form-group">
										<label>From:</label>
										<input type="date" class="form-control">
									</div>

								</div>

							</div>
							<div class="col-md-3">
								<div class="my-2">
									<div class="form-group">
										<label>To:</label>
										<input type="date" class="form-control">
									</div>

								</div>

							</div>

						</div><!-- form-group -->
						<div class="col-md-12">
							<div class="text-center">
								<button class="btn btn-success" type="submit">Generate!</button>
							</div>
						</div>


						<!--end row-->
					</form>
					<!--end form-->
				</div>
				<!--end general-->
			</div>
			<!--end card-body-->
		</div>
	</div>
	<div class="col-md-12">


		<div class="card">
			<div class="card-body">
				<h4 class="header-title mt-0 mb-3">Transaction History</h4>
				<div class="table-responsive dash-social">
					<div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

						<div class="row">
							<div class="col-sm-12">
								<table id="datatable" class="table table-bordered dataTable no-footer"
									   role="grid" aria-describedby="datatable_info">
									<thead class="thead-light">
									<tr role="row">
										<th class="sorting_asc" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1" aria-sort="ascending"
											aria-label="No: activate to sort column descending"
											style="width: 25px;">No
										</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Date: activate to sort column ascending"
											style="width: 67px;">Date
										</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Time: activate to sort column ascending"
											style="width: 44px;">Time
										</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Transaction ID: activate to sort column ascending"
											style="width: 123px;">Transaction ID
										</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Type: activate to sort column ascending"
											style="width: 49px;">Type
										</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Value: activate to sort column ascending"
											style="width: 49px;">Value
										</th>
									</tr>
									<!--end tr-->
									</thead>
									<tbody>
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<tr role="row" class="odd">
										<td class="sorting_1">01</td>
										<td>14 Jan 2019</td>
										<td>12:05PM</td>
										<td>0001245368452136</td>
										<td><span class="badge badge-soft-danger">Sent</span></td>
										<td>$521.36</td>
									</tr>
									<tr role="row" class="even">
										<td class="sorting_1">02</td>
										<td>13 Jan 2019</td>
										<td>10:15AM</td>
										<td>0012369584712458</td>
										<td><span class="badge badge-soft-success">Received</span></td>
										<td>$990.00</td>
									</tr>
									<tr role="row" class="odd">
										<td class="sorting_1">03</td>
										<td>11 Jan 2019</td>
										<td>09:14PM</td>
										<td>0012457896321548</td>
										<td><span class="badge badge-soft-danger">Sent</span></td>
										<td>$321.21</td>
									</tr>
									<tr role="row" class="even">
										<td class="sorting_1">04</td>
										<td>08 Jan 2019</td>
										<td>12:05PM</td>
										<td>0126582125487896</td>
										<td><span class="badge badge-soft-success">Received</span></td>
										<td>$321.21</td>
									</tr>
									<tr role="row" class="odd">
										<td class="sorting_1">05</td>
										<td>06 Jan 2019</td>
										<td>11:30AM</td>
										<td>0125896312345878</td>
										<td><span class="badge badge-soft-danger">Sent</span></td>
										<td>$458.80</td>
									</tr>
									<tr role="row" class="even">
										<td class="sorting_1">06</td>
										<td>05 Jan 2019</td>
										<td>05:50PM</td>
										<td>0001245863254874</td>
										<td><span class="badge badge-soft-success">Received</span></td>
										<td>125.50</td>
									</tr>
									<tr role="row" class="odd">
										<td class="sorting_1">07</td>
										<td>04 Jan 2019</td>
										<td>08:10PM</td>
										<td>0000021253698745</td>
										<td><span class="badge badge-soft-danger">Sent</span></td>
										<td>$365.21</td>
									</tr>
									<tr role="row" class="even">
										<td class="sorting_1">08</td>
										<td>03 Jan 2019</td>
										<td>01:30PM</td>
										<td>0000212125487963</td>
										<td><span class="badge badge-soft-success">Received</span></td>
										<td>$843.21</td>
									</tr>
									<tr role="row" class="odd">
										<td class="sorting_1">09</td>
										<td>03 Jan 2019</td>
										<td>12:05PM</td>
										<td>0124563985478456</td>
										<td><span class="badge badge-soft-danger">Sent</span></td>
										<td>$335.15</td>
									</tr>
									<tr role="row" class="even">
										<td class="sorting_1">10</td>
										<td>02 Jan 2019</td>
										<td>03:15PM</td>
										<td>0125896325483658</td>
										<td><span class="badge badge-soft-success">Received</span></td>
										<td>$554.50</td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>
			<!--end card-body-->
		</div>
	</div>

</div>

