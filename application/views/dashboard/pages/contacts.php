<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<ul>
					<li style="font-size: 16px" ><i class="fa fa-envelope-square"></i>&nbsp;&nbsp;Email Address:&nbsp;&nbsp;<a href="mailto:info@crownpaints.co.ke">info@crownpaints.co.ke,

							sales@crownpaints.co.ke,

							marketing@crownpaints.co.ke</a> </li>
					<br>
					<br>
					<li style="font-size: 16px" ><i style="color:gray" class="fa fa-phone"></i>&nbsp;&nbsp;Call Centre Number:&nbsp;&nbsp 0709 887 000

						TEL: 020 2165703/5/6, 2032751 </li>
					<br>
					<br>
					<li style="font-size: 16px" ><i style="color:green" class="fa fa-whatsapp"></i>&nbsp;&nbsp;Whatsapp Number: 0709 887 000</li>
					<br>

				</ul>
			</div>
			<!--end card-body-->
		</div><!--end card-->
	</div><!--end col-->
</div>

