<div class="row">
	<div class="col-md-4">
		<div class="card report-card">
			<div class="card-body">
				<div class="float-right"><i class="fas fa-dollar-sign bg-soft-pink main-widgets-icon"></i>
				</div>
				<h4 class="title-text mt-0">Available Balance Credit Limit</h4>
				<h3 class="my-3">Ksh.500<small></small></h3>
			</div><!--end card-body-->
			<div class="card-body p-0">
				<div id="spark1" class="apex-charts"></div>
			</div>
		</div><!--end card--></div><!--end col-->
	<div class="col-md-4">
		<div class="card report-card">
			<div class="card-body">
				<div class="float-right"><i class="fas fa-shopping-cart bg-soft-success main-widgets-icon"></i>
				</div>
				<h4 class="title-text mt-0">Total Volume Sales (MTD)</h4>
				<h3 class="my-3">35,000 Litres<small></small></h3>
			</div><!--end card-body-->
			<div class="card-body p-0">
				<div id="spark3" class="apex-charts"></div>
			</div>
		</div><!--end card--></div>
	<div class="col-md-4">
		<div class="card report-card">
			<div class="card-body">
				<div class="float-right"><i class="fas fa-money bg-soft-warning main-widgets-icon"></i>
				</div>
				<h4 class="title-text mt-0">Total Value Sales (MTD)</h4>
				<h3 class="my-3">Ksh.8800 <small></small></h3>
			</div><!--end card-body-->
			<div class="card-body p-0">
				<div id="spark2" class="apex-charts"></div>
			</div>
		</div><!--end card--></div>
</div>
<style>
	#chartdiv {
		width: 750px;
		height: 500px;
		font-size: 12px;
	}
	#chartdiv2,#chartdiv3,#chartdiv4, .graph_div
	 {
		height: 300px;
		font-size: 12px;

	 }


</style>
<div class="row">
	<div class="col-md-8">
		<div class="card">
			<div class="card-body">
				<h4 class="mt-0 header-title">Total Volume Sales </h4>

				<div id="chartdiv"></div>
			</div>
		</div>

	</div>

	<div class="col-lg-4">
		<div class="card carousel-bg-img">
			<div class="card-body dash-info-carousel">
				<h4 class="mt-0 header-title">New Products</h4>
				<div id="carousel_2" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="media"><img src="<?php echo base_url() ?>/assets/images/min_1.png" height="400"
													class="mr-4" alt="...">

							</div>
						</div>
						<div class="carousel-item">
							<div class="media"><img src="<?php echo base_url() ?>/assets/images/min_2.jpg" style="height: 400px;width: 400px!important;" class="mr-4" alt="...">

							</div>
						</div>

					</div><a class="carousel-control-prev" href="#carousel_2" role="button"
							 data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span> </a><a class="carousel-control-next"
																	 href="#carousel_2" role="button" data-slide="next"><span
							class="carousel-control-next-icon" aria-hidden="true"></span> <span
							class="sr-only">Next</span></a>
				</div>
			</div>
			<!--end card-body-->
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-body">
				<h4 class="mt-0 header-title">Total Volume Sales </h4>

				<div id="chartdiv2"></div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="card">
			<div class="card-body">
				<h4 class="mt-0 header-title">Products Range </h4>

				<div id="chartdiv3"></div>
			</div>
		</div>

	</div>

</div>

<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-body">
				<h4 class="mt-0 header-title">Premium Decorative Products </h4>
				<div class="graph_div" id="chartdiv11"></div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="card">
			<div class="card-body">
				<h4 class="mt-0 header-title">Premium Automotive and Industrial Products </h4>
				<div class="graph_div" id="chartdiv6"></div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-body">
				<h4 class="mt-0 header-title">Economy Products </h4>
				<div class="graph_div" id="chartdiv7"></div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="card">
			<div class="card-body">
				<h4 class="mt-0 header-title">Wood Finish Products </h4>
				<div class="graph_div" id="chartdiv8"></div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-body">
				<h4 class="mt-0 header-title">New Products </h4>
				<div class="graph_div" id="chartdiv9"></div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="card">
			<div class="card-body">
				<h4 class="mt-0 header-title">Pidilite Products </h4>
				<div class="graph_div" id="chartdiv10"></div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="mt-0 header-title">Privilege Club status</h4>

				<div id="chartdiv4"></div>
			</div>
		</div>
	</div>

</div>
<script>


</script>
