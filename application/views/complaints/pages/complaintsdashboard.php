
    <div class="container-fluid">
        <!-- Page-Title -->
        <!--end row-->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <button type="button"
                            class="btn btn-primary waves-effect waves-light float-right mb-3" data-toggle="modal"
                            data-animation="bounce" data-target=".bs-example-modal-lg">+ Record New Complain</button>
                        <h4 class="header-title mt-0 mb-3">All Complaints From CRM</h4>
                        <div class="table-responsive dash-social">
                            <div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                               
                                <div class="row">
                                    <div class="col-sm-12">
                                    <table id="datatable" class="table dataTable no-footer" role="grid"
                                            aria-describedby="datatable_info">
                                            <thead class="thead-light">
                                                <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="datatable"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Lead: activate to sort column descending"
                                                        style="width: 207px;">Complaint Type</th>
                                                    
                                                    <th class="sorting" tabindex="0" aria-controls="datatable"
                                                        rowspan="1" colspan="1"
                                                        aria-label="Status: activate to sort column ascending"
                                                        style="width: 87px;">Status</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable"
                                                        rowspan="1" colspan="1"
                                                        aria-label="Action: activate to sort column ascending"
                                                        style="width: 82px;">Created At</th>
                                                </tr>
                                                <!--end tr-->
                                            </thead>
                                            <tbody>
                                                <!--end tr-->
                                                <!--end tr-->
                                                <!--end tr-->
                                                <!--end tr-->
                                                <!--end tr-->
                                                <!--end tr-->
                                                <tr role="row" class="odd">
                                                    <td class="sorting_1">Product Complaint
                                                    </td>
                                                    <td><span class="badge badge-soft-purple">Processing</span></td>
                                                    <td>2020-01-26
                                                    </td>
                                                </tr>
                                                <tr role="row" class="even">
                                                    <td class="sorting_1">Quotation Complaint</td>
                                                    <td><span class="badge badge-soft-primary">Processing</span></td>
                                                    <td>2020-01-26
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div><!-- container -->
    <!--  Modal content for the above example -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myLargeModalLabel">Record  New Complaint</h5><button type="button"
                        class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-md-12">
								<div class="form-group">
								<label for="example-input1-group1">Complaint Type</label>
								<div class="input-group">
									<div class="input-group-prepend"></div>
									<select required class="form-control">
										<option>choose</option>
										<option>Product Complaint </option>
										<option>Quotation Complaint</option>
									</select>
								</div>
								</div>


                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group"><label for="status-select" class="mr-2">Description</label> 
                                <textarea class="form-control" rows="5" id="message"></textarea>
                                </div>
                            </div>
                        </div><button type="button" class="btn btn-sm btn-primary">Save</button> <button type="button"
                            class="btn btn-sm btn-danger">Delete</button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
