
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Quote Complaints Form</h4>
                     
                        <form>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-input1-group1">Complaint</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">
                                                <i class="fas fa-exclamation"></i></span></div>
                                                <select class="form-control">
                                    <option>choose</option>
                                    <option>Delayed responses on quotation request</option>
                                    <option>Wrong Quote</option>
                                    <option>High Quotation</option>

                                </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="message">Description</label>
                                        <textarea class="form-control" rows="5" id="description"></textarea>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-danger">Cancel</button>
                        </form>
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->
            </div>
        </div>
        <!--end row-->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mt-0 mb-3">Quotation Complaints</h4>
                        <div class="table-responsive dash-social">
                            <div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                             
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="datatable" class="table dataTable no-footer" role="grid"
                                            aria-describedby="datatable_info">
                                            <thead class="thead-light">
                                                <tr role="row">
                                       
                                                        <th class="sorting" tabindex="0" aria-controls="datatable"
                                                        rowspan="1" colspan="1"
                                                        aria-label="Email: activate to sort column ascending"
                                                        style="width: 144px;">Complaints</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable"
                                                        rowspan="1" colspan="1"
                                                        aria-label="Status: activate to sort column ascending"
                                                        style="width: 87px;">Status</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable"
                                                        rowspan="1" colspan="1"
                                                        aria-label="Action: activate to sort column ascending"
                                                        style="width: 82px;">Created At</th>
                                                </tr>
                                                <!--end tr-->
                                            </thead>
                                            <tbody>
                                      
                                                <tr role="row" class="odd">
                                                  
                                                    <td>High Quotation</td>
                                                    <td><span class="badge badge-soft-purple">Processing</span></td>
                                                    <td>2020-01-26
                                                    </td>
                                                </tr>
                                                <tr role="row" class="even">
                                          
                                                    <td>Wrong Quote</td>
                                                    <td><span class="badge badge-soft-purple">Processing</span></td>
                                                    <td>2020-01-26
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                   
                            </div>
                        </div>
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div><!-- container -->
