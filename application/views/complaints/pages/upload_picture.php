
<div class="container-fluid">
	<!-- Page-Title -->
	<!--end row-->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<button type="button"
							class="btn btn-primary waves-effect waves-light float-right mb-3" data-toggle="modal"
							data-animation="bounce" data-target=".bs-example-modal-lg">+ Upload New Photo</button>
					<h4 class="header-title mt-0 mb-3">Photos Uploaded</h4>
					<div class="table-responsive dash-social">
						<div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

							<div class="row">
								<div class="col-sm-12">
									<table id="datatable" class="table dataTable no-footer" role="grid"
										   aria-describedby="datatable_info">
										<thead class="thead-light">
										<tr role="row">
											<th >Complaint Type</th>
											<th >Status</th>
											<th>Uploaded On</th>
										</tr>
										<!--end tr-->
										</thead>
										<tbody>

										<tr role="row" class="odd">
											<td >Product Complaint
											</td>
											<td><span class="badge badge-soft-purple">Processing</span></td>
											<td>2020-01-26
											</td>
										</tr>
										<tr role="row" class="even">
											<td >Quotation Complaint</td>
											<td><span class="badge badge-soft-primary">Processing</span></td>
											<td>2020-01-26
											</td>
										</tr>

										</tbody>
									</table>
								</div>
							</div>

						</div>
					</div>
				</div>
				<!--end card-body-->
			</div>
			<!--end card-->
		</div>
		<!--end col-->
	</div>
	<!--end row-->
</div><!-- container -->
<!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
	 aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title mt-0" id="myLargeModalLabel">Upload New Photo</h5><button type="button"
																									  class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="example-input1-group1">Complaint Type</label>
								<div class="input-group">
									<div class="input-group-prepend"></div>
									<select required class="form-control">
										<option>choose</option>
										<option>Product Complaint </option>
										<option>Quotation Complaint</option>
									</select>
								</div>
							</div>


						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="LeadEmail">Upload Picture</label> <input type="file"
																										 class="form-control" id="LeadEmail" required=""></div>
						</div>
					</div>
					<div class="row">
						<!-- <div class="col-md-6">
							<div class="form-group"><label for="PhoneNo">Phone No</label> <input type="text"
									class="form-control" id="PhoneNo" required=""></div>
						</div> -->
						<div class="col-md-12">
							<div class="form-group"><label for="status-select" class="mr-2">Description</label>
								<textarea class="form-control" rows="5" id="message"></textarea>
							</div>
						</div>
					</div><button type="button" class="btn btn-sm btn-primary">Save</button> <button type="button"
																									 class="btn btn-sm btn-danger">Delete</button>
				</form>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
