
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Product Complaints Form</h4>
                     
                        <form>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-input1-group1">Product Name</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="fa fa-tags"></i></span></div><select class="form-control">
                                    <option>choose</option>
                                    <option>MATT FINISH BLACK|4L </option>
                                    <option>COVERMATT EMULSION BUTTERMILK|20L</option>
                                </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-input1-group1">Product Code</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="fa fa-hashtag"></i></span></div><input type="number"
                                                id="example-input1-group1" name="example-input1-group1"
                                                class="form-control" placeholder="Quantity">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-input1-group1">Complaint</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">
                                                <i class="fas fa-exclamation"></i></span></div>
                                                <select class="form-control">
                                    <option>choose</option>
                                    <option>Bittyness/Dust in paint film </option>
                                    <option>Bubbling</option>
                                    <option>Film Flaking/Peel Off</option>

                                </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="message">Description</label>
                                        <textarea class="form-control" rows="5" id="description"></textarea>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-danger">Cancel</button>
                        </form>
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->
            </div>
        </div>
        <!--end row-->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mt-0 mb-3">Product Complaints</h4>
                        <div class="table-responsive dash-social">
                            <div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                             
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="datatable" class="table dataTable no-footer" role="grid"
                                            aria-describedby="datatable_info">
                                            <thead class="thead-light">
                                                <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="datatable"
                                                        rowspan="1" colspan="1" aria-sort="ascending"
                                                        aria-label="Lead: activate to sort column descending"
                                                        style="width: 207px;">Product Name</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable"
                                                        rowspan="1" colspan="1"
                                                        aria-label="Email: activate to sort column ascending"
                                                        style="width: 144px;">Product Code</th>
                                                        <th class="sorting" tabindex="0" aria-controls="datatable"
                                                        rowspan="1" colspan="1"
                                                        aria-label="Email: activate to sort column ascending"
                                                        style="width: 144px;">Complaints</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable"
                                                        rowspan="1" colspan="1"
                                                        aria-label="Status: activate to sort column ascending"
                                                        style="width: 87px;">Status</th>
                                                    <th class="sorting" tabindex="0" aria-controls="datatable"
                                                        rowspan="1" colspan="1"
                                                        aria-label="Action: activate to sort column ascending"
                                                        style="width: 82px;">Created At</th>
                                                </tr>
                                                <!--end tr-->
                                            </thead>
                                            <tbody>
                                      
                                                <tr role="row" class="odd">
                                                  
                                                    <td>SUPER GLOSS ALKYD BASE PASTEL</td>
                                                    <td>C08ACSEA2B</td>
                                                    <td>Bittyness/Dust in paint film</td>
                                                    <td><span class="badge badge-soft-purple">Processing</span></td>
                                                    <td>2020-01-26
                                                    </td>
                                                </tr>
                                                <tr role="row" class="even">
                                                <td>SUPER GLOSS ALKYD BASE PASTEL</td>
                                                    <td>C08ACSEA2B</td>
                                                    <td>Bubbling</td>
                                                    <td><span class="badge badge-soft-purple">Processing</span></td>
                                                    <td>2020-01-26
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                   
                            </div>
                        </div>
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div><!-- container -->
    <!--  Modal content for the above example -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myLargeModalLabel">Create New Order</h5><button type="button"
                        class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form>
                      
                        <div class="row">
                            <div class="col-md-6">

                                <div class="form-group"><label for="LeadName">Product Name</label><select class="form-control">
                                    <option>Select</option>
                                    <option>MATT FINISH BLACK|4L </option>
                                    <option>COVERMATT EMULSION BUTTERMILK|20L</option>
                                </select></div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group"><label for="LeadEmail">Quantity</label> <input type="number"
                                        class="form-control" id="LeadEmail" required=""></div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- <div class="col-md-6">
                                <div class="form-group"><label for="PhoneNo">Phone No</label> <input type="text"
                                        class="form-control" id="PhoneNo" required=""></div>
                            </div> -->
                            <div class="col-md-6">
                                <div class="form-group"><label for="status-select" class="mr-2">Description</label>
                                    <textarea class="form-control" rows="5" id="message"></textarea>
                                </div>
                            </div>
                        </div><button type="button" class="btn btn-sm btn-primary">Save</button> <button type="button"
                            class="btn btn-sm btn-danger">Delete</button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
