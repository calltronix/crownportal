<div class="row">
	<div class="card report-card">
		<div class="card-body">

			<span class="usd-rate font-14"><b>
                                               </b></span>
			<ul>
				<?php $query = $this->db->get('tbl_product_category2')->result(); ?>
				<?php foreach ($query as $category){
					$this->db->where('categoryId',$category->categoryID);
					$count = $this->db->count_all_results('tbl_productsPdf');
					if ($count > 0)
					{
						$this->db->where('categoryId',$category->categoryID);
						$get_pdf = $this->db->get('tbl_productsPdf')->row();
						$pdf = $get_pdf->pdfLink;
					}else
					{
						$pdf = "1";
					}

					?>
					<br><br>
					<li data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-lg<?php echo $category->categoryID; ?>">
						<h5><?php echo $category->category; ?></h5>
					</li>
					<div class="modal fade bs-example-modal-lg<?php echo $category->categoryID; ?>" tabindex="-1" role="dialog"
						 aria-labelledby="myLargeModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header"><h5 class="modal-title mt-0"
															  id="myLargeModalLabel"><?php echo $category->category; ?></h5>
									<button type="button" class="close" data-dismiss="modal"
											aria-hidden="true">×
									</button>
								</div>
								<div class="modal-body">
									<div class="row">
										<?php if ($pdf === '1'){ ?>
											Product List Not Uploaded yet.
										<?php } else {?>


										<iframe src="<?php echo $pdf ;?>" height="800" width="800"></iframe>

 <?php } ?>
									</div>


								</div>
								<div class="modal-footer">

									<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
								</div>
							</div><!-- /.modal-content --></div><!-- /.modal-dialog --></div>
				<?php }?>

			</ul>


		</div><!--end card-body-->
		<div class="card-body p-0">
			<div id="spark1" class="apex-charts"></div>
		</div>
	</div>
</div>

