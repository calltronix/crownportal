

<div class="page-wrapper"><!-- Page Content-->
	<div class="page-content-tab">
		<div class="container-fluid">
<div class="row">
	<div class="col-sm-12">
		<div class="page-title-box">
			<div class="float-right">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0);">Dealer Portal</a></li>
					<li class="breadcrumb-item"><a href="javascript:void(0);">Sales</a></li>
					<li class="breadcrumb-item active">Sales Dashboard</li>
				</ol>
			</div>
			<h4 class="page-title">Sales Dashboard</h4>
		</div>
		<!--end page-title-box-->
	</div>
	<!--end col-->
</div><!-- end page title end breadcrumb -->
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<div class="wrap">
					<!-- <div class="jctkr-label"><span><i class="fas fa-exchange-alt mr-2"></i>Change 24
							Hours</span></div> -->
					<div class="js-conveyor-example jctkr-wrapper jctkr-initialized">
						<ul style="width: 2295px; left: -329.025px;">
							<li style=""><img src="../assets/images/coins/btc.png" alt=""
											  class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Account Limit:
                                                8435.21</b></span>
							</li>
							<li style=""><img src="../assets/images/coins/dash.png" alt=""
											  class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Account Balance:
                                                1233.54</b></span>
							</li>

							<li style=""><img src="../assets/images/coins/btc.png" alt=""
											  class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Credit Limit:
                                                224446.64</b></span>
							</li>
							<li style=""><img src="../assets/images/coins/lite.png" alt=""
											  class="thumb-xs rounded"> <span class="usd-rate font-14"><b>Commitment Limit:
                                                129999.91</b></span>
							</li>

						</ul>
					</div>
				</div>
			</div>
			<!--end card-body-->
		</div>
		<!--end card-->
	</div>
	<!--end col-->
</div>
<div class="row">
	<div class="col-lg-4">
		<div class="card">
			<div class="card-body profile-card">
				<div class="media align-items-center"><img src="../assets/images/users/avatar.jpg" alt="user"
														   class="rounded-circle thumb-lg">
					<div class="media-body ml-3 align-self-center">
						<h5 class="pro-title">Dealer Code</h5>
						<p class="mb-1 text-muted">Dealer Name</p>
					</div>
					<!-- <div class="action-btn"><button class="mr-1 btn btn-sm btn-soft-info"><i
								class="fas fa-pen"></i></button> <button class="btn btn-sm btn-soft-danger"><i
								class="far fa-trash-alt"></i></button></div> -->
				</div>
			</div>
			<!--end card-body-->
		</div>
		<!--end card-->
	</div><!-- end col-->
	<div class="col-lg-8">
		<div class="row">
			<div class="col-lg-4">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-4 align-self-center">
								<div class="icon-info"><i class="mdi mdi-account-group text-warning"></i></div>
							</div>
							<div class="col-8 align-self-center text-right">
								<div class="ml-2">
									<p class="mb-1 text-muted">Total Orders</p>
									<h4 class="mt-0 mb-1">1935</h4>
								</div>
							</div>
						</div>
						<!-- <div class="progress mt-2" style="height:3px;">
							<div class="progress-bar bg-warning" role="progressbar" style="width: 55%;"
								aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
						</div> -->
					</div>
					<!--end card-body-->
				</div>
				<!--end card-->
			</div>
			<!--end col-->
			<div class="col-lg-4">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-4 align-self-center">
								<div class="icon-info"><i class="mdi mdi-folder-open text-purple"></i></div>
							</div>
							<div class="col-8 align-self-center text-right">
								<div class="ml-2">
									<div class="ml-2">
										<p class="mb-0 text-muted">Delivered</p>
										<h4 class="mt-0 mb-1 d-inline-block">1240</h4>

									</div>
								</div>
							</div>
						</div>
						<!-- <div class="progress mt-2" style="height:3px;">
							<div class="progress-bar bg-purple" role="progressbar" style="width: 39%;"
								aria-valuenow="39" aria-valuemin="0" aria-valuemax="100"></div>
						</div> -->
					</div>
					<!--end card-body-->
				</div>
				<!--end card-->
			</div>
			<!--end col-->
			<div class="col-lg-4">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-4 align-self-center">
								<div class="icon-info"><i class="mdi mdi-folder text-pink"></i></div>
							</div>
							<div class="col-8 align-self-center text-right">
								<div class="ml-2">
									<p class="mb-0 text-muted">Processing</p>
									<h4 class="mt-0 mb-1 d-inline-block">240</h4>
								</div>
							</div>
						</div>
						<!-- <div class="progress mt-2" style="height:3px;">
							<div class="progress-bar bg-pink" role="progressbar" style="width: 48%;"
								aria-valuenow="48" aria-valuemin="0" aria-valuemax="100"></div>
						</div> -->
					</div>
					<!--end card-body-->
				</div>
				<!--end card-->
			</div>
			<!--end col-->
		</div>
		<!--end row-->
	</div>
	<!--end col-->
</div>
<!--end row-->
<div class="row">
	<div class="col-lg-8">
		<div class="card">
			<div class="card-body"><button type="button"
										   class="btn btn-primary waves-effect waves-light float-right mb-3" data-toggle="modal"
										   data-animation="bounce" data-target=".bs-example-modal-lg">+ Create New Order</button>
				<h4 class="header-title mt-0 mb-3">All Orders</h4>
				<div class="table-responsive dash-social">
					<div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

						<div class="row">
							<div class="col-sm-12">
								<table id="datatable" class="table dataTable no-footer" role="grid"
									   aria-describedby="datatable_info">
									<thead class="thead-light">
									<tr role="row">
										<th class="sorting_asc" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1" aria-sort="ascending"
											aria-label="Lead: activate to sort column descending"
											style="width: 207px;">SAP OrderID</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Email: activate to sort column ascending"
											style="width: 144px;">OrderID</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Phone No: activate to sort column ascending"
											style="width: 108px;">Product Code</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Company: activate to sort column ascending"
											style="width: 151px;">Product Name</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Status: activate to sort column ascending"
											style="width: 87px;">Status</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Action: activate to sort column ascending"
											style="width: 82px;">Created At</th>
									</tr>
									<!--end tr-->
									</thead>
									<tbody>
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<tr role="row" class="odd">
										<td class="sorting_1">2000
										</td>
										<td>34</td>
										<td>C08ACSEA2B</td>
										<td>SUPER GLOSS ALKYD BASE PASTEL</td>
										<td><span class="badge badge-soft-purple">Processing</span></td>
										<td>2020-01-26
										</td>
									</tr>
									<tr role="row" class="even">
										<td class="sorting_1">2001</td>
										<td>35</td>
										<td>C09AA1260D</td>
										<td>CROWN EGGSHELL WHITE</td>
										<td><span class="badge badge-soft-primary">Processing</span></td>
										<td>2020-01-26
										</td>
									</tr>

									</tbody>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>
			<!--end card-body-->
		</div>
		<!--end card-->
	</div>

	<div class="col-lg-4">
		<div class="card carousel-bg-img">
			<div class="card-body dash-info-carousel">
				<h4 class="mt-0 header-title">New Products</h4>
				<div id="carousel_2" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="media"><img src="../assets/images/products/prod1.jpeg" height="400"
													class="mr-4" alt="...">
								<!-- <div class="media-body align-self-center">
									<h4 class="mt-0">Important Watch</h4>
									<p class="text-muted mb-0">$99.00</p>
								</div> -->
							</div>
						</div>
						<div class="carousel-item">
							<div class="media"><img src="../assets/images/products/prod2.jpeg" height="400"
													class="mr-4" alt="...">
								<!-- <div class="media-body align-self-center">
									<h4 class="mt-0">Wireless Headphone</h4>
									<p class="text-muted mb-0">$39.00</p>
								</div> -->
							</div>
						</div>
						<div class="carousel-item">
							<div class="media"><img src="../assets/images/products/prod3.jpeg" height="400"
													class="mr-4" alt="...">
								<div class="media-body align-self-center">
									<h4 class="mt-0">Leather Bag</h4>
									<p class="text-muted mb-0">$49.00</p>
								</div>
							</div>
						</div>
						<div class="carousel-item">
							<div class="media"><img src="../assets/images/products/prod4.jpeg" height="400"
													class="mr-4" alt="...">
								<!-- <div class="media-body align-self-center">
									<h4 class="mt-0">Leather Bag</h4>
									<p class="text-muted mb-0">$49.00</p>
								</div> -->
							</div>
						</div>
					</div><a class="carousel-control-prev" href="#carousel_2" role="button"
							 data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span> </a><a class="carousel-control-next"
																	 href="#carousel_2" role="button" data-slide="next"><span
							class="carousel-control-next-icon" aria-hidden="true"></span> <span
							class="sr-only">Next</span></a>
				</div>
			</div>
			<!--end card-body-->
		</div>
	</div>
	<!--end col-->
</div>
			<!--end footer--></div>
		<!-- end page content --></div>
