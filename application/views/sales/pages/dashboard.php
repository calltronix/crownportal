<div class="row">
	<div class="col-lg-8">
		<div class="card">
			<div class="card-body">
				<button type="button"
						class="btn btn-gradient-primary waves-effect waves-light float-right mb-3"
						data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-lg">+ Create
					New order
				</button>
				<h4 class="header-title mt-0 mb-3">All Orders</h4>
				<div class="table-responsive dash-social">
					<div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

						<div class="row">
							<div class="col-sm-12">
								<table id="datatable" class="table dataTable no-footer" role="grid"
									   aria-describedby="datatable_info">
									<thead class="thead-light">
									<tr role="row">
										<th class="sorting_asc" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1" aria-sort="ascending"
											aria-label="Lead: activate to sort column descending"
											style="width: 207px;">SAP OrderID
										</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Email: activate to sort column ascending"
											style="width: 144px;">OrderID
										</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Phone No: activate to sort column ascending"
											style="width: 108px;">Product Code
										</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Company: activate to sort column ascending"
											style="width: 151px;">Product Name
										</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Status: activate to sort column ascending"
											style="width: 87px;">Service Status
										</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Action: activate to sort column ascending"
											style="width: 82px;">Creation Date
										</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Action: activate to sort column ascending"
											style="width: 82px;">Creation Time
										</th>
									</tr>
									<!--end tr-->
									</thead>
									<tbody>
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<tr role="row" class="odd">
										<td class="sorting_1">2000
										</td>
										<td>34</td>
										<td>C08ACSEA2B</td>
										<td>SUPER GLOSS ALKYD BASE PASTEL</td>
										<td><span class="badge badge-soft-purple">Processing</span></td>
										<td>2020-01-26
										</td>
										<td>11:35
										</td>
									</tr>
									<tr role="row" class="even">
										<td class="sorting_1">2001</td>
										<td>35</td>
										<td>C09AA1260D</td>
										<td>CROWN EGGSHELL WHITE</td>
										<td><span class="badge badge-soft-primary">Processing</span></td>
										<td>2020-01-26
										</td>
										<td>12:58
										</td>
									</tr>

									</tbody>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>
			<!--end card-body-->
		</div>
		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
			 aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header"><h5 class="modal-title mt-0"
												  id="myLargeModalLabel">Create New
							Order</h5>
						<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">×
						</button>
					</div>
					<div class="modal-body">
						<form>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group"><label for="Location">Product Photo </label>
										<input type="file" class="form-control"
											   id="Location" required=""></div>
								</div>
								<div class="col-md-6">
									<div class="form-group"><label for="PhoneNo">Product Name
										</label> <input type="text"
														class="form-control"
														id="PhoneNo" required="">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group"><label for="Location">Shade Name </label>
										<input type="text" class="form-control"
											   id="Location" required=""></div>
								</div>
								<div class="col-md-4">
									<div class="form-group"><label for="PhoneNo">Quantity
										</label> <input type="text"
														class="form-control"
														id="PhoneNo" required="">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group"><label for="PhoneNo">Pack Size
										</label> <input type="text"
														class="form-control"
														id="PhoneNo" required="">
									</div>
								</div>
							</div>
							<button type="button" class="btn btn-sm btn-primary">
								Save
							</button>
							<button type="button" class="btn btn-sm btn-danger">
								Delete
							</button>
						</form>
					</div>
				</div><!-- /.modal-content --></div><!-- /.modal-dialog --></div>
		<!--end card-->
	</div>

	<div class="col-lg-4">
		<div class="card carousel-bg-img">
			<div class="card-body dash-info-carousel">
				<h4 class="mt-0 header-title">New Products</h4>
				<div id="carousel_2" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="media"><img src="<?php echo base_url() ?>/assets/images/min_1.png" height="400"
													class="mr-4" alt="...">

							</div>
						</div>
						<div class="carousel-item">
							<div class="media"><img src="<?php echo base_url() ?>/assets/images/min_2.jpg" style="height: 400px;width: 400px!important;" class="mr-4" alt="...">

							</div>
						</div>

					</div><a class="carousel-control-prev" href="#carousel_2" role="button"
							 data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span> </a><a class="carousel-control-next"
																	 href="#carousel_2" role="button" data-slide="next"><span
								class="carousel-control-next-icon" aria-hidden="true"></span> <span
								class="sr-only">Next</span></a>
				</div>
			</div>
			<!--end card-body-->
		</div>
	</div>
	<!--end col-->
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body"><h4 class="header-title mt-0">Top 10 Products Ordered</h4>
				<div class="">
					<div id="chartdivs" class="apex-charts"></div>
				</div>
			</div><!--end card-body--></div><!--end card-->
		<!--end row--></div>
</div>
<!--end footer--></div>
<!-- end page content --></div>
<!-- Styles -->
<style>
	#chartdivs {
		width: 100%;
		height: 500px;
	}

</style>

<!-- Resources -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<!-- Chart code -->
<script>
	am4core.ready(function () {

// Themes begin
		am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
		var chart = am4core.create("chartdivs", am4charts.XYChart);

// Add data
		chart.data = [{
			"country": "Product 1",
			"visits": 55
		}, {
			"country": "Product 2",
			"visits": 45
		}, {
			"country": "Product 3",
			"visits": 35
		}, {
			"country": "Product 4",
			"visits": 25
		}, {
			"country": "Product 5",
			"visits": 20
		},
			{
				"country": "Product 6",
				"visits": 15
			},
			{
				"country": "Product 7",
				"visits": 25
			},
			{
				"country": "Product 8",
				"visits": 22
			},
			{
				"country": "Product 9",
				"visits": 19
			},
			{
				"country": "Product 10",
				"visits": 10
			}];

// Create axes

		var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.dataFields.category = "country";
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.minGridDistance = 30;

		categoryAxis.renderer.labels.template.adapter.add("dy", function (dy, target) {
			if (target.dataItem && target.dataItem.index & 2 == 2) {
				return dy + 25;
			}
			return dy;
		});

		var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
		var series = chart.series.push(new am4charts.ColumnSeries());
		series.dataFields.valueY = "visits";
		series.dataFields.categoryX = "country";
		series.name = "Visits";
		series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
		series.columns.template.fillOpacity = .8;

		var columnTemplate = series.columns.template;
		columnTemplate.strokeWidth = 2;
		columnTemplate.strokeOpacity = 1;

	}); // end am4core.ready()
</script>
