

<div class="row">
	<div class="col-lg-8">
		<div class="card">
			<div class="card-body">
				<h4 class="header-title mt-0 mb-3">All FOC Requests</h4>
				<div class="table-responsive dash-social">
					<div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

						<div class="row">
							<div class="col-sm-12">
								<table id="datatable" class="table dataTable no-footer" role="grid"
									   aria-describedby="datatable_info">
									<thead class="thead-light">
									<tr role="row">

										<th>Sales Order Number</th>
										<th>Time of Placing Order</th>
										<th> Status</th>
										<th>Fully Serviced</th>
										<th>Pending Items</th>
										<th >Action </th>


									</tr>
									<!--end tr-->
									</thead>
									<tbody>

									<tr role="row" class="even">
										<td>123</td>
										<td>12:45 PM</td>
										<td><span class="badge badge-soft-primary">Processing</span></td>
										<td><span class="badge badge-soft-primary">YES</span></td>
										<td><span class="badge badge-warning">3</span>
										</td>
										<td><button type="button"
													class="btn btn-sm btn-gradient-primary waves-effect waves-light float-right mb-3"
													data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-lg2">View
											</button>
										</td>


									</tr>
									<tr role="row" class="even">
										<td>234</td>
										<td>02:45 PM</td>
										<td><span class="badge badge-soft-primary">Processing</span></td>
										<td><span class="badge badge-soft-primary">NO</span></td>
										<td><span class="badge badge-warning">1</span>
										</td>
										<td><button type="button"
													class="btn btn-sm btn-gradient-primary waves-effect waves-light float-right mb-3"
													data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-lg2">View
											</button>
										</td>


									</tr>

									</tbody>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>
			<!--end card-body-->
		</div>

		<div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog"
			 aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header"><h5 class="modal-title mt-0"
												  id="myLargeModalLabel">
							FOC Request Status</h5>
						<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">×
						</button>
					</div>
					<div class="modal-body">
						<form>
							<div class="row">
								<div class="col-md-6">
									Sales Order Number:123

								</div>
								<div class="col-md-6">
									Time of Placing Order:12:45 PM

								</div>
							</div>
							<br>
							<hr>
							<div class="row">
								<div class="col-md-6">
									Status : <span class="badge badge-soft-primary">Processing</span>


								</div>

								<div class="col-md-6">
									 Fully Serviced: YES

								</div>
							</div>
							<br>
							<hr>
							<div class="row">
								<div class="col-md-6">
									Pending Items:
									<ul>
										<li>Item 1</li>
										<li>Item 2</li>
									</ul>
								</div>

							</div>
							<button type="button" class="btn btn-sm btn-primary">
								Save
							</button>
							<button type="button" class="btn btn-sm btn-danger">
								Delete
							</button>
						</form>
					</div>
				</div><!-- /.modal-content --></div><!-- /.modal-dialog --></div>
		<!--end card-->
	</div>

	<div class="col-lg-4">
		<div class="card carousel-bg-img">
			<div class="card-body dash-info-carousel">
				<h4 class="mt-0 header-title">New Products</h4>
				<div id="carousel_2" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="media"><img src="<?php echo base_url() ?>/assets/images/min_1.png" height="400"
													class="mr-4" alt="...">

							</div>
						</div>
						<div class="carousel-item">
							<div class="media"><img src="<?php echo base_url() ?>/assets/images/min_2.jpg" style="height: 400px;width: 400px!important;" class="mr-4" alt="...">

							</div>
						</div>

					</div><a class="carousel-control-prev" href="#carousel_2" role="button"
							 data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span> </a><a class="carousel-control-next"
																	 href="#carousel_2" role="button" data-slide="next"><span
								class="carousel-control-next-icon" aria-hidden="true"></span> <span
								class="sr-only">Next</span></a>
				</div>
			</div>
			<!--end card-body-->
		</div>
	</div>
	<!--end col-->
</div>

<!--end footer--></div>
<!-- end page content --></div>
<!-- Styles -->

