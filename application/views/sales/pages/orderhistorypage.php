
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">History Search Form</h4>
                        <p class="text-muted mb-3">Search for orders with OrderID.</p>
                        <form action="">
                            <div class="form-group"><label for="exampleInputPassword1">Order ID</label>
                                <div class="input-group"><input type="text" class="form-control"
                                        placeholder="Search for..." aria-label="Search for..."> <span
                                        class="input-group-append"><button class="btn btn-primary"
                                            type="button">Go!</button></span></div>
                            </div>
                        </form>
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->
            </div>
        </div>
