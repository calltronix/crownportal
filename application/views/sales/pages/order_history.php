

<!--end row-->
<div class="row">
	<div class="col-8">
		<div class="card">
			<div class="card-body">
				<h4 class="header-title mt-0 mb-3"> Orders Placed</h4>
				<div class="table-responsive dash-social">
					<div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

						<div class="row">
							<div class="col-sm-12">
								<table id="datatable" class="table dataTable no-footer" role="grid"
									   aria-describedby="datatable_info">
									<thead class="thead-light">
									<tr role="row">
										<th class="sorting_asc" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1" aria-sort="ascending"
											aria-label="Lead: activate to sort column descending"
											style="width: 207px;">SAP OrderID</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Email: activate to sort column ascending"
											style="width: 144px;">OrderID</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Phone No: activate to sort column ascending"
											style="width: 108px;">Product Code</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Company: activate to sort column ascending"
											style="width: 151px;">Product Name</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Status: activate to sort column ascending"
											style="width: 87px;">Status</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Action: activate to sort column ascending"
											style="width: 82px;">Created At</th>
									</tr>
									<!--end tr-->
									</thead>
									<tbody>
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<!--end tr-->
									<tr role="row" class="odd">
										<td class="sorting_1">2000
										</td>
										<td>34</td>
										<td>C08ACSEA2B</td>
										<td>SUPER GLOSS ALKYD BASE PASTEL</td>
										<td><span class="badge badge-soft-purple">Processing</span></td>
										<td>2020-01-26
										</td>
									</tr>
									<tr role="row" class="even">
										<td class="sorting_1">2001</td>
										<td>35</td>
										<td>C09AA1260D</td>
										<td>CROWN EGGSHELL WHITE</td>
										<td><span class="badge badge-soft-primary">Processing</span></td>
										<td>2020-01-26
										</td>
									</tr>

									</tbody>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>
			<!--end card-body-->

		</div>
		<div class="card">
			<div class="card-body">
				<h4 class="header-title mt-0 mb-3"> Orders Serviced</h4>
				<div class="table-responsive dash-social">
					<div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

						<div class="row">
							<div class="col-sm-12">
								<table id="datatable" class="table dataTable no-footer" role="grid"
									   aria-describedby="datatable_info">
									<thead class="thead-light">
									<tr role="row">
										<th class="sorting_asc" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1" aria-sort="ascending"
											aria-label="Lead: activate to sort column descending"
											style="width: 207px;">SAP OrderID</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Email: activate to sort column ascending"
											style="width: 144px;">OrderID</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Phone No: activate to sort column ascending"
											style="width: 108px;">Product Code</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Company: activate to sort column ascending"
											style="width: 151px;">Product Name</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Status: activate to sort column ascending"
											style="width: 87px;">Status</th>
										<th class="sorting" tabindex="0" aria-controls="datatable"
											rowspan="1" colspan="1"
											aria-label="Action: activate to sort column ascending"
											style="width: 82px;">Created At</th>
									</tr>
									<!--end tr-->
									</thead>
									<tbody>

									<tr role="row" class="odd">
										<td class="sorting_1">2000
										</td>
										<td>34</td>
										<td>C08ACSEA2B</td>
										<td>SUPER GLOSS ALKYD BASE PASTEL</td>
										<td><span class="badge badge-soft-purple">Serviced</span></td>
										<td>2020-01-26
										</td>
									</tr>
									<tr role="row" class="even">
										<td class="sorting_1">2001</td>
										<td>35</td>
										<td>C09AA1260D</td>
										<td>CROWN EGGSHELL WHITE</td>
										<td><span class="badge badge-soft-primary">Serviced</span></td>
										<td>2020-01-26
										</td>
									</tr>

									</tbody>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!--end card-->
	</div>
	<!--end col-->
	<div class="col-lg-4">
		<div class="card carousel-bg-img">
			<div class="card-body dash-info-carousel">
				<h4 class="mt-0 header-title">New Products</h4>
				<div id="carousel_2" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="media"><img src="<?php echo base_url() ?>/assets/images/min_1.png" height="400"
													class="mr-4" alt="...">

							</div>
						</div>
						<div class="carousel-item">
							<div class="media"><img src="<?php echo base_url() ?>/assets/images/min_2.jpg" style="height: 400px;width: 400px!important;" class="mr-4" alt="...">

							</div>
						</div>

					</div><a class="carousel-control-prev" href="#carousel_2" role="button"
							 data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span> </a><a class="carousel-control-next"
																	 href="#carousel_2" role="button" data-slide="next"><span
								class="carousel-control-next-icon" aria-hidden="true"></span> <span
								class="sr-only">Next</span></a>
				</div>
			</div>
			<!--end card-body-->
		</div>
	</div>
</div>

