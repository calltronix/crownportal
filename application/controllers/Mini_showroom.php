<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mini_showroom extends CI_Controller
{
	public function video()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('minishowroom/pages/video.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function concept_ppt()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('minishowroom/pages/concept_ppt.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function questionnaire()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('minishowroom/pages/questionnaire.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function value_sales()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('minishowroom/pages/value_sales.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function agreement()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('minishowroom/pages/agreement.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function faq()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);

		$this->load->view('header/header',$data);
		$this->load->view('minishowroom/pages/faq.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function check_session()
	{
		$session = $this->session->userdata('user_session');


		if ($session) {
			return $session['dealerCode'];
		} else {
			redirect('Auth');
		}
	}

}
