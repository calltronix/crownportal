<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticketing extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('header/header');
		$this->load->view('complaints/complaintsdashboard');
		$this->load->view('footer/footer');

    }
    
    public function productComplaint()
	{
		$this->load->view('header/header');
		$this->load->view('complaints/product');
		$this->load->view('footer/footer');

    }
    
    public function quoteComplaint()
	{
		$this->load->view('header/header');
		$this->load->view('complaints/quotecomplaint');
		$this->load->view('footer/footer');

    }
        
    public function orderComplaint()
	{
		$this->load->view('header/header');
		$this->load->view('complaints/ordercomplaint');
		$this->load->view('footer/footer');

	}

	public function brandingRequest()
	{
		$this->load->view('header/header');
		$this->load->view('requests/brandingrequest');
		$this->load->view('footer/footer');

	}
	public function trainingRequest()
	{
		$this->load->view('header/header');
		$this->load->view('requests/trainingrequest');
		$this->load->view('footer/footer');

	}
}
