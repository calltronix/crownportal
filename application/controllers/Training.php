<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Training extends CI_Controller
{
	public function category()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('training/pages/category.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function training_venue()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('training/pages/training_venue.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function factory_visit_request()
	{

		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('training/pages/factory_visit_request.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function Products_TNA()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('training/pages/products_tna.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function online_training()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('training/pages/online_training.php',$data);
		$this->load->view('footer/footer',$data);
	}

	public function faq()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('training/pages/faq.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function check_session()
	{
		$session = $this->session->userdata('user_session');


		if ($session) {
			return $session['dealerCode'];
		} else {
			redirect('Auth');
		}
	}
}
