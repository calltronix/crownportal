<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documents extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('header/dashboardheader');
		$this->load->view('itsupport/support_page');
		$this->load->view('footer/footer');

    }
    
    public function faqs()
	{
		$this->load->view('header/header');
		$this->load->view('documents/faqpage');
		$this->load->view('footer/footer');

    }
    
        
    public function paintSpecify()
	{
		$this->load->view('header/header');
		$this->load->view('documents/specifierpage');
		$this->load->view('footer/footer');

    }
    
    public function priceLists()
	{
		$this->load->view('header/header');
        $this->load->view('documents/pricespage');
		$this->load->view('footer/footer');

	}
}
