<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CVP extends CI_Controller
{
	public function questionnaire()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('cvp/pages/questionnaire',$data);
		$this->load->view('footer/footer',$data);
	}
	public function survey()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);

		$this->load->view('header/header',$data);
		$this->load->view('cvp/pages/survey',$data);
		$this->load->view('footer/footer',$data);
	}
	public function retail_audit_format()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('cvp/pages/retail_audit_format',$data);
		$this->load->view('footer/footer',$data);
	}
	public function faq()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('cvp/pages/faq',$data);
		$this->load->view('footer/footer',$data);
	}
	public function check_session()
	{
		$session = $this->session->userdata('user_session');


		if ($session) {
			return $session['dealerCode'];
		} else {
			redirect('Auth');
		}
	}
}
