<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dealer extends CI_Controller
{
	public function dashboard()
	{
		$session = $this->check_session();

		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('dashboard/pages/dashboard.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function promotions()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('dashboard/pages/promotions',$data);
		$this->load->view('footer/footer',$data);
	}
	public function dealer_profile()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('dealer_profile/pages/dealer_profile',$data);
		$this->load->view('footer/footer',$data);
	}
	public function price_list()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('price/pages/price_list',$data);
		$this->load->view('footer/footer',$data);
	}
	public function price_faq()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('price/pages/faq',$data);
		$this->load->view('footer/footer',$data);
	}

	public function sales_dashboard()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('sales/pages/dashboard',$data);
		$this->load->view('footer/footer',$data);
	}
	public function place_order()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('sales/pages/place_order',$data);
		$this->load->view('footer/footer',$data);
	}
	public function order_status()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('sales/pages/order_status',$data);
		$this->load->view('footer/footer',$data);
	}
	public function foc_request()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('sales/pages/foc_request',$data);
		$this->load->view('footer/footer',$data);
	}
	public function foc_request_status()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('sales/pages/foc_request_status',$data);
		$this->load->view('footer/footer',$data);
	}
	public function stock_balance()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('sales/pages/stock_balance',$data);
		$this->load->view('footer/footer',$data);
	}

	public function account_Statement()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('sales/pages/accountpage',$data);
		$this->load->view('footer/footer',$data);

	}

	public function pending_orders()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('sales/pages/pending_orders',$data);
		$this->load->view('footer/footer',$data);

	}
	public function other_issues()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('sales/pages/other_issues',$data);
		$this->load->view('footer/footer',$data);
	}
	public function club_benefit()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('privilege/pages/club_benefit',$data);
		$this->load->view('footer/footer',$data);
	}
	public function club_status()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('privilege/pages/club_status',$data);
		$this->load->view('footer/footer',$data);
	}
	public function club_PPT()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('privilege/pages/club_ppt',$data);
		$this->load->view('footer/footer',$data);
	}
	public function scheme_pictures()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('sales/pages/scheme_pictures',$data);
		$this->load->view('footer/footer',$data);
	}
	public function club_faq()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('sales/pages/club_faq',$data);
		$this->load->view('footer/footer',$data);
	}
	public function order_history()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('sales/pages/order_history',$data);
		$this->load->view('footer/footer',$data);
	}
	public function products()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('sales/pages/productspage',$data);
		$this->load->view('footer/footer',$data);

	}
	public function order_Form()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('sales/pages/createorderpage',$data);
		$this->load->view('footer/footer',$data);

	}
	public function accounts_dashboard()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('accounts/pages/dashboard',$data);
		$this->load->view('footer/footer',$data);
	}

	public function cart()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('sales/pages/cartpage',$data);
		$this->load->view('footer/footer',$data);

	}
	public function invoice()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('sales/pages/invoicingpage',$data);
		$this->load->view('footer/footer',$data);

	}
	public function history()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('sales/pages/orderhistorypage',$data);
		$this->load->view('footer/footer',$data);

	}
	public function complaint_dashboard()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('complaints/pages/complaintsdashboard',$data);
		$this->load->view('footer/footer',$data);

	}
	public function upload_picture()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('complaints/pages/upload_picture',$data);
		$this->load->view('footer/footer',$data);

	}
	public function complaint_faqs()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('complaints/pages/faq',$data);
		$this->load->view('footer/footer',$data);
	}
	public function quote_complaint()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('complaints/pages/quotecomplaint',$data);
		$this->load->view('footer/footer',$data);
	}
	public function order_complaint()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('complaints/pages/ordercomplaint',$data);
		$this->load->view('footer/footer',$data);
	}
	public function privilege_club_ppt()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('privilege/pages/club_ppt',$data);
		$this->load->view('footer/footer',$data);
	}
	public function privilege_club()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('privilege/pages/privilege_club',$data);
		$this->load->view('footer/footer',$data);
	}
	public function ongoing_scheme()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('privilege/pages/ongoing_scheme',$data);
		$this->load->view('footer/footer',$data);
	}
	public function promotional_slab()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('privilege/pages/promotional_slab',$data);
		$this->load->view('footer/footer',$data);

	}
	public function scheme_circular()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('privilege/pages/scheme_circular',$data);
		$this->load->view('footer/footer',$data);

	}
	public function privilege_faq()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('privilege/pages/faq',$data);
		$this->load->view('footer/footer',$data);

	}
	public function cvp_questionare()
	{
		$this->load->view('header/header');
		$this->load->view('complaints/pages/cvp_questionare');
		$this->load->view('footer/footer');

	}
	public function customer_satisfaction_survey()
	{
		$this->load->view('header/header');
		$this->load->view('complaints/pages/customer_satisfaction_survey');
		$this->load->view('footer/footer');
	}
	public function retail_audit()
	{
		$this->load->view('header/header');
		$this->load->view('complaints/pages/retail_audit');
		$this->load->view('footer/footer');
	}
	public function faq()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('complaints/pages/faq',$data);
		$this->load->view('footer/footer',$data);
	}
	public function contacts()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('dashboard/pages/contacts',$data);
		$this->load->view('footer/footer',$data);
	}
	public function competitor_details()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('competitor/pages/competitor_details',$data);
		$this->load->view('footer/footer',$data);

	}
	public function check_session()
	{
		$session = $this->session->userdata('user_session');


		if ($session) {
			return $session['dealerCode'];
		} else {
			redirect('Auth');
		}
	}
}
