<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Price extends CI_Controller
{
	public function test()
	{
		$this->load->view('dashboard/pages/test.php');
	}
	public function price_list()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('price/pages/price_list.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function products()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('price/pages/products.php',$data);
		$this->load->view('footer/footer',$data);
	}
		public function faq()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('price/pages/faq.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function check_session()
	{
		$session = $this->session->userdata('user_session');


		if ($session) {
			return $session['dealerCode'];
		} else {
			redirect('Auth');
		}
	}
}
