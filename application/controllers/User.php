<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{
	public function dashboard()
	{
		$this->load->view('header/header');
		$this->load->view('dashboard.php/pages/dashboard.php');
		$this->load->view('footer/footer');
	}
	public function promotions()
	{
		$this->load->view('header/header');
		$this->load->view('dashboard.php/pages/promotions');
		$this->load->view('footer/footer');
	}
	public function dealer_profile()
	{
		$this->load->view('header/header');
		$this->load->view('dealer_profile/pages/dealer_profile');
		$this->load->view('footer/footer');
	}

}
