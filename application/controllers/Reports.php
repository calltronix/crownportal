<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller
{
	public function value_sales()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('reports/pages/value_sales.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function volume_sales()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('reports/pages/volume_sales.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function product_category_level()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('reports/pages/product_category_level.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function check_session()
	{
		$session = $this->session->userdata('user_session');


		if ($session) {
			return $session['dealerCode'];
		} else {
			redirect('Auth');
		}
	}
}
