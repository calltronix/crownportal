<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		//$this->load->view('header/header');
		$this->load->view('auth/pages/signinpage');
		$this->load->view('auth/temps/scripts');

    }

	public function login_action()
	{
		$dealer_code = $this->input->post('dealer_code');
		$password = $this->input->post('password');

		$login = $this->AuthModel->login_user($dealer_code, $password);



		if($login === 'wrong')
		{
			echo "1";

			exit(0);
		}
		else
		{
			$sess_data = array
			(
				'dealerCode' => $dealer_code,

			);
			$this->session->set_userdata('user_session', $sess_data);
			echo "2";
		}
	}

	public function register()
	{
		$this->load->view('auth/pages/registerpage');
		$this->load->view('auth/temps/scripts');
	}
	public function recover_password()
	{
		$this->load->view('auth/pages/head');
		$this->load->view('auth/pages/recoverpassword');
		$this->load->view('auth/temps/scripts');
	}
    
    public function signin()
	{
		// $this->load->view('header/header');
		$this->load->view('auth/signinpage');
		// $this->load->view('footer/footer');

    }
    public function signout()
	{
		// $this->load->view('header/header');
		$this->load->view('auth/pages/signinpage');
		$this->load->view('auth/temps/scripts');
	}
    
    public function signup()
	{
		// $this->load->view('header/header');
		$this->load->view('auth/registerpage');
		// $this->load->view('footer/footer');

	}
}
