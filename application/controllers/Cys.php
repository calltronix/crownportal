<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cys extends CI_Controller
{
	public function ppt()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('cys/pages/ppt.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function videos()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('cys/pages/videos.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function agreement()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('cys/pages/agreement.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function customers_price_list()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('cys/pages/customers_price_list.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function purchase_price_list()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('cys/pages/purchase_price_list.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function leaflets_and_bronchures()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('cys/pages/leafletsandbronchures.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function value_sales()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('cys/pages/value_sales.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function questionare()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('cys/pages/questionare.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function faq()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('cys/pages/faq.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function bronchures()
	{
		$session = $this->check_session();
		$data = array
		(
			's_id' => $session,
			'page' => $this->uri->segment(2),
			'details' => $this->AuthModel->get_user_details($session)
		);
		$this->load->view('header/header',$data);
		$this->load->view('cys/pages/bronchures.php',$data);
		$this->load->view('footer/footer',$data);
	}
	public function check_session()
	{
		$session = $this->session->userdata('user_session');


		if ($session) {
			return $session['dealerCode'];
		} else {
			redirect('Auth');
		}
	}
}
